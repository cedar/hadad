#!/bin/bash
###############################################
#											  
#    Run HADAD-base to find Rewritings	      
#											  
###############################################
cd code/
chmod +x build-skip-tests.sh
./build-skip-tests.sh
cd HADAD-base/
mvn exec:java -Dexec.mainClass=hadad.base.runner.Runner
cd ../..

###############################################
#
# 	    Generate data
#
###############################################
cd data/synthetic/
chmod +x generate-synthetic.sh
./generate-synthetic.sh
cd ../..

###############################################
#											  
#			    LA No Views 				  
#											  
###############################################
FilePathLANoViews="scripts/LAPipe/"
cd $FilePathLANoViews

echo "Running LA NO Views at:" 
echo $PWD

#Run R Pipeline and Rewritings
cd R/PART1/
./run.sh
cd ../PART2
./run.sh
cd ../../

#Run NumPy Pipeline and Rewritings
cd NumPy/PART1/
./run.sh
cd ../PART2
./run.sh
cd ../../

#Run SystemML Pipeline and Rewritings
cd SystemML/PART1/
./run.sh
cd ../PART2
./run.sh
cd ../../

#Run TensorFlow Pipeline and Rewritings
cd TensorFlow/PART1/
./run.sh
cd ../PART2
./run.sh
cd ../../

#Run SparkMLlib Pipeline and Rewritings
cd SparkMLlib/PART1/
./run.sh
cd ../PART2
./run.sh
cd ../../../../


###############################################
#											  
#			    LA Views 				  	  
#											  
###############################################
FilePathLAViews="scripts/LAPipe-Views/"
cd $FilePathLAViews

echo "Running LA - Views at:" 
echo $PWD
#Run R Pipeline and Rewritings
cd R/
./run.sh
cd ..

#Run NumPy Pipeline and Rewritings
cd NumPy/
./run.sh
cd ..

#Run SystemML Pipeline and Rewritings
cd SystemML/
./run.sh
cd ..

#Run TensorFlow Pipeline and Rewritings
cd TensorFlow
./run.sh
cd ..

#Run SparkMLlib Pipeline and Rewritings
cd SparkMLlib/
./run.sh
cd ../../../

###############################################
#											  
#			      LA-RA 	 				  
#											  
###############################################
#Morpheus
FilePathMorpheus="scripts/Hybrid/MorpheusR/LAPipe/"
cd $FilePathMorpheus
./run.sh
cd ../../../../

###############################################
#											  
# 				 Ploting	  				  
#											
###############################################
#LA No Views (Naive and MNC)
FilePathPlotingLANoViews="figures/LAPipe/"
cd $FilePathPlotingLANoViews
echo "Ploting Figures LA NO Views at :" 
echo $PWD

cd MNC/
./run.sh
cd ../Naive/
./run.sh
cd ../../../

#LA Views (Naive)
FilePathPlotingLAViews="figures/LAViews/"
cd $FilePathPlotingLAViews
echo "Ploting Figures LA Views at :" 
echo $PWD

cd Naive/
./run.sh
cd ../../../

#Hybrid 
FilePathPlotingMorpheus="figures/Hybrid/Morpheus/"
cd $FilePathPlotingMorpheus
echo "Ploting Figures Morpheus at :"
echo $PWD

cd results/Overhead/
./run.sh
cd ..
cd Runtime/
./run.sh
cd ../../../../../

FilePathPlotingHybrid="figures/Hybrid/"
cd $FilePathPlotingHybrid
echo $PWD
cd Twitter/
echo "Ploting Figures Twitter at :"
echo $PWD
./run.sh
cd ../MIMIC/
echo "Ploting Figures MIMIC  at :"
echo $PWD
./run.sh
