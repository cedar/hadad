[![Version](https://img.shields.io/badge/version-0.0.1-blue.svg?style=flat)]()
[![GitHub license](https://img.shields.io/badge/license-apache-green.svg?style=flat)](https://www.apache.org/licenses/LICENSE-2.0)
[![Paper](https://img.shields.io/badge/paper-SIGMOD21-brown.svg?style=flat)](https://dl.acm.org/doi/10.1145/3448016.3457311)


<p>
    <img src="https://gitlab.inria.fr/cedar/hadad/-/raw/master/HADAD.png"  width=150  >
    <br>
</p>

# HADAD
<meta name="robots" content="noindex">

To work with HADAD, please follow the following steps:

1.  [Download HADAD Source code](https://gitlab.inria.fr/cedar/hadad#download-hadad)
2.  [Code Requirements and Build](https://gitlab.inria.fr/cedar/hadad#code-requirements-and-build)
3.  [Datasets/Views](https://gitlab.inria.fr/cedar/hadad/-/tree/master#datasets)
4.  [System Requirements for Running Pipelines](https://gitlab.inria.fr/cedar/hadad/-/tree/master#system-requirements-for-running-pipelines)
5.  [HADAD Runner](https://gitlab.inria.fr/cedar/hadad#hadad-runner)
6.  [Description of Used Hardware](https://gitlab.inria.fr/cedar/hadad#description-of-used-hardware)
7.  [Plot Figures](https://gitlab.inria.fr/cedar/hadad#plot-figures)
8.  [Tutorial: HADAD Quick Actions](https://gitlab.inria.fr/cedar/hadad#hadad-quick-actions)
9.  [Documentation ](https://gitlab.inria.fr/cedar/hadad#documentation)
10. [Code Format ](https://gitlab.inria.fr/cedar/hadad#code-format)
11. [Paper Reference](https://gitlab.inria.fr/cedar/hadad#paper-reference)
12. [Contact](https://gitlab.inria.fr/cedar/hadad#contact)


## Download HADAD
We recommend that you clone/pull the repo for latest updates.
```
 $ git clone https://gitlab.inria.fr/cedar/hadad.git
```
## Code Requirements and Build

###  Requirements
* A Unix-ish environment (Linux, Mac OS X). **NOTE: the code build was tested on Mac OS X and CentOS7 Linux.**
* Apache Maven 3.6.1 or newer ([Download and Install](https://maven.apache.org/))
* Java version >= 1.8 ([Download and Install](https://www.oracle.com/java/technologies/downloads/))

### Used Libraries
* [Antlr4.7](https://www.antlr.org/)
* [Google Guice](https://github.com/google/guice)
* [Google GSON](https://github.com/google/gson)
* [Google Guava](https://github.com/google/guava)
* [Google Findbugs](https://github.com/findbugsproject/findbugs)
* [JSON Jackson core](https://github.com/FasterXML/jackson-core)

### Instructions for the code build 
The directory ``code`` contains three main **components**:

* ``HADAD-commons`` - contains conjunctive query compiler and classes for relations and schemas
* ``HADAD-PACB`` - contains relational rewriting engine
* ``HADAD-base`` - contains HADAD core including supported language grammars, encoders, and decoders

Code build (skipping tests). This will automatically build the source code, all related dependencies and generate APIs documentation. **Note: documenting the code is work in progress.**
```bash
$ cd code/
$ chmod +x build-skip-tests.sh 
$./build-skip-tests.sh 
```
Code build (enabling tests). This will automatically build the source code, all related dependencies, run the tests and generate APIs documentation.
```bash
$ cd code/
$ chmod +x build-tests.sh 
$./build-tests.sh 
```

## Datasets 
We used a set of dense synthetic matrices and real-world sparse matrices 

### Synthetic
```data/synthetic```  : this directory generates dense synthetic datasets. 
```bash
$ cd data/synthetic 
$ chmod +x generate-synthetic.sh 
$./generate-synthetic.sh 
```
### Real-world
The real-data can be found in this [link](https://gitlab.inria.fr/cedar/hadad/-/blob/master/data/real-data/README.md) with instructions of how to request access.
The datasets should be placed under ``data/real-data`` directory.

### Views
```data/views``` : this directory generates the necessary views. 
```bash
$ cd data/views
$ chmod +x generate.sh
$./generate.sh
```
## System Requirements for Running Pipelines 
### Programming Languages: ```python - version 2.7```, ```scala - version 2.11.8```, ```R - version 3.6.0```
###  Systems Requirements: the following commands used in CentOS Linux release 7.9.2009 (Core)
* ```Python2.7``` - Libraries: ```NumPy 1.16.6```, ```os```, ```sys```, ```datetime```, ```time```, ```numpy.linalg```, ```pandas``` and ```genfromtxt```
```bash
$ yum install -y python2
$ pip2 install numpy
```
* `R version 3.6.0 ` - Libraries: `Matrix` and `psych`
	
```bash
$ yum install -y dnf-plugins-core
$ yum config-manager --set-enabled powertools
$ yum --enablerepo=extras install -y epel-release
$ yum install -y libcurl-devel
$ yum groupinstall -y "Development Tools"
$ yum install -y gcc-gfortran # installing gfortran compiler
$ yum install -y ncurses-devel zlib-devel texinfo gtk2-devel  tcl-devel tk-devel kernel-headers kernel-devel readline-devel
$ wget https://cran.r-project.org/src/base/R-3/R-3.6.0.tar.gz
$ tar xvzf R-3.6.0.tar.gz
$ cd R-3.6.0
$ ./configure --with-x=no
$ make
$ make install
$ R --version # print the version: 3.6.0
$ wget https://cran.r-project.org/src/contrib/Archive/mnormt/mnormt_1.5-7.tar.gz
$ R CMD INSTALL mnormt_1.5-7.tar.gz
$ Rscript -e 'install.packages("psych", repos = "https://personality-project.org/r/", type="source")'
$ Rscript -e 'print(1)'
```
* `TenforFlow r1.4` [Download and Install](https://www.tensorflow.org/install). Libraries: `logging` in addition to `python2.7` libraries. For building the TensorFlow from the sources, follow the instructions [here](https://www.tensorflow.org/install/source). 
	 
```bash
# Install Python requirements
$ yum install -y python3
$ ln -s /usr/bin/python3 /usr/bin/python
$ python3 -m venv ~/.virtualenvs/tf_dev
$ pip3 install -U pip six 'numpy<1.19.0' wheel setuptools mock 'future>=0.17.1'
$ pip3 install  keras_preprocessing --no-deps
$ export PYTHON_INCLUDE_PATH=\"/usr/bin/python\"
$ dnf install python3-devel -y
$ dnf install python2-devel -y
#Install Bazel version 0.5.4 - required to build TenforFlow r1.4
$ wget https://releases.bazel.build/0.5.4/release/bazel-0.5.4-without-jdk-installer-linux-x86_64.sh
$ chmod +x bazel-0.5.4-without-jdk-installer-linux-x86_64.sh
$ ./bazel-0.5.4-without-jdk-installer-linux-x86_64.sh
$ bazel version
#Build TenforFlow from source and enable SSE4.2 and AVX instructions optimization
$ git clone -b r1.4 https://github.com/tensorflow/tensorflow.git
$ cd tensorflow
$ touch /usr/include/stropts.h
$ python2.7 configure.py
$ bazel build -c opt --copt=-mavx --copt=-msse4.1 --copt=-msse4.2  -k //tensorflow/tools/pip_package:build_pip_package
$ pip3 install wheel
$ pip2 install wheel
$./bazel-bin/tensorflow/tools/pip_package/build_pip_package /tmp/tensorflow_pkg
$ pip2 install /tmp/tensorflow_pkg/tensorflow-1.4.2-cp27-cp27mu-linux_x86_64.whl
$ pip2 uninstall -y protobuf
$ pip2 install protobuf==3.8
$ python2.7 -c 'import tensorflow as tf; print(tf.__version__)' # This line should print 1.4.2
```
* `SparkMLlib 2.4.5 or 2.4.6` [Download and Install ](https://spark.apache.org/downloads.html). Libraries: [`Breeze`](https://github.com/scalanlp/breeze). We compile the popular OpenBLAS library from the sources  following the instructions [here](http://systemds.apache.org/docs/1.2.0/native-backend) and import  the optimized BLAS library at runtime by providing the  location of the native library through the Spark conf parameter `Example: --conf spark.executorEnv.LD_LIBRARY_PATH=/opt/OpenBLAS/lib/libopenblas.so` or `export LD_PRELOAD=/opt/OpenBLAS/lib/libopenblas.so` before running `SparkMLlib` pipelines.
```bash
$ cd bin/
$ wget https://github.com/xianyi/OpenBLAS/archive/v0.2.20.tar.gz
$ tar -xzf v0.2.20.tar.gz
$ cd OpenBLAS-0.2.20/
$ make clean
$ make USE_OPENMP=1
$ make install
$ cd ..
$ wget https://archive.apache.org/dist/spark/spark-2.4.6/spark-2.4.6.tgz
$ tar zxvf spark-2.4.6.tgz
$ cd spark-2.4.6/
$ ./build/mvn -DskipTests clean package
```

* SystemML 1.2.0 [Download and Install](http://www.apache.org/dyn/closer.lua/systemml/1.2.0/systemml-1.2.0-bin.tgz). ``bin/systemml-1.2.0-bin/lib/`` directory contains ``systemml-1.2.0.jar``  that we used.
```bash
$ cd bin/
$ wget https://archive.apache.org/dist/systemml/1.2.0/systemml-1.2.0-bin.tgz
$ tar zxvf systemml-1.2.0-bin.tgz
```

* `MorpheusR`. [Download and Install](https://github.com/lchen001/Morpheus). Make sure to clone and build the project under `bin/` directory. 
* `Spark-Solr connector`. [Build from source](https://github.com/lucidworks/spark-solr.git), and place generated `target/targetspark-solr-3.9.0-SNAPSHOT-shaded.jar` under  `bin/`


## HADAD Runner 
* The `HADAD_Runner.sh` script will perform the following steps:
	* (1) Generates synthetic data (as directed [here](https://gitlab.inria.fr/cedar/hadad/-/tree/master#datasets)). The real datasets should be placed under `data/real-data` directory
	* (2) Runs HADAD rewriting engine, which rewrites pipelines detailed [here](https://gitlab.inria.fr/cedar/hadad/-/blob/master/documentation/SIGMOD21ExtendedVersion/HADAD_EXTENDED_arxiv.pdf) using Naive and MNC cost models. The script will output the found rewritings in e.g., ``figures/LAPipe/Naive/results/resultsNaiveNoViewsRWS`` and the rewriting times in e.g., ``figures/LAPipe/Naive/results/resultsNaiveNoViewsRWTime``.
	* (3) Executes the original pipelines and their rewritings found by ``HADAD`` (pre-requisite systems should be installed, see [System Requirements for Running Pipelines](https://gitlab.inria.fr/cedar/hadad/-/blob/master/README.md#system-requirements-for-running-pipelines) section for installation instructions).  

## Description of Used Hardware
```
Architecture:          x86_64
CPU op-mode(s):        32-bit, 64-bit
Byte Order:            Little Endian
CPU(s):                40
On-line CPU(s) list:   0-39
Thread(s) per core:    2
Core(s) per socket:    10
Socket(s):             2
NUMA node(s):          2
Vendor ID:             GenuineIntel
CPU family:            6
Model:                 79
Model name:            Intel(R) Xeon(R) CPU E5-2640 v4 @ 2.40GHz
Stepping:              1
CPU MHz:               2893.212
CPU max MHz:           3400.0000
CPU min MHz:           1200.0000
BogoMIPS:              4799.55
Virtualization:        VT-x
L1d cache:             32K
L1i cache:             32K
L2 cache:              256K
L3 cache:              25600K
NUMA node0 CPU(s):     0,2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32,34,36,38
NUMA node1 CPU(s):     1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,3
Memory:                123G RAM
OS:                    CentOS Linux release 7.9.2009 (Core) 

```
## Plot Figures 
The directory `figures/`  contains three folders `LAPipe` (includes scripts to plot the results of LA-based pipelines experiments), `LAViews` ( to includes scripts to plot the results of LA-views based pipelines experiments) and `Hybrid`(includes scripts to plot the results of hybrid experiments)

### Requirements 
* pyhton3.7 
* Gnuplot  5.2 patchlevel 6
* Example:

```bash
 cd figures/LAPipe/MNC/
 # PART1 LA pipelines results (results_1.csv)
 # This command reads results from results_1.csv file and generates a set of Gnuplot scripts.
 $ python3.7 generateGnuPlot.py results_1.csv 
 # This command runs the generated Gnuplot scripts and outputs the figures under /figures folder.
 $ chmod +x run.sh
 $ ./run.sh
```
 
## HADAD Quick Actions
Check out the [tutorial](https://gitlab.inria.fr/cedar/hadad/-/blob/master/code/HADAD-tutorial/GettingStarted.md) for some quick actions using `HADAD` intermmediate abstraction!

## Documentation
```bash
cd documentation/
```
* `HADAD` SIGMOD 2021 extended version ```SIGMOD21ExtendedVersion/HADAD_EXTENDED_arxiv.PDF```
* `HADAD` Code documentation: ```sites/apidocs/help-doc.html```. HADAD APIs documentation is automatically generated from the code using `mvn javadoc:javadoc`. To read the doc, open ```help-doc.html``` using your default browser.

## Code Format 
``` bash
# Eclipse IDE:
Step 1: Eclipse > Preferences > Java > Code Style > Formatter > 
		import > HADADCodeFormatProfile.XML 
Step 2: Eclipse > Preferences > Java > Editor > Save Actions:
		1- Select "Perform the selected actions on save"
		2- Select "Format source code" and "Format all lines"
		3- Select "Organize imports"
Step 3: Select "Apply and Close"
```
``` bash
# IntelliJ IDE:
Step 1: IntelliJ > Preferences > Editor > Code Style > 
		Java > Scheme > Import Scheme > 
		Eclipse XML Profile > HADADCodeFormatProfile.XML 
Step 2: Select "Apply", then "OK".
```
## Paper Reference
If you use `HADAD`, please cite our SIGMOD'21 paper 
```bash
@inproceedings{DBLP:conf/sigmod/AlotaibiCDM21,
  author    = {Rana Alotaibi and
               Bogdan Cautis and
               Alin Deutsch and
               Ioana Manolescu},
  title     = {{HADAD:} {A} {L}ightweight {A}pproach for {O}ptimizing {H}ybrid {C}omplex {A}nalytics
               {Q}ueries},
  booktitle = {{SIGMOD}},
  pages     = {23--35},
  year      = {2021}
}
```
## Contact
Email: ralotaib@eng.ucsd.edu.
