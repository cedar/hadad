/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.backchase.Utils;

public enum CostFunction {

    RELATIONAL_JOIN_NUMBERS("relational_join_numbers"),
    MATRIX_INTERMEDIATE_RESULTS("matrix_intermediate_results"),
    SYSTEM_API("system_api");

    private final String costFunction;

    private CostFunction(final String costFunction) {
        this.costFunction = costFunction;
    }

    public String getCostFunction() {
        return costFunction;
    }
}
