/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.backchase.Utils;

import java.util.List;

import org.apache.log4j.Logger;

import hadad.backchase.provenanace.flatProvenance.FlatConjunct;
import hadad.commons.conjunctivequery.ConjunctiveQuery;

/**
 * Naive Costing
 */
public final class NaiveCosting {

    private final static Logger LOGGER = Logger.getLogger(NaiveCosting.class);

    private final CostFunction costFunction;
    private final List<FlatConjunct> provenanaceFormula;
    private final ConjunctiveQuery uPlan;
    private final ConjunctiveQuery query;

    public NaiveCosting(final List<FlatConjunct> provenanaceFormula, final ConjunctiveQuery uPlan,
            final ConjunctiveQuery query, final CostFunction costFunction) {
        this.costFunction = costFunction;
        this.provenanaceFormula = provenanaceFormula;
        this.uPlan = uPlan;
        this.query = query;
    }

    /**
     * Find minimum-cost Rewriting
     * 
     * @return rewriting
     */
    public ConjunctiveQuery findMinCostRewriting() {
        switch (costFunction) {
            case RELATIONAL_JOIN_NUMBERS:
                return CostingRelationallJoinNumbers.findRewriting(provenanaceFormula, uPlan, query);
            case MATRIX_INTERMEDIATE_RESULTS:
                return CostingMatrixModel.findRewriting(provenanaceFormula, uPlan, query);
            default:
                return null;
        }
    }
}
