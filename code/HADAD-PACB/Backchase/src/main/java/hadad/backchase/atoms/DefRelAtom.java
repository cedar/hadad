/**
 * Copyright 2013, 2014 Ioana Ileana @ Telecom ParisTech 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package hadad.backchase.atoms;

import java.util.Arrays;

import org.apache.log4j.Logger;

/**
 * A relational atom in the definition of a constraint
 * 
 * @author Ioana Ileana
 */
public class DefRelAtom {
    private final static Logger LOGGER = Logger.getLogger(DefRelAtom.class);
    public static int countDefRelAtoms = 0;

    private Relation relation;
    private AtomPositionTerm[] terms;
    private int id;

    /** Constructor **/
    public DefRelAtom(Relation relation, int size) {
        this.relation = relation;
        this.terms = new AtomPositionTerm[size];
        this.id = countDefRelAtoms;
        countDefRelAtoms++;
    }

    /**
     * Set relation
     * 
     * @param relation
     */
    public void setRelation(final Relation relation) {
        this.relation = relation;
    }

    /**
     * Set AtomPositionTerms
     * 
     * @param terms
     */
    public void setAtomPositionTerms(final AtomPositionTerm[] terms) {
        this.terms = terms;
    }

    /**
     * Set an AtomPositionTerm
     * 
     * @param term
     *            the term
     * @param i
     *            the index
     */
    public void setAtomPositionTerm(final AtomPositionTerm term, int i) {
        this.terms[i] = term;
    }

    /**
     * Get relation
     * 
     * @return relation
     */
    public Relation getRelation() {
        return relation;
    }

    /**
     * Get AtomPositionTerms
     * 
     * @return terms
     */
    public AtomPositionTerm[] getAtomPositionTerms() {
        return terms;
    }

    /**
     * Get AtomPositionTerm
     * 
     * @param i
     *            the index
     * @return the term
     */
    public AtomPositionTerm getAtomPositionTerm(int i) {
        return terms[i];
    }

    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder();
        str.append(relation.getName());
        str.append(Arrays.toString(terms));
        return str.toString();
    }

}
