/**
 * Copyright 2013, 2014 Ioana Ileana @ Telecom ParisTech 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package hadad.backchase.atoms;

import org.apache.log4j.Logger;

import hadad.backchase.instance.ConnectedComponent;

/**
 * A variable of the chased instance or a constant (not an atom position)
 * 
 * @author Ioana Ileana
 */
public class ResolvedTerm {

    private final static Logger LOGGER = Logger.getLogger(ResolvedTerm.class);

    private String name;
    private ConnectedComponent component;
    private int index;
    private int indexInComponent;

    /**
     * Constructor
     * 
     * @param name
     *            variable/constant name
     * @param index
     *            the variable index
     */
    public ResolvedTerm(final String name, int index) {
        this.name = name;
        this.index = index;
        setComponent(new ConnectedComponent(this));
        indexInComponent = 0;
    }

    /**
     * Set name
     * 
     * @param name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Set index
     * 
     * @param index
     */
    public void setIndex(final int index) {
        this.index = index;
    }

    /**
     * Set component
     * 
     * @param component
     */
    public void setComponent(final ConnectedComponent component) {
        this.component = component;
    }

    /**
     * Set index component
     * 
     * @param indexInComponent
     */
    public void setIndexComponenet(final int indexInComponent) {
        this.indexInComponent = indexInComponent;
    }

    /**
     * Get index
     * 
     * @return index
     */
    public int getIndex() {
        return index;
    }

    /**
     * Get name
     * 
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Get component
     * 
     * @return component
     */
    public ConnectedComponent getComponent() {
        return component;
    }

    /**
     * Get index component
     * 
     * @return indexInComponent
     */
    public int getIndexComponenet() {
        return indexInComponent;
    }

    @Override
    public String toString() {
        return name;
    }
}
