/**
  * Copyright 2013, 2014 Ioana Ileana @ Telecom ParisTech 
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

package hadad.backchase.instance;

import org.apache.log4j.Logger;

import hadad.backchase.constraints.Tgd;
import hadad.backchase.provenanace.flatProvenance.FlatFormula;
import hadad.backchase.provenanace.placeHodlerProvenance.PHFormula;

/**
 * 
 * Query
 * 
 * @author Ioana Ileana
 *
 */
public class Query extends Tgd {
    private final static Logger LOGGER = Logger.getLogger(Query.class);

    private FlatFormula provenance;
    private int minCost;

    /**
     * Constructor
     */
    public Query() {
        super();
        provenance = null;
        minCost = 100000;

    }

    /**
     * Set provenance
     * 
     * @param provenance
     */
    public void setProvenance(final FlatFormula provenance) {
        this.provenance = provenance;
    }

    /**
     * Get provenance
     * 
     * @return provenance
     */
    public FlatFormula getProvenance() {
        return provenance;
    }

    /**
     * Get minimum cost
     * 
     * @return minCost
     */
    public int getMinCost() {
        return minCost;
    }

    @Override
    public void addNewLocalMapping(final LocalMapping mapping, final Object adder) {
        provenance = mapping.m_flatProvenance;
        if (Switches.COST_ON)
            minCost = provenance.shrinkToMinCost();
    }

    @Override
    public void addExistingMappingAdditionalProvenance(LocalMapping mapping, PHFormula phform, FlatFormula flatform,
            Object adder) {
        if (Switches.COST_ON)
            minCost = provenance.shrinkToMinCost();
    }
}
