/**
  * Copyright 2013, 2014 Ioana Ileana @ Telecom ParisTech 
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

package hadad.backchase.provenanace.provenanaceEvents;

import org.apache.log4j.Logger;

import hadad.backchase.atoms.ResolvedEquality;
import hadad.backchase.atoms.ResolvedRelAtom;

/**
 * 
 * ProvChangedOnAtom
 * 
 * @author Ioana Ileana
 */
public class ProvChangedOnAtom extends ProvenanceEvent {
    private final static Logger LOGGER = Logger.getLogger(ProvChangedOnAtom.class);

    private ResolvedRelAtom atom;

    /**
     * Constructor
     * 
     * @param atom
     */
    public ProvChangedOnAtom(ResolvedRelAtom atom) {
        this.atom = atom;
    }

    public void propagate() {
        for (ResolvedRelAtom atom : atom.getAtomWatchers())
            atom.provenanceChanged();
        for (ResolvedEquality eq : atom.getEqualityWatchers())
            eq.provenanceChanged();
    }
}
