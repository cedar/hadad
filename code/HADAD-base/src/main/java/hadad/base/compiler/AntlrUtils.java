/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.Interval;

/**
 * Provides some common methods for Antlr
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
public final class AntlrUtils {
    /**
     * Returns the original text (preserving white spaces) of the given context.
     *
     * @param context
     *            the parse rule context whose original text (preserving white
     *            spaces) is wanted.
     * @return original text (preserving white spaces) of the given context.
     */
    public static String getFullText(final ParserRuleContext context) {
        if (context.start == null || context.stop == null || context.start.getStartIndex() < 0
                || context.stop.getStopIndex() < 0) {
            return context.getText();
        }

        return context.start.getInputStream()
                .getText(Interval.of(context.start.getStartIndex(), context.stop.getStopIndex()));
    }
}
