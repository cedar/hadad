/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import java.util.stream.Collectors;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

/**
 * Node setter query block tree
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
@Singleton
public class CreatedNodesSetteQueryBlockTree implements IQueryBlockTreeVisitor {
    private static final Logger LOGGER = Logger.getLogger(CreatedNodesSetteQueryBlockTree.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }

    private final CreatedNodesSetterReturnTerm createdNodesSetterVisitor;
    private final VariableFactory elementVariableFactory;

    @Inject
    public CreatedNodesSetteQueryBlockTree(final VariableCopier variableCopier,
            @Named("ElementVariableFactory") final VariableFactory elementVariableFactory) {
        this.elementVariableFactory = elementVariableFactory;
        createdNodesSetterVisitor = new CreatedNodesSetterReturnTerm(variableCopier, elementVariableFactory);
    }

    public void setCreatedNodes(final QueryBlockTree nbt) {
        elementVariableFactory.reset();
        nbt.accept(this);
    }

    @Override
    public void visit(final QueryBlockTree tree) {
        // NOP (no created nodes on the tree)
    }

    @Override
    public void visit(final RootBlock block) {
        _visit(block);
    }

    @Override
    public void visit(final ChildBlock block) {
        _visit(block);
    }

    private void _visit(final Block block) {
        createdNodesSetterVisitor.setCreatedNodes(block.getReturnTemplate(),
                block.getReturnTemplate().getTerms().stream().filter(t -> t instanceof ReturnConstructTerm)
                        .map(t -> elementVariableFactory.createFreshVar()).collect(Collectors.toList()));
    }
}