/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Singleton;

import hadad.commons.conjunctivequery.Variable;

/**
 * Nodes setter for return term
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
@Singleton
class CreatedNodesSetterReturnTerm implements IReturnTemplateVisitor {
    private static final Logger LOGGER = Logger.getLogger(CreatedNodesSetterReturnTerm.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }
    private final VariableCopier variableCopier;
    private final VariableFactory elementVariableFactory;

    private List<Variable> createdNodes;

    public CreatedNodesSetterReturnTerm(final VariableCopier variableCopier,
            final VariableFactory elementVariableFactory) {
        this.variableCopier = variableCopier;
        this.elementVariableFactory = elementVariableFactory;
    }

    public void setCreatedNodes(final ReturnTemplate template, final List<Variable> createdNodes) {
        this.createdNodes = createdNodes;
        template.accept(this);
    }

    @Override
    public void visit(ReturnTemplate template) {
        int i = 0;
        for (final ReturnTerm term : template.getTerms()) {
            if (term instanceof ReturnConstructTerm) {
                term.setCreatedNode(createdNodes.get(i++));
            }
        }
    }

    @Override
    public void visitPre(ReturnConstructTerm term) {
        for (final ReturnTerm child : term.getChildren()) {
            if (child instanceof ReturnConstructTerm) {
                child.setCreatedNode(elementVariableFactory.createFreshVar());
            }
        }
    }

    @Override
    public void visitPost(ReturnConstructTerm term) {
        // NOP (no created node to set after the children are processed)
    }

    @Override
    public void visit(ReturnVariableTerm term) {
        term.setCreatedNode(variableCopier.getCopy(term.getVariable()));
    }

    @Override
    public void visit(ReturnStringTerm term) {
        // NOP (leaf term created node is set on the parent or in the
        // return
        // template itself)
    }
}