/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Represents the supported storage system data types.
 *
 * Used to construct elements for a given data row.
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
public enum DataType {
    STRING(String.class.getClass()),
    INT(Integer.class);

    /* The correspondent Java class for the data type. */
    private final Class<?> klass;

    /**
     * Constructs a new data type with the specified correspondent Java class.
     *
     * @param klass
     *            the correspondent Java class for the data type.
     */
    private DataType(final Class<?> klass) {
        this.klass = klass;
    }

    /**
     * The correspondent class for this data type.
     *
     * @return the correspondent class for this data type.
     */
    public Class<?> classOf() {
        return klass;
    }

    /**
     * The value of the given object correspondent for this data type.
     *
     * @param o
     *            an object
     * @return the value of the given object correspondent for this data type.
     */
    public Object valueOf(final Object o) throws RuntimeException {
        try {
            final Method m = klass.getMethod("valueOf", o.getClass());
            return m.invoke(null, o);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
            throw new RuntimeException("Cannot get the value of the given object.");
        }

    }
}
