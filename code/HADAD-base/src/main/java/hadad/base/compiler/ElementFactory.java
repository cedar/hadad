/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import hadad.base.compiler.exceptions.ParseException;
import hadad.commons.conjunctivequery.Variable;

/**
 * Element factory.
 *
 * Creates the corresponding element.
 */
public class ElementFactory {
    /**
     * Attempts to creates an element.
     *
     * @param o
     * Object
     * @throws RuntimeException
     * runtime exception
     * @return element
     * string literal or variable
     * @throws
     */
    public static IElement createElement(final Object o) throws ParseException {
        if (o instanceof String) {
            return new StringElement((String) o);
        }
        if (o instanceof Variable) {
            return new VariableElement((Variable) o);
        }
        throw new ParseException("Type not supported");
    }
}
