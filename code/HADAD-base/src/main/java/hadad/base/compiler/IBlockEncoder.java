package hadad.base.compiler;

import hadad.commons.conjunctivequery.ConjunctiveQuery;

/**
 * Block Encoder
 */
public interface IBlockEncoder {
    /**
     * Encodes Query block into {@link ConjunctiveQuery}
     * 
     * @param block
     *            query
     * @return encoded {@link ConjunctiveQuery}
     */
    ConjunctiveQuery encode(final Block block);
}
