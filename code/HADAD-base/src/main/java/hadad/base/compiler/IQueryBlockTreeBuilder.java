/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import com.google.common.collect.ImmutableMap;

import hadad.base.compiler.exceptions.ParseException;
import hadad.commons.conjunctivequery.Variable;

/**
 * Builder pattern.
 *
 * Provides a way to build path expressions, return templates and nested block
 * trees from the syntax (encode), and build path expressions and patterns from
 * conjunctive queries (decode).
 *
 * Path expression and return template encoding is used to create multi-model
 * nested block trees.
 *
 * Path expression and pattern decoding is used to create (PACB) rewritten
 * nested block trees
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
public interface IQueryBlockTreeBuilder {
    /**
     * Parse the given path expression and return a path expression instance.
     *
     * @param str
     *            path expression string
     *
     * @return the path expression
     * @throws ParseException
     *             parsing exception
     */
    PathExpression buildPathExpression(final String str) throws ParseException;

    /**
     * Parse the given return template and return a return template instance.
     *
     * @param definitions
     *            the definitions available in this return template (used for
     *            the path expression optionals).
     * @param str
     *            return template string
     *
     * @return the return template
     * @throws ParseException
     *             parsing exception
     */
    ReturnTemplate buildReturnTemplate(ImmutableMap<Variable, PathExpression> definitions, final String str)
            throws ParseException;

    /**
     * Parse the given query expression string and return a child instance.
     *
     * @param str
     *            query expression string
     * @return the query block tree instance
     * @throws ParseException
     *             parsing exception
     */
    QueryBlockTree buildQueryBlockTree(final String str) throws ParseException;

    /**
     * Parse the given pattern expression string and a pattern instance
     * 
     * @param str
     *            pattern expression string
     * @return the pattern instance
     * @throws ParseException
     *             parsing exception
     */
    Pattern buildPattern(final String str) throws ParseException;
}
