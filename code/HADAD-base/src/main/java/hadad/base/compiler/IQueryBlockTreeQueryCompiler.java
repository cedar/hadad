/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import java.util.LinkedHashSet;
import java.util.Set;

import hadad.base.compiler.exceptions.HadadCompilationException;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.ConjunctiveQuery;
import hadad.commons.conjunctivequery.Term;

/**
 * Query block tree (query) compiler
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
public interface IQueryBlockTreeQueryCompiler {
    /**
     * Compiles the nested block tree
     *
     * @param nbt
     *            the nested block trees.
     * @param includeComments
     *            if <code>true</code> comments will be included.
     * @return the encoded conjunctive query for the specified nested block trees.
     * @throws HadadCompilationException
     *             compilation exception
     */
    default ConjunctiveQuery compileQuery(final QueryBlockTree nbt) throws HadadCompilationException {
        final Set<Atom> queryBody = new LinkedHashSet<>();
        final Set<Term> queryHead = new LinkedHashSet<>();
        queryBody.addAll(compilePattern(nbt.getRoot().getPattern()));
        queryHead.addAll(compileReturnTemplate(nbt.getRoot().getReturnTemplate()));

        return new ConjunctiveQuery(queryHead, queryBody);
    }

    /**
     * Compiles the pattern for the specified query block tree.
     *
     * @param nbtPattern
     *            the nested block tree.
     * @param includeComments
     *            if <code>true</code> comments will be
     *            included.
     * @return the encoded query body for the specified nested block tree.
     * @throws HadadCompilationException
     *             compilation exception
     */
    Set<? extends Atom> compilePattern(final Pattern nbtPattern) throws HadadCompilationException;

    /**
     * Compiles the return template for the specified query block tree.
     *
     * @param nbtReturnTemplate
     *            the nested block tree.
     * @return the encoded query head for the specified nested block tree.
     * @throws HadadCompilationException
     *             compilation exception
     */
    Set<? extends Term> compileReturnTemplate(final ReturnTemplate nbtReturnTemplate) throws HadadCompilationException;
}
