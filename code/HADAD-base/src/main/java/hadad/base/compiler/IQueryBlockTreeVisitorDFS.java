/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

/**
 * Visitor pattern.
 *
 * Provides a DFS way to navigate the elements of a query block tree.
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
public interface IQueryBlockTreeVisitorDFS {
    void visit(final QueryBlockTree tree);

    void visit(final RootBlock block);

    void visit(final ChildBlock block);

    void visitPre(final QueryBlockTree block);

    void visitPost(final QueryBlockTree block);

    void visitPre(final RootBlock block);

    void visitPre(final ChildBlock block);

    void visitPost(final RootBlock block);

    void visitPost(final ChildBlock block);

}
