/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import hadad.base.compiler.exceptions.ReturnConstructException;

/**
 * Interface for return template construction.
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
public interface IReturnTemplateConstructor {
    /**
     * Constructs the given return template using the given row data.
     *
     * @param template
     *            the return template.
     * @param row
     *            the retrieved row to be constructed.
     * @return the constructed element with the retrieved row data and this
     *         return term.
     * @throws ReturnConstructException
     */
    String construct(final ReturnTemplate template, final Row row) throws ReturnConstructException;

    /**
     * Constructs the given return template.
     *
     * @param template
     *            the return template.
     * @return the constructed element with the variables in their positions.
     * @throws ReturnConstructException
     */
    default String construct(final ReturnTemplate template) throws ReturnConstructException {
        return construct(template, null);
    }
}
