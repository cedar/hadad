/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import java.util.Collection;

import hadad.commons.conjunctivequery.ConjunctiveQuery;

/**
 * Provides a way to select one (conjunctive query) rewriting from a collection
 * of (conjunctive query) rewritings (given by a rewrite algorithm)
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
public interface IRewritingSelector {
    /**
     * Selects one rewriting from the specified collection.
     *
     * Throws an IllegalArgumentException if the given collection of rewritings
     * is empty.
     *
     * @param rewritings
     *            collection of rewritings to select the rewriting from.
     * @return the selected rewriting from the collection.
     */
    ConjunctiveQuery select(final Collection<ConjunctiveQuery> rewritings) throws IllegalArgumentException;
}
