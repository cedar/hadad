/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Part of nested block tree data structure.
 *
 * Represents the supported models.
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
public abstract class Model {
    /* The model unique identifier. */
    private final String id;
    /* The model format. */
    private final Format format;
    /* The model language. */
    private final Language language;
    /* The model block encoder. */
    private final IBlockEncoder blockEncoder;
    /* The model nested block tree builder. */
    private final IQueryBlockTreeBuilder queryBlockTreeBuilder;

    /**
     * Constructs a new model with the specified format and language.
     *
     * @param id
     *            the model unique identifier.
     * @param format
     *            the model format.
     * @param language
     *            the model language
     * @param queryBlockTreeBuilder
     *            the model nested block tree builder.
     * @param blockEncoder
     *            the model block encoder.
     */
    protected Model(final String id, Format format, Language language,
            final IQueryBlockTreeBuilder queryBlockTreeBuilder, final IBlockEncoder blockEncoder) {
        this.id = checkNotNull(id);
        this.format = checkNotNull(format);
        this.language = checkNotNull(language);
        this.queryBlockTreeBuilder = checkNotNull(queryBlockTreeBuilder);
        this.blockEncoder = checkNotNull(blockEncoder);
    }

    /**
     * This model unique identifier.
     *
     * @return this model unique identifier.
     */
    public String getId() {
        return id;
    }

    /**
     * This model format.
     *
     * @return this model format.
     */
    public Format getFormat() {
        return format;
    }

    /**
     * This model language.
     *
     * @return the model language.
     */
    public Language getLanguage() {
        return language;
    }

    /**
     * This model block encoder.
     *
     * @return the model block encoder.
     */
    public IBlockEncoder getBlockEncoder() {
        return blockEncoder;
    }

    /**
     * This model nested block tree builder.
     *
     * @return the model nested block tree builder.
     */
    public IQueryBlockTreeBuilder getNestedBlockTreeBuilder() {
        return queryBlockTreeBuilder;
    }
}
