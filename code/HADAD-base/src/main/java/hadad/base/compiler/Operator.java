/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Part of query block tree data structure.
 *
 * Represents the operator in a query block tree block conditional binary
 * condition.
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
public enum Operator {
    EQUALS("=");

    /* The string representation of the operator. */
    private final String str;

    /**
     * Constructs a new operator with the specified string representation.
     *
     * @param str
     *            the string representation of the operator.
     */
    private Operator(final String str) {
        this.str = checkNotNull(str);
    }

    @Override
    public String toString() {
        return str;
    }
}
