/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.rits.cloning.Cloner;

import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.Variable;

/**
 * Part of query block tree data structure.
 *
 * Represents (nested block tree) block pattern expression, composed of the
 * structural and the conditional.
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
public final class Pattern {
    private static final Logger LOGGER = Logger.getLogger(PathExpression.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }

    /* The structural of this pattern. */
    private final Structural structural;
    /* The conditional of this pattern. */
    private final Conditional conditional;

    /**
     * Constructs a new pattern with the specified structural and conditional.
     *
     * @param structural
     *            the structural for the pattern.
     * @param conditional
     *            the conditional for the pattern.
     */
    public Pattern(final Structural structural, final Conditional conditional) {
        this.structural = checkNotNull(structural);
        this.conditional = checkNotNull(conditional);

    }

    /**
     * Returns <code>true</code> if this pattern contains no structural and no
     * conditionals.
     *
     * @return <code>true</code> if this structural contains no structural and
     *         no conditionals.
     */
    public boolean isEmpty() {
        return structural.isEmpty() && conditional.isEmpty();
    }

    /**
     * The variables introduced in this block.
     *
     * @return variables introduced in this block.
     */
    public List<Variable> getLocalDefinedVariables() {
        return structural.getDefinedVariables();
    }

    /**
     * The variables referred in this block.
     *
     * @return variables referred in this block.
     */
    public Set<Variable> getReferredVariables() {
        final ImmutableSet.Builder<Variable> builder = ImmutableSet.builder();
        builder.addAll(structural.getReferredVariables());
        builder.addAll(conditional.getReferredVariables());
        return builder.build();
    }

    /**
     * The definitions introduced in this structural.
     *
     * @return definitions introduced in this structural.
     */
    public Map<Variable, PathExpression> getDefinitions() {
        return structural.getDefinitions();
    }

    /**
     * Encodes this pattern into relational CQ model.
     *
     * @param f
     *            the function that accepts one condition as argument and return
     *            its encoding.
     * @return relational CQ encoding for this pattern.
     */
    public List<Atom> encoding(final Function<Condition, List<Atom>> f) {
        final ImmutableList.Builder<Atom> builder = ImmutableList.builder();
        builder.addAll(structural.encoding());
        builder.addAll(conditional.encoding());
        builder.addAll(conditional.encoding(f));
        return builder.build();
    }

    /**
     * @param p
     *            the pattern that will be merged with current pattern
     * @return the new merged pattern
     */
    public Pattern mergePattern(final Pattern p) {
        this.structural.merge(p.structural);
        this.conditional.merge(p.conditional);
        return this;
    }

    /**
     * Returns the structural instance of the current pattern
     * 
     * @return structural
     *         structural pattern
     */
    public Structural getStructural() {
        return this.structural;
    }

    /**
     * Returns the conditional instance of the current pattern
     * 
     * @return conditional
     *         conditional pattern
     */
    public Conditional getConditional() {
        return this.conditional;
    }

    public Pattern deepClone() {
        final Cloner cloner = new Cloner();
        return cloner.deepClone(this);
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        if (!structural.isEmpty()) {
            builder.append("FOR: ").append(structural.toString()).append(";\n");
        }
        if (!conditional.isEmpty()) {
            builder.append("WHERE: ").append(conditional.toString()).append(";\n");
        }
        return builder.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(structural, conditional);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Pattern)) {
            return false;
        }
        final Pattern p = (Pattern) o;
        return structural.equals(p.structural) && conditional.equals(p.conditional);
    }
}
