/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.rits.cloning.Cloner;

/**
 * Part of query block tree data structure.
 *
 * We call s block tree of a query V the tree structure arranging the
 * blocks that reflects (in the parent-child relationship) the immediate nesting
 * relationship of the syntax.
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
public final class QueryBlockTree {
    private static final Logger LOGGER = Logger.getLogger(PathExpression.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }
    /* The nested block tree root. */
    private final RootBlock root;

    /**
     * Constructs a new nested block tree with the specified root.
     *
     * @param root
     *            the nested block tree root.
     */
    public QueryBlockTree(final RootBlock root) {
        this.root = checkNotNull(root);
    }

    /**
     * Returns the root block of this nested block tree.
     *
     * @return the root block of this nested block tree.
     */
    public RootBlock getRoot() {
        return root;
    }

    /**
     * The query name of the query represented by this nested block tree.
     *
     * @return the query name of the query represented by nested block tree.
     */
    public String getQueryName() {
        return root.getQueryName();
    }

    /**
     * Visitor pattern.
     *
     * Provides a way to navigate the elements of a nested block tree.
     *
     * @param visitor
     *            the nested block tree visitor implementation.
     */
    public void accept(final IQueryBlockTreeVisitor visitor) {
        visitor.visit(this);
        root.accept(visitor);
    }

    /**
     * Visitor pattern in DFS.
     *
     * Provides a DFS way to navigate the elements of a nested block tree .
     *
     * @param visitor
     *            the nested block tree visitor implementation.
     */
    public void acceptDFS(final IQueryBlockTreeVisitorDFS visitor) {
        visitor.visit(this);
        root.acceptDFS(visitor);
    }

    /**
     * The height of this nested block tree.
     *
     * @return the height of this nested block tree.
     */
    public int height() {
        return root.height();
    }

    public QueryBlockTree deepClone() {
        final Cloner cloner = new Cloner();
        return cloner.deepClone(this);
    }

    @Override
    public String toString() {
        return root.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(root);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof QueryBlockTree)) {
            return false;
        }
        return root.equals(((QueryBlockTree) o).root);
    }
}
