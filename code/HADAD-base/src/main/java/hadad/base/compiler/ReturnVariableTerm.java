/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Map;
import java.util.Objects;
import java.util.Set;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import hadad.commons.conjunctivequery.Term;
import hadad.commons.conjunctivequery.Variable;

/**
 * Part of nested block tree data structure.
 *
 * Represents (nested block tree) block return template variable (leaf) term.
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
public final class ReturnVariableTerm extends ReturnTerm implements IReturnLeafTerm {
    private static final Logger LOGGER = Logger.getLogger(ReturnVariableTerm.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }
    /* The variable of this return (leaf) term */
    private final Variable var;
    /* The optionals in this return template. */
    private final ImmutableMap<String, String> optionals;

    /**
     * Constructs a new return template variable (leaf) term with the specified
     * variable and optionals.
     *
     * @param var
     *            the variable for the return (leaf) term.
     * @param optionals
     *            the optionals for the return variable term.
     */
    public ReturnVariableTerm(final Variable var, final Map<String, String> optionals) {
        this(null, var, optionals);
    }

    /**
     * Constructs a new return template variable (leaf) term with the specified
     * parent return template construct term, variable and optionals.
     *
     * @param parent
     *            the parent return template construct term.
     * @param var
     *            the variable for the return (leaf) term.
     * @param optionals
     *            the optionals for the return variable term.
     */
    public ReturnVariableTerm(final ReturnConstructTerm parent, final Variable var,
            final Map<String, String> optionals) {
        super(parent);
        this.var = checkNotNull(var);
        this.optionals = ImmutableMap.copyOf(checkNotNull(optionals));
    }

    /**
     * The variable of this return (leaf) term.
     *
     * @return the variable of this return (leaf) term.
     */
    public Variable getVariable() {
        return var;
    }

    /**
     * The optionals in this return template.
     *
     * @return optionals in this return template.
     */
    public Map<String, String> getOptionals() {
        return optionals;
    }

    @Override
    public Term toTerm() {
        return var;
    }

    @Override
    public void accept(final IReturnTemplateVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public Set<Variable> getReferredVariables() {
        final ImmutableSet.Builder<Variable> builder = ImmutableSet.builder();
        builder.add(var);
        return builder.build();
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append(var.toString());
        if (!optionals.isEmpty()) {
            builder.append("[").append(optionals.toString()).append("]");
        }
        return builder.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(var, super.hashCode(), optionals);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof ReturnVariableTerm)) {
            return false;
        }
        final ReturnVariableTerm p = (ReturnVariableTerm) o;
        return super.equals(p) && var.equals(p.var) && optionals.equals(p.optionals);
    }
}
