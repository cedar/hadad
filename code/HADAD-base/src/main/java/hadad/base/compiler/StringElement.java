/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.commons.conjunctivequery.StringConstant;

/**
 * Part of query block tree data structure.
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
/* Should we give one id to each created element? */
public final class StringElement implements IElement {
    private static final Logger LOGGER = Logger.getLogger(StringElement.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }
    /* The element tag name. */
    private final String tag;

    /**
     * Constructs a new element with the specified tag.
     *
     * @param tag
     *            the element tag name.
     */
    public StringElement(final String tag) {
        this.tag = checkNotNull(tag);
    }

    /**
     * The conjunctive query term for this element.
     *
     * @return the conjunctive query term for this element.
     */
    @Override
    public StringConstant toTerm() {
        return new StringConstant(tag);
    }

    @Override
    public boolean isEmpty() {
        return tag.isEmpty();
    }

    @Override
    public String toString() {
        return tag;
    }

    @Override
    public int hashCode() {
        return tag.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof StringElement) && tag.equals(((StringElement) o).tag);
    }
}
