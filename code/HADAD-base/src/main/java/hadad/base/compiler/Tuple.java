/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import java.util.Objects;

/**
 * Simple tuple implementation.
 *
 * @param <S>
 * @param <T>
 */
public class Tuple<S, T> {
    private final S s;
    private final T t;

    /**
     * @param s
     *            first element of the tuple
     * @param t
     *            second element of the tuple
     */
    public Tuple(final S s, final T t) {
        this.s = s;
        this.t = t;
    }

    /**
     * Returns the first element of the tuple.
     *
     * @return the first element of the tuple
     */
    public S first() {
        return s;
    }

    /**
     * Returns the second element of the tuple
     *
     * @return the second element of the tuple
     */
    public T second() {
        return t;
    }

    @Override
    public String toString() {
        return "<" + s.toString() + "," + t.toString() + ">";
    }

    @Override
    public int hashCode() {
        return Objects.hash(s, t);
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Tuple<?, ?>) && s.equals(((Tuple<?, ?>) o).s) && t.equals(((Tuple<?, ?>) o).t);
    }
}
