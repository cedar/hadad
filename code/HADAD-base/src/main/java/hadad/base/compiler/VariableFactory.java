/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.commons.conjunctivequery.Variable;

/**
 * Provides fresh conjunctive query variables.
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
public final class VariableFactory {
    private static final Logger LOGGER = Logger.getLogger(VariableFactory.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }
    /* The fresh variables name prefix. */
    private final String freshVarPrefix;
    /* The fresh variables counter. */
    private int freshVarCounter;

    /**
     * Constructs a new variable factory with the specified fresh variable name
     * prefix.
     *
     * @param freshVarPrefix
     *            the fresh variables name prefix
     */
    public VariableFactory(final String freshVarPrefix) {
        this.freshVarPrefix = checkNotNull(freshVarPrefix);
        reset();
    }

    /**
     * Creates a fresh variable.
     *
     * @return a fresh variable.
     */
    public Variable createFreshVar() {
        return new Variable(freshVarPrefix + Integer.toString(freshVarCounter++));
    }

    /**
     * Restarts the fresh variables counter to its initial seed.
     */
    public void reset() {
        freshVarCounter = 0;
    }

    @Override
    public String toString() {
        return freshVarPrefix + ": " + freshVarCounter;
    }

    @Override
    public int hashCode() {
        return Objects.hash(freshVarPrefix, freshVarCounter);
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof VariableFactory)) {
            return false;
        }
        final VariableFactory p = (VariableFactory) o;
        return freshVarPrefix.equals(p.freshVarPrefix) && freshVarCounter == p.freshVarCounter;
    }
}
