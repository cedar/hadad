/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.aj;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionException;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.AntlrUtils;
import hadad.base.compiler.Condition;
import hadad.base.compiler.Conditional;
import hadad.base.compiler.PathExpression;
import hadad.base.compiler.exceptions.ParseException;

/**
 * AJ Conditional Listener
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
@Singleton
final class ConditionalListener extends AQLBaseListener {
    private static final Logger LOGGER = Logger.getLogger(ConditionalListener.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }

    private final ConditionListener conditionListener;
    private final ConditionalStructuralListener conditionalStructuralListener;

    /* Used to keep the path expression in the conditional being parsed */
    private List<PathExpression> pathExpressions;
    /* Used to keep the equalities in the conditional being parsed */
    private List<Condition> conditions;

    /* Used to define models in the conditional */
    private List<String> models;

    @Inject
    public ConditionalListener(final ConditionListener conditionListener,
            final ConditionalStructuralListener conditionalStructuralListener) {
        this.conditionListener = conditionListener;
        this.conditionalStructuralListener = conditionalStructuralListener;
    }

    public Conditional parse(final String str) throws ParseException {
        pathExpressions = new ArrayList<PathExpression>();
        conditions = new ArrayList<Condition>();
        models = new ArrayList<String>();
        models.add(AJModel.ID);
        final AQLLexer lexer = new AQLLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final AQLParser parser = new AQLParser(tokens);
        final ParserRuleContext tree = parser.whereClause();
        final ParseTreeWalker walker = new ParseTreeWalker();
        try {
            walker.walk(this, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        return new Conditional(conditions, pathExpressions, models);
    }

    @Override
    public void enterWhereCondEquality(AQLParser.WhereCondEqualityContext ctx) {
        LOGGER.debug("Entering WhereCondEquality: " + ctx.getText());
        try {
            conditions.add(conditionListener.parse(AntlrUtils.getFullText(ctx)));
        } catch (ParseException e) {
            throw new CompletionException(e);
        }
    }

    @Override
    public void enterWhereCondSome(AQLParser.WhereCondSomeContext ctx) {
        LOGGER.debug("Entering WhereCondSome: " + ctx.getText());
        try {
            pathExpressions
                    .addAll(conditionalStructuralListener.parse(AntlrUtils.getFullText(ctx)).getPathExpressions());
        } catch (ParseException e) {
            throw new CompletionException(e);
        }
    }
}
