/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.aj;

import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import hadad.base.compiler.AntlrUtils;
import hadad.base.compiler.VariableFactory;
import hadad.base.compiler.VariableMapper;
import hadad.base.compiler.exceptions.ParseException;
import hadad.commons.conjunctivequery.Variable;

/**
 * AJ Conditional Structural Listener which extends {@link StructuralBaseListener}
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
@Singleton
final class ConditionalStructuralListener extends StructuralBaseListener {
    private static final Logger LOGGER = Logger.getLogger(ConditionalStructuralListener.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }

    @Inject
    public ConditionalStructuralListener(final PathExpressionListener listener,
            @Named("AQLVariableFactory") VariableFactory aqlVariableFactory, VariableMapper variableMapper) {
        super(listener, aqlVariableFactory, variableMapper);
    }

    @Override
    public void enterWhereCondBindingVar(AQLParser.WhereCondBindingVarContext ctx) {
        LOGGER.debug("Entering WhereCondBindingVar: " + ctx.getText());
        if (currentVar != null) {
            throw new IllegalStateException("Path expression expected.");
        }
        final Variable var = aqlVariableFactory.createFreshVar();
        variableMapper.define(ctx.getText(), var);
        currentVar = var;
    }

    @Override
    public void enterWhereCondBindingSource(AQLParser.WhereCondBindingSourceContext ctx) {
        LOGGER.debug("Entering WhereCondBindingSource: " + ctx.getText());
        try {
            defineVariable(pathExpressionListener.parse(AntlrUtils.getFullText(ctx)).copy(currentVar));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    protected ParserRuleContext createParseTree(final AQLParser parser) {
        return parser.whereCondSome();
    }
}
