/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.aj;

import java.util.concurrent.CompletionException;

import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import hadad.base.compiler.AntlrUtils;
import hadad.base.compiler.VariableFactory;
import hadad.base.compiler.VariableMapper;
import hadad.base.compiler.exceptions.ParseException;
import hadad.commons.conjunctivequery.Variable;

/**
 * AJ StructuralListener which extends {@link StructuralBaseListener}.
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
@Singleton
public final class StructuralListener extends StructuralBaseListener {
    private static final Logger LOGGER = Logger.getLogger(StructuralListener.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }

    @Inject
    public StructuralListener(final PathExpressionListener listener,
            @Named("AQLVariableFactory") VariableFactory aqlVariableFactory, VariableMapper variableMapper) {
        super(listener, aqlVariableFactory, variableMapper);
        LOGGER.setLevel(Level.OFF);
    }

    @Override
    public void enterForClauseBindingVar(AQLParser.ForClauseBindingVarContext ctx) {
        LOGGER.debug("Entering ForClauseBindingVar: " + ctx.getText());
        if (currentVar != null) {
            throw new IllegalStateException("Path expression expected.");
        }
        final Variable var = aqlVariableFactory.createFreshVar();
        variableMapper.define(ctx.getText(), var);
        currentVar = var;
    }

    @Override
    public void enterForClauseBindingSource(AQLParser.ForClauseBindingSourceContext ctx) {
        LOGGER.debug("Entering ForClauseBindingSource: " + ctx.getText());
        try {
            defineVariable(pathExpressionListener.parse(AntlrUtils.getFullText(ctx)).copy(currentVar));
        } catch (ParseException e) {
            throw new CompletionException(e);
        }
    }

    @Override
    protected ParserRuleContext createParseTree(final AQLParser parser) {
        return parser.forClause();
    }
}
