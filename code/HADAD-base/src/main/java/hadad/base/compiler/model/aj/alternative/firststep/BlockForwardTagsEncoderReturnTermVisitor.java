/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.aj.alternative.firststep;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Singleton;

import hadad.base.compiler.ReturnConstructTerm;
import hadad.base.compiler.ReturnStringTerm;
import hadad.base.compiler.ReturnTemplate;
import hadad.base.compiler.IReturnTemplateVisitor;
import hadad.base.compiler.ReturnVariableTerm;
import hadad.base.compiler.model.aj.Predicate;
import hadad.commons.conjunctivequery.Atom;

@Singleton
class BlockForwardTagsEncoderReturnTermVisitor implements IReturnTemplateVisitor {
    private ImmutableList.Builder<Atom> builder;
    private String viewName;

    public List<Atom> encode(final ReturnTemplate template, final String viewName) {
        builder = ImmutableList.builder();
        this.viewName = viewName;
        template.accept(this);
        return builder.build();
    }

    @Override
    public void visit(final ReturnTemplate template) {
        // NOP (no encoding for the template optionals)
    }

    @Override
    public void visitPost(final ReturnConstructTerm term) {
        // NOP (no encoding after the children are processed)
    }

    @Override
    public void visitPre(final ReturnConstructTerm term) {
        if (!term.getElement().isEmpty()) {
            builder.add(new Atom(Predicate.TAG.toString() + "_" + viewName, term.getCreatedNode(),
                    term.getElement().toTerm()));
        }
    }

    @Override
    public void visit(final ReturnVariableTerm term) {

    }

    @Override
    public void visit(final ReturnStringTerm term) {

    }
}