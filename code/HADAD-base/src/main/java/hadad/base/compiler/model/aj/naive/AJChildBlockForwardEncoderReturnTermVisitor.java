/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.aj.naive;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.IElement;
import hadad.base.compiler.ReturnTemplate;
import hadad.base.compiler.ReturnTerm;
import hadad.base.compiler.StringElement;
import hadad.base.compiler.model.aj.ArrayElementFactory;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.Variable;

@Singleton
public class AJChildBlockForwardEncoderReturnTermVisitor extends AJBaseForwardEncoderReturnTermVisitor {
    private Variable parentBlockCreatedNode;

    @Inject
    public AJChildBlockForwardEncoderReturnTermVisitor(final ArrayElementFactory arrayElementFactory) {
        super(arrayElementFactory);
    }

    public List<Atom> encode(final Variable parentBlockCreatedNode, final ReturnTemplate template,
            final String viewName) {
        builder = ImmutableList.builder();
        this.parentBlockCreatedNode = parentBlockCreatedNode;
        this.viewName = viewName;
        template.accept(this);
        return builder.build();
    }

    @Override
    protected IElement getParentBlockElement(final ReturnTerm term) {
        if (term.hasParent()) {
            if (!term.getParent().getElement().isEmpty()) {
                return term.getParent().getElement();
            } else {
                if (term.getParent().hasParent() && !term.getParent().getParent().getElement().isEmpty()) {
                    return term.getParent().getParent().getElement();
                }
            }
        }
        return new StringElement("");
    }

    @Override
    protected Variable getParentBlockCreatedNode(final ReturnTerm term) {
        if (term.hasParent()) {
            if (!term.getParent().getElement().isEmpty()) {
                return term.getParent().getCreatedNode();
            } else {
                if (term.getParent().hasParent() && !term.getParent().getParent().getElement().isEmpty()) {
                    return term.getParent().getParent().getCreatedNode();
                }
            }
        }
        return parentBlockCreatedNode;
    }
}