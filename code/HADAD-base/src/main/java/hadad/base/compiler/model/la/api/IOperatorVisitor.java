/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.api;

import hadad.base.compiler.model.la.builder.operators.BinaryOperator;
import hadad.base.compiler.model.la.builder.operators.ScalarOperator;
import hadad.base.compiler.model.la.builder.operators.ScanOperator;
import hadad.base.compiler.model.la.builder.operators.UnaryOperator;

/**
 * 
 * LA Plan Visitor
 * 
 * @author ranaalotaibi
 *
 * @param <R>
 *            return value
 * @param <A>
 *            argument value
 */

public interface IOperatorVisitor<R, A> {

    R visit(UnaryOperator unaryOperator, A arg);

    R visit(ScanOperator unaryOperator, A arg);

    R visit(BinaryOperator unaryOperator, A arg);

    R visit(ScalarOperator unaryOperator, A arg);

}
