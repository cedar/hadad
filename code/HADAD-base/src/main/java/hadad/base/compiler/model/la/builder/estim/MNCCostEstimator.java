/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.builder.estim;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.exceptions.HadadException;
import hadad.base.compiler.model.la.api.IOperatorVisitor;
import hadad.base.compiler.model.la.builder.operators.BinaryOperator;
import hadad.base.compiler.model.la.builder.operators.OpType;
import hadad.base.compiler.model.la.builder.operators.ScalarOperator;
import hadad.base.compiler.model.la.builder.operators.ScanOperator;
import hadad.base.compiler.model.la.builder.operators.UnaryOperator;
import hadad.base.utils.UtilsLoadHistogram;

/**
 * MCN-based cost estimator using {@link SparsityEstimatorMCN}
 * 
 * @author ranaalotaibi
 */
public class MNCCostEstimator extends Estimator implements IOperatorVisitor<MatrixHistogram, Void> {
    private static final Logger LOGGER = Logger.getLogger(MNCCostEstimator.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }

    /** Constructor **/
    public MNCCostEstimator() {
        totalCost = 0;
    }

    @Override
    public MatrixHistogram visit(final UnaryOperator unaryOperator, Void e) {
        try {
            final MatrixHistogram h1 = unaryOperator.getChildOp().accept(this, e);
            if (h1 != null) {
                switch (unaryOperator.getOpType()) {
                    case TR:
                        totalCost += SparsityEstimatorMCN.estimIntern(h1, null, OpType.TR);
                        return MatrixHistogramUtils.deriveOutputHistogram(h1, null, 0, OpType.TR);
                    case IN:
                        totalCost += SparsityEstimatorMCN.estimIntern(h1, null, OpType.IN);
                        return MatrixHistogramUtils.deriveOutputHistogram(h1, null, 0, OpType.IN);
                    case EXP:
                        totalCost += SparsityEstimatorMCN.estimIntern(h1, null, OpType.EXP);
                        return MatrixHistogramUtils.deriveOutputHistogram(h1, null, 0, OpType.EXP);
                    case ROWSUMS:
                        totalCost += SparsityEstimatorMCN.estimIntern(h1, null, OpType.ROWSUMS);
                        return MatrixHistogramUtils.deriveOutputHistogram(h1, null, 0, OpType.ROWSUMS);
                    case COLSUMS:
                        totalCost += SparsityEstimatorMCN.estimIntern(h1, null, OpType.COLSUMS);
                        return MatrixHistogramUtils.deriveOutputHistogram(h1, null, 0, OpType.COLSUMS);
                    case TRACE:
                    case DET:
                    case INS:
                    case SUM:
                        totalCost += 0;
                    default:
                        return null;
                }
            }

        } catch (HadadException e1) {
            throw new RuntimeException(e1);
        }
        return null;
    }

    @Override
    public MatrixHistogram visit(final ScanOperator scanOperator, Void e) {
        MatrixHistogram h1 = null;
        try {
            h1 = UtilsLoadHistogram.get(scanOperator.getBaseMatrixName());
        } catch (HadadException e1) {
            throw new RuntimeException(e1);
        }
        return h1;
    }

    @Override
    public MatrixHistogram visit(final BinaryOperator binaryOperator, Void e) {
        try {
            final MatrixHistogram h1 = binaryOperator.getChildOpLeft().accept(this, e);
            final MatrixHistogram h2 = binaryOperator.getChildOpRight().accept(this, e);
            switch (binaryOperator.getOpType()) {
                case MULTI:
                    double nnz = SparsityEstimatorMCN.estimIntern(h1, h2, OpType.MULTI);
                    totalCost += nnz;
                    double spOut = nnz / ((double) h1.getRows() * h2.getCols());
                    return MatrixHistogramUtils.deriveOutputHistogram(h1, h2, spOut, OpType.MULTI);
                case ADD:
                    totalCost += SparsityEstimatorMCN.estimIntern(h1, h2, OpType.ADD);
                    return MatrixHistogramUtils.deriveOutputHistogram(h1, h2, 0, OpType.ADD);
                case SUB:
                    totalCost += SparsityEstimatorMCN.estimIntern(h1, h2, OpType.SUB);
                    return MatrixHistogramUtils.deriveOutputHistogram(h1, h2, 0, OpType.SUB);
                case MULTI_S:
                    totalCost += SparsityEstimatorMCN.estimIntern(h1, h2, OpType.MULTI_S);
                    return MatrixHistogramUtils.deriveOutputHistogram(h1, h2, 0, OpType.MULTI_S);
                case MULTI_E:
                    totalCost += SparsityEstimatorMCN.estimIntern(h1, h2, OpType.MULTI_E);
                    return MatrixHistogramUtils.deriveOutputHistogram(h1, h2, 0, OpType.MULTI_E);
                case MULTI_C:
                case ADD_S:
                    totalCost += 0; // NO intermediate results.

                default:
                    return null;
            }
        } catch (HadadException e1) {
            throw new RuntimeException(e1);
        }
    }

    @Override
    public MatrixHistogram visit(final ScalarOperator scalarOperator, Void arg) {
        return null;
    }
}
