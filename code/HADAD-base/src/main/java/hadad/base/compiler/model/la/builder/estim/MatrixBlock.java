/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**
 *  Based on MatrixBlock.java from  https://github.com/apache/systemds.
 */
package hadad.base.compiler.model.la.builder.estim;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.log4j.Logger;

/**
 * Matrix block to be used by {@link MatrixHistogram} for histogram computation.
 * Simplified version of SystemML Matrix Block implementation.
 */
public class MatrixBlock {
    private static final Logger LOGGER = Logger.getLogger(MatrixBlock.class);

    /** data (matrix) **/
    private double[] data;
    /** number of rows **/
    private int nRows;
    /** Number of columns **/
    private int nCols;
    /** Number of non-zeros **/
    private int nnz;

    /**
     * Constructor
     * 
     * @param data
     *            matrix data
     * 
     */
    public MatrixBlock(final double[] data, final int nRows, final int nCols) {
        this.data = checkNotNull(data);
        this.nRows = nRows;
        this.nCols = nCols;
        this.nnz = 0;
        computeNonZero();
    }

    /**
     * Compute non-zeros
     */
    private void computeNonZero() {
        for (int i = 0; i < nRows * nCols; i++) {
            nnz += (data[i] != 0) ? 1 : 0;
        }
    }

    /**
     * Compute non-zero per row
     * 
     * @param rowNumber
     *            the row number
     * @return the number of non-zero per row.
     */
    public int computeNonZero(int rowNumber) {
        int lnnz = 0;
        int start = rowNumber * nCols;
        for (int i = start; i < start + nCols; i++)
            lnnz += (data[i] != 0) ? 1 : 0;
        return lnnz;
    }

    /**
     * Compute non-zero per column
     * 
     * @param columnNumber
     *            the column number
     * @return the number of non-zero per column.
     */
    public int computeNonZeroColumn(int columnNumber) {
        int lnnz = 0;
        for (int i = columnNumber; i < nRows * nCols; i += nCols) {
            lnnz += (data[i] != 0) ? 1 : 0;
        }
        return lnnz;
    }

    /**
     * Get number of rows
     * 
     * @return number of rows
     */
    public int getNumRows() {
        return nRows;
    }

    /**
     * Get number of columns
     * 
     * @return number of columns
     */
    public int getNumColumns() {
        return nCols;
    }

    /**
     * Get non-zeros
     * 
     * @return non-number of zeros
     */
    public int getNonZeros() {
        return nnz;
    }

    /**
     * Get data size
     * 
     * @return data size
     */
    public long getLength() {
        return data.length;
    }

    /**
     * Check if data (matrix) is empty
     * 
     * @return <code>True </code> if data is not empty. Otherwise returns <code> False </code>
     */
    public boolean isEmpty() {
        return nnz == 0;
    }

    /**
     * Starting position
     * 
     * @param rowNumber
     *            the row number
     * @return starting position.
     */
    public int pos(int rowNumber) {
        return rowNumber * nCols;
    }

    /**
     * Return row values
     * 
     * @param rowNumber
     *            row numbers
     * @return the row values
     */
    public double[] values(int rowNumber) {
        double[] rowValues = new double[nCols];
        int ix = pos(rowNumber);
        for (int j = 0; j < nCols; j++)
            rowValues[j] = data[ix + j];
        return rowValues;
    }

    public double get(int r, int c) {
        return data[r * nCols + c];
    }

}
