/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.builder.estim;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.model.la.api.IOperatorVisitor;
import hadad.base.compiler.model.la.builder.operators.BinaryOperator;
import hadad.base.compiler.model.la.builder.operators.IOperator;
import hadad.base.compiler.model.la.builder.operators.OpType;
import hadad.base.compiler.model.la.builder.operators.ScalarOperator;
import hadad.base.compiler.model.la.builder.operators.ScanOperator;
import hadad.base.compiler.model.la.builder.operators.UnaryOperator;
import hadad.base.compiler.model.la.metadata.Metadata;

/**
 * Naive-based cost-estimator using naive worse case
 * {@link SparsityEstimatorNaive}. Adopted from SystemML.
 */
public class NaiveCostEstimator extends Estimator implements IOperatorVisitor<Void, Void> {
    private static final Logger LOGGER = Logger.getLogger(NaiveCostEstimator.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }

    /** Constructor **/
    public NaiveCostEstimator() {
        super();
    }

    @Override
    public Void visit(final UnaryOperator unaryOperator, Void e) {
        IOperator childOp = unaryOperator.getChildOp();
        childOp.accept(this, e);
        double nnz;
        if (unaryOperator.getOpType().equals(OpType.ROWSUMS)) {
            nnz = SparsityEstimatorNaive.estimateUnary(unaryOperator.getOpType(), childOp.getNnz(),
                    childOp.getNumberOfRows());
        } else {
            if (unaryOperator.getOpType().equals(OpType.COLSUMS)) {
                nnz = SparsityEstimatorNaive.estimateUnary(unaryOperator.getOpType(), childOp.getNnz(),
                        childOp.getNumberOfColumns());
            } else {
                nnz = SparsityEstimatorNaive.estimateUnary(unaryOperator.getOpType(), childOp.getNnz(), 0);
            }
        }
        unaryOperator.setNnz(nnz);
        totalCost += nnz;
        return e;

    }

    @Override
    public Void visit(final ScanOperator scanOperator, Void e) {
        double nnz = SparsityEstimatorNaive.estimateBase(scanOperator.getNumberOfRows(),
                scanOperator.getNumberOfColumns(), Metadata.instance.getNNZ(scanOperator.getBaseMatrixName()));
        scanOperator.setNnz(nnz);
        return e;

    }

    @Override
    public Void visit(final ScalarOperator scalarOperator, Void arg) {
        return null;
    }

    @Override
    public Void visit(final BinaryOperator binaryOperator, Void e) {
        binaryOperator.getChildOpLeft().accept(this, e);
        binaryOperator.getChildOpRight().accept(this, e);
        double nnz =
                SparsityEstimatorNaive.estimateBinary(binaryOperator.getOpType(), binaryOperator.getNumberOfColumns(),
                        binaryOperator.getChildOpLeft().getNnz(), binaryOperator.getChildOpRight().getNnz());
        binaryOperator.setNnz(nnz);
        totalCost += nnz;
        return e;
    }

}
