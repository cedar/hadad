/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.builder.estim;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.model.la.builder.operators.OpType;

/**
 * Worse case sparsity estimator for supported LA operations.
 * 
 * @author ranaalotaibi
 */
public final class SparsityEstimatorNaive {
    private static final Logger LOGGER = Logger.getLogger(SparsityEstimatorNaive.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }

    /** Private constructor **/
    private SparsityEstimatorNaive() {
    };

    /**
     * Binary operator estimate
     *
     * @param opType
     *            operator type
     * @param nCols
     *            number of cols
     * @param nnzLeft
     *            nnz left
     * @param nnzRight
     *            nnz right
     * @return estimated sparsity
     */
    public static double estimateBinary(final OpType opType, int nCols, double nnzLeft, double nnzRight) {
        switch (opType) {
            case ADD:
                return Math.min(1, (nnzLeft + nnzRight));
            case ADD_S:
                return 0;
            case MULTI:
                return (Math.min(1, nnzLeft * nCols) * Math.min(1, nnzRight * nCols));
            case MULTI_E:
                return Math.min(nnzLeft, nnzRight);
            case MULTI_S:
                return nnzRight;
            case DIV:
                return Math.min(nnzLeft, nnzRight);
            case DIVS:
                return nnzRight;
            default:
                return 0;
        }
    }

    /**
     * Unary operator estimate
     * 
     * @param opType
     *            operator type
     * @param nnz
     *            number of non-zero
     * @param aggDim
     *            aggregate dim if any
     * @return estimated sparsity
     */
    public static double estimateUnary(final OpType opType, double nnz, double aggDim) {
        switch (opType) {
            case TR:
                return nnz;
            case EXP: // TODO: Check exponential
                return nnz;
            case INS:
            case SUM:
            case TRACE:
            case DET:
                return 0;
            case ROWSUMS:
            case COLSUMS:
                return Math.min(1, aggDim * nnz);
            case IN:
                return nnz;
            default:
                return 0;
        }
    }

    /**
     * Base matrix sparsity estimate
     * 
     * @param nRows
     *            number of rows
     * @param nCols
     *            number of cols
     * @param nnz
     *            number of nnz
     * @return sparsity
     */
    public static double estimateBase(int nRows, int nCols, double nnz) {
        return nnz / (nCols * nRows);
    }
}
