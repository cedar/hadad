/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.builder.operators;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.model.la.api.IOperatorVisitor;

/**
 * Represents an LA Unary operator
 * 
 * @author ranaalotaibi
 */
public class BinaryOperator extends IOperator {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(BinaryOperator.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }
    /** Left child **/
    private final IOperator childOpLeft;
    /** Right child **/
    private final IOperator childOpRight;

    /**
     * Constructor
     * 
     * @param childOpLeft
     *            left child operator
     * @param childOpRight
     *            right child operator
     * @param opType
     *            operator type
     */
    public BinaryOperator(final IOperator childOpLeft, final IOperator childOpRight, final OpType opType) {
        super();
        this.childOpLeft = checkNotNull(childOpLeft);
        this.childOpRight = checkNotNull(childOpRight);
        this.opType = checkNotNull(opType);
        childOpLeft.setParentOperator(this);
        childOpRight.setParentOperator(this);
        deriveOutputDimension();
    }

    /**
     * Get the child left operator
     * 
     * @return the child operator
     */
    public IOperator getChildOpLeft() {
        return childOpLeft;
    }

    /**
     * Get the child right operator
     * 
     * @return the child operator
     */
    public IOperator getChildOpRight() {
        return childOpRight;
    }

    private void deriveOutputDimension() {
        switch (opType) {
            case MULTI:
                this.nRows = childOpLeft.nRows;
                this.nCols = childOpRight.nCols;
                break;
            case ADD:
                this.nRows = childOpLeft.nRows;
                this.nCols = childOpLeft.nCols;
                break;
            case MULTI_S:
                this.nRows = childOpLeft.nRows;
                this.nCols = childOpLeft.nCols;
                break;
            case MULTI_E:
                this.nRows = childOpLeft.nRows;
                this.nCols = childOpLeft.nCols;
                break;
            case DIV:
                this.nRows = childOpLeft.nRows;
                this.nCols = childOpLeft.nCols;
                break;
            case DIVS:
                this.nRows = childOpLeft.nRows;
                this.nCols = childOpLeft.nCols;
                break;
            case ADD_S:
                this.nRows = 0;
                this.nCols = 0;
                break;
            case MULTI_C:
                this.nRows = 0;
                this.nCols = 0;
                break;
            case NORMALIZEDMATRIX:
                this.nRows = childOpLeft.nRows;
                this.nCols = childOpRight.nRows;
            default:
                break;
        }
    }

    @Override
    public String toString() {
        final StringBuffer str = new StringBuffer();
        str.append(opType);
        str.append(":\n");
        str.append("childOpLeft:{\n");
        str.append(childOpLeft.toString());
        str.append("\n");
        str.append("}\n");
        str.append("childOpRight:{\n");
        str.append(childOpRight.toString());
        str.append("\n");
        str.append("}");
        return str.toString();
    }

    @Override
    public <R, A> R accept(IOperatorVisitor<R, A> visitor, A arg) {
        return visitor.visit(this, arg);
    }

}