/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.builder.operators;

import java.io.Serializable;

import hadad.base.compiler.model.la.api.IVisitable;

/**
 * Represents LA operator as a Unary or Binary operator
 * 
 * @author ranaalotaibi
 *
 */
public abstract class IOperator implements Serializable, IVisitable {

    private static final long serialVersionUID = 1L;

    /** The parent of the current operator **/
    protected IOperator parentOp;
    /** Operator type **/
    protected OpType opType;
    /** Non-zero estimate for the result of each operator **/
    protected double nNZ;
    /** Row size **/
    protected int nRows;
    /** Column size **/
    protected int nCols;

    /** Constructor **/
    public IOperator() {
        nNZ = 0;
    }

    /**
     * Set parent operator
     * 
     * @param parentOp
     *            the parent operator
     */
    public void setParentOperator(final IOperator parentOp) {
        this.parentOp = parentOp;
    }

    /**
     * Set the non-zero estimate
     * 
     * @param nNZ
     *            the non-zero estimate
     */
    public void setNnz(final double nNZ) {
        this.nNZ = nNZ;
    }

    /**
     * Set the number of rows
     * 
     * @param nRows
     *            the number of rows
     */
    public void setNumberOfRows(final int nRows) {
        this.nRows = nRows;
    }

    /**
     * Set the number of columns
     * 
     * @param nCols
     *            the number of columns
     */
    public void setNumberOfCols(final int nCols) {
        this.nCols = nCols;
    }

    /**
     * Get non-zero count
     * 
     * @return non-zero count
     */
    public double getNnz() {
        return nNZ;
    }

    /**
     * Get number of rows
     * 
     * @return number of rows
     */
    public int getNumberOfRows() {
        return nRows;
    }

    /**
     * Get number of columns
     * 
     * @return number of columns
     */
    public int getNumberOfColumns() {
        return nCols;
    }

    /**
     * Get operator type
     * 
     * @return operator type
     */
    public OpType getOpType() {
        return opType;
    }

    @Override
    public String toString() {
        final StringBuffer str = new StringBuffer();
        str.append(parentOp.toString());
        return str.toString();
    }

}