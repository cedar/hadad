/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.builder.operators;

/**
 * Supported operators
 * 
 * @author ranaalotaibi
 *
 */
public enum OpType {

    ADD("add"),
    SUB("sub"),
    ADD_S("add_s"),
    MULTI("multi"),
    MULTI_S("multi_s"),
    MULTI_E("multi_e"),
    MULTI_C("multi_c"),
    DIV("div"),
    DIVS("div_s"),
    TR("tr"),
    IN("in"),
    INS("in_s"),
    EXP("exp"),
    DET("det"),
    TRACE("trace"),
    SUM("sum"),
    COLSUMS("colsums"),
    ROWSUMS("rowsums"),
    SUBSET("subset"),
    NORMALIZEDMATRIX("normalizedMatrix");

    private String opType;

    OpType(final String opType) {
        this.opType = opType;
    }

    public String getOpType() {
        return opType;
    }
}
