/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.builder.operators;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.model.la.api.IOperatorVisitor;

/**
 * LA Scan operator
 * 
 * @author ranaalotaibi
 */
public class ScanOperator extends IOperator {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(ScanOperator.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }
    /** Base matrix name **/
    final String baseMatrixName;

    /**
     * Constructor
     * 
     * @param baseMatrixName
     *            matrix name
     */
    public ScanOperator(final String baseMatrixName) {
        super();
        this.baseMatrixName = checkNotNull(baseMatrixName);
    }

    /**
     * Get base matrix name
     * 
     * @return base matrix name
     */
    public String getBaseMatrixName() {
        return baseMatrixName;
    }

    @Override
    public String toString() {
        final StringBuffer str = new StringBuffer();
        str.append("Scan:{\n");
        str.append(baseMatrixName);
        str.append("\n");
        str.append("}");
        return str.toString();
    }

    @Override
    public <R, A> R accept(IOperatorVisitor<R, A> visitor, A arg) {
        return visitor.visit(this, arg);
    }
}
