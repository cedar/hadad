/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.builder.operators;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.model.la.api.IOperatorVisitor;

/**
 * Represents an LA Unary operator
 * 
 * @author ranaalotaibi
 *
 */
public class UnaryOperator extends IOperator {
    private static final long serialVersionUID = 1L;
    private static final Logger LOGGER = Logger.getLogger(UnaryOperator.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }

    /** Child operator **/
    private final IOperator childOp;

    public UnaryOperator(final IOperator childOp, final OpType opType) {
        super();
        this.childOp = checkNotNull(childOp);
        this.opType = checkNotNull(opType);
        childOp.setParentOperator(this);
        deriveOutputDimension();
    }

    /**
     * Get the child operator
     * 
     * @return the child operator
     */
    public IOperator getChildOp() {
        return childOp;
    }

    /**
     * Derive output size
     */
    private void deriveOutputDimension() {
        switch (opType) {
            case TR:
                this.nRows = childOp.nCols;
                this.nCols = childOp.nRows;
                break;
            case IN:
            case INS:
            case EXP:
            case SUBSET:
                this.nRows = childOp.nRows;
                this.nCols = childOp.nCols;
            default:
                break;
        }
    }

    @Override
    public String toString() {
        final StringBuffer str = new StringBuffer();
        str.append(opType);
        str.append(":\n");
        str.append("childOp:{\n");
        str.append(childOp.toString());
        str.append("\n");
        str.append("}");
        return str.toString();
    }

    @Override
    public <R, A> R accept(IOperatorVisitor<R, A> visitor, A arg) {
        return visitor.visit(this, arg);
    }

}