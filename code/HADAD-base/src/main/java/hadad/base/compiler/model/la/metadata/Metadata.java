/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.metadata;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import hadad.base.compiler.exceptions.MetadataException;
import hadad.base.utils.Tuple;

/**
 * Utils for metadata
 * 
 * @author ranaalotaibi
 */
public final class Metadata {
    private static final Logger LOGGER = Logger.getLogger(Metadata.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }
    public static Metadata instance;
    private static final Charset CHARSET = Charsets.UTF_8;
    private final Map<String, Tuple<Integer, Integer, Double, String>> metadata;

    private static String DEFAULT_PATH = "src/main/resources/metadata.json";

    private Metadata() {
        metadata = new HashMap<>();
    }

    public static void MetadataInit(final String path) {
        if (path != null) {
            DEFAULT_PATH = path;
        }

    }

    /**
     * Get metadata insance
     * 
     * @throws MetadataException
     *             metadata exception
     */
    public static void getInstance() throws MetadataException {
        if (instance == null) {
            final File file = new File(DEFAULT_PATH);
            if (file.exists() && file.length() > 0) {
                instance = load();
            } else {
                instance = new Metadata();
                try {
                    write(instance);
                } catch (IOException e) {
                    throw new MetadataException(e.getMessage());
                }
            }
        }
    }

    /**
     * Attempts to read, JSON-decode,
     * and return the contents of a metadata
     * 
     * @return loaded metadata
     * @throws MetadataException
     *             metadata exception
     */
    public static Metadata load() throws MetadataException {
        final Gson gson = createGson();
        String json = null;
        try {
            json = Files.toString(new File(DEFAULT_PATH), CHARSET);
        } catch (IOException e) {
            throw new MetadataException(e);
        }
        final Type type = new TypeToken<Metadata>() {
        }.getType();

        return gson.fromJson(json, type);
    }

    /**
     * Receives a catalog and attempts to JSON-encode it and write it into a
     * file
     * 
     * @param metadata
     *            the metadata instance
     * @throws IOException
     *             IOException
     */
    public static void write(final Metadata metadata) throws IOException {
        final Gson gson = createGson();
        final String json = gson.toJson(metadata);
        try {
            Files.write(json, new File(DEFAULT_PATH), CHARSET);
        } catch (IOException e) {
            throw e;
        }
    }

    /**
     * Attempts to JSON-encode and write a metadata into a file
     * 
     * @throws IOException
     *             IOException
     */
    public void write() throws IOException {
        try {
            write(this);
        } catch (IOException e) {
            throw e;
        }
    }

    /**
     * Adds a new item to the catalog
     * 
     * @param matrixname
     *            matrix file name
     * @param nrows
     *            number of rows
     * @param ncol
     *            number of cols
     * @param nnz
     *            number of non-zero
     * @param histogramPath
     *            matrix histogram path
     * @throws MetadataException
     *             metadata exception
     */
    public void add(final String matrixname, int nrows, int ncol, double nnz, String histogramPath)
            throws MetadataException {

        metadata.put(matrixname, new Tuple<Integer, Integer, Double, String>(nrows, ncol, nnz, histogramPath));
        try {
            write();
        } catch (IOException e) {
            LOGGER.error("Couldn't write to the catalog file!", e);
            throw new MetadataException(e);

        }
    }

    /**
     * Adds a new item to the catalog
     * 
     * @param matrixname
     *            matrix file name
     * @param histogramPath
     *            matrix histogram path
     * @throws MetadataException
     *             metadata exception
     */
    public void update(final String matrixname, String histogramPath) throws MetadataException {
        Tuple<Integer, Integer, Double, String> temp = metadata.get(matrixname);
        metadata.put(matrixname,
                new Tuple<Integer, Integer, Double, String>(temp.first(), temp.second(), temp.third(), histogramPath));
        try {
            write();
        } catch (IOException e) {
            LOGGER.error("Couldn't write to the catalog file!", e);
            throw new MetadataException(e);
        }
    }

    /** Deletes the catalog */
    public void delete() {
        final File file = new File(DEFAULT_PATH);
        if (file.exists()) {
            file.delete();
        }
        metadata.clear();
    }

    /** Searches the catalog for a specified string key */
    /**
     * Searches the catalog for a specified string key
     * 
     * @param key
     *            the key
     * @return the value of the given key
     */
    public boolean contains(String key) {
        return metadata.containsKey(key);
    }

    @Override
    public String toString() {
        return metadata.toString();
    }

    /**
     * Get created gson
     * 
     * @return created gson
     */
    private static Gson createGson() {
        return new GsonBuilder().create();
    }

    /**
     * Get nrows
     * 
     * @param key
     *            name of the matrix
     * @return number of rows
     */
    public int getNRows(final String key) {
        if (metadata.get(key) != null)
            return metadata.get(key).first();
        else {
            return -1;
        }
    }

    /**
     * Get ncols
     * 
     * @param key
     *            name of the matrix
     * @return number of ncols
     */
    public int getNCols(String key) {
        if (metadata.get(key) != null)
            return metadata.get(key).second();
        else {
            return -1;
        }
    }

    /**
     * Get nnz
     * 
     * @param key
     *            name of the matrix
     * @return get number of non-zeros
     */
    public double getNNZ(String key) {
        if (metadata.get(key) != null)
            return metadata.get(key).third();
        else {
            return -1;
        }

    }

    /**
     * Get matrix histgoram path
     * 
     * @param key
     *            name of the matrix
     * @return the histgoram path
     */
    public String getHistogramPath(String key) {
        return metadata.get(key).fourth();
    }
}
