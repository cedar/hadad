/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.mr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.AntlrUtils;
import hadad.base.compiler.Block;
import hadad.base.compiler.ChildBlock;
import hadad.base.compiler.Pattern;
import hadad.base.compiler.ReturnConstructTerm;
import hadad.base.compiler.IReturnLeafTerm;
import hadad.base.compiler.ReturnTemplate;
import hadad.base.compiler.ReturnTerm;
import hadad.base.compiler.RootBlock;
import hadad.base.compiler.StringElement;
import hadad.base.compiler.VariableElement;
import hadad.base.compiler.exceptions.ParseException;
import hadad.base.parsers.MRBaseListener;
import hadad.base.parsers.MRLexer;
import hadad.base.parsers.MRParser;
import hadad.commons.conjunctivequery.Variable;

/**
 * MR BlockListener
 * 
 * @author ranaalotaibi
 */
@Singleton
final class BlockListener {
    private static final Logger LOGGER = Logger.getLogger(BlockListener.class.getName());

    static {
        LOGGER.setLevel(Level.OFF);
    }
    private final PatternListener patternListener;
    @SuppressWarnings("unused")
    private final ReturnTemplateListener returnTemplateListener;

    @Inject
    public BlockListener(final PatternListener patternListener, final ReturnTemplateListener returnTemplateListener) {
        this.patternListener = patternListener;
        this.returnTemplateListener = returnTemplateListener;
        LOGGER.setLevel(Level.OFF);
    }

    /**
     * Parse R script
     * 
     * @param queryName
     *            a query name
     * @param strQuery
     *            R script
     * @return rootblock constructed root block
     * @throws ParseException
     *             parse exception
     */
    public RootBlock parse(final String queryName, final String strQuery) throws ParseException {
        final BlockListenerAux listener = _parse(strQuery);
        final Pattern pattern = patternListener.parse(listener.getPattern());

        return new RootBlock(queryName, pattern,
                constructReturnTemplate(pattern.getStructural().getDefinedVariables()));
    }

    public ChildBlock parse(final Block parent, final String createdNode, final String str) throws ParseException {
        final BlockListenerAux listener = _parse(str);
        final Pattern pattern = patternListener.parse(listener.getPattern());
        return new ChildBlock(parent, createdNode, pattern,
                constructReturnTemplate(pattern.getStructural().getDefinedVariables()));
    }

    /**
     * Construct return template for R script
     * 
     * @param patternVars
     *            the variables introduced in the pattern structural (ordered)
     * @return return template
     */
    private ReturnTemplate constructReturnTemplate(final List<Variable> patternVars) {
        final List<ReturnTerm> terms = new ArrayList<ReturnTerm>();
        final Variable var = patternVars.get(patternVars.size() - 1);
        final ReturnConstructTerm term = new ReturnConstructTerm(
                new ReturnConstructTerm(new VariableElement(var), new HashMap<String, IReturnLeafTerm>()),
                new StringElement(var.getName()), new HashMap<String, IReturnLeafTerm>());
        terms.add(term);
        return new ReturnTemplate(MRModel.ID, terms);
    }

    private BlockListenerAux _parse(final String str) throws ParseException {
        final MRLexer lexer = new MRLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final MRParser parser = new MRParser(tokens);
        final ParserRuleContext tree = parser.mrScript();
        final ParseTreeWalker walker = new ParseTreeWalker();
        final BlockListenerAux listener = new BlockListenerAux();
        try {
            walker.walk(listener, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        if (listener.getPattern() == null) {
            throw new ParseException(new IllegalStateException("Pattern expected."));
        }
        return listener;
    }

    private class BlockListenerAux extends MRBaseListener {
        private String pattern;

        /**
         * Get the pattern
         * 
         * @return pattern the constructed pattern
         */
        public String getPattern() {
            return pattern;
        }

        @Override
        public void enterMrScript(MRParser.MrScriptContext ctx) {
            if (pattern == null) {
                pattern = AntlrUtils.getFullText(ctx);
            }
        }
    }
}
