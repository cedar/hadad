/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.mr;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Properties;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

import hadad.base.compiler.NaiveRewritingSelector;
import hadad.base.compiler.IQueryBlockTreeBuilder;
import hadad.base.compiler.QueryBlockTreeConstraintsTemplate;
import hadad.base.compiler.IRewritingSelector;
import hadad.base.compiler.VariableFactory;
import hadad.commons.constraints.Constraint;
import hadad.commons.constraints.parser.ConstraintParser;
import hadad.commons.constraints.parser.ParseException;

/**
 * MR Module
 * 
 * @author ranaalotaibi
 */
public abstract class MRModule extends AbstractModule {
    @Override
    protected void configure() {
        final Properties properties = new Properties();
        try {
            properties.load(new FileReader(getPropertiesFileName()));
            Names.bindProperties(binder(), properties);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        bind(VariableFactory.class).annotatedWith(Names.named("ArrayElementVariableFactory"))
                .toInstance(new VariableFactory(properties.getProperty("fresh_array_element_variable_prefix", "a_")));
        bind(VariableFactory.class).annotatedWith(Names.named("ConjunctiveQueryVariableFactory")).toInstance(
                new VariableFactory(properties.getProperty("fresh_conjunctive_query_variable_prefix", "c_")));
        bind(VariableFactory.class).annotatedWith(Names.named("RVariableFactory"))
                .toInstance(new VariableFactory(properties.getProperty("fresh_dml_variable_prefix", "b_")));
        bind(VariableFactory.class).annotatedWith(Names.named("ElementVariableFactory"))
                .toInstance(new VariableFactory(properties.getProperty("fresh_element_variable_prefix", "d_")));
        bind(VariableFactory.class).annotatedWith(Names.named("ReturnTemplateVariableFactory"))
                .toInstance(new VariableFactory(properties.getProperty("fresh_return_variable_prefix", "e_")));
        try {
            bind(QueryBlockTreeConstraintsTemplate.class).annotatedWith(Names.named("ForwardConstraintsTemplate"))
                    .toInstance(new QueryBlockTreeConstraintsTemplate(
                            parseConstraints(
                                    properties.getProperty("forward_constraints.template_grex_schema_file_name")),
                            parseConstraints(properties.getProperty("forward_constraints.template_view_file_name")),
                            properties.getProperty("template_document_name"),
                            properties.getProperty("document_name_prefix"),
                            properties.getProperty("template_view_name"), properties.getProperty("view_name_prefix")));
        } catch (ParseException | IOException e) {
            throw new RuntimeException(e);
        }
        try {
            bind(QueryBlockTreeConstraintsTemplate.class).annotatedWith(Names.named("BackwardConstraintsTemplate"))
                    .toInstance(new QueryBlockTreeConstraintsTemplate(
                            parseConstraints(
                                    properties.getProperty("backward_constraints.template_grex_schema_file_name")),
                            parseConstraints(properties.getProperty("backward_constraints.template_view_file_name")),
                            properties.getProperty("template_document_name"),
                            properties.getProperty("document_name_prefix"),
                            properties.getProperty("template_view_name"), properties.getProperty("view_name_prefix")));
        } catch (ParseException | IOException e) {
            throw new RuntimeException(e);
        }
        bind(IRewritingSelector.class).to(NaiveRewritingSelector.class);
        bind(IQueryBlockTreeBuilder.class).to(MRQueryBuilder.class);
    }

    private List<Constraint> parseConstraints(final String fileName) throws ParseException, IOException {
        final Reader reader = new FileReader(fileName);
        final ConstraintParser parser = new ConstraintParser(reader);
        final List<Constraint> constraints = parser.parse();
        reader.close();
        return constraints;
    }

    protected abstract String getPropertiesFileName();
}