/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.mr;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import hadad.base.compiler.DocumentsCatalog;
import hadad.base.compiler.PathExpression;
import hadad.base.compiler.VariableFactory;
import hadad.base.compiler.VariableMapper;
import hadad.base.compiler.exceptions.ParseException;
import hadad.base.parsers.MRBaseListener;
import hadad.base.parsers.MRLexer;
import hadad.base.parsers.MRParser;
import hadad.base.parsers.RParser;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.StringConstant;
import hadad.commons.conjunctivequery.Term;
import hadad.commons.conjunctivequery.Variable;

/**
 * MR PathExpressionListener
 * 
 * @author ranaalotaibi
 */
@Singleton
class PathExpressionListener extends MRBaseListener {
    private static final Logger LOGGER = Logger.getLogger(PathExpressionListener.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    private final VariableFactory cqVariableFactory;
    private final VariableMapper variableMapper;
    @SuppressWarnings("unused")
    private final String documentNamePrefix;
    @SuppressWarnings("unused")
    private final DocumentsCatalog documentsCatalog;
    private Set<Variable> referredVariables;
    private ParseTreeProperty<Variable> rStatementTreeProperty;
    private List<Atom> encoding;
    private Variable currentVar;

    @Inject
    public PathExpressionListener(@Named("document_name_prefix") final String documentNamePrefix,
            @Named("ConjunctiveQueryVariableFactory") final VariableFactory cqVariableFactory,
            final VariableMapper variableMapper, final DocumentsCatalog documentsCatalog) {
        this.documentNamePrefix = checkNotNull(documentNamePrefix);
        this.cqVariableFactory = checkNotNull(cqVariableFactory);
        this.variableMapper = checkNotNull(variableMapper);
        this.documentsCatalog = checkNotNull(documentsCatalog);
        LOGGER.setLevel(Level.OFF);

    }

    /**
     * Parse R expression
     * 
     * @param str
     *            R expression
     *
     * @return parsed expression
     * @throws ParseException
     */
    public PathExpression parse(final String str) throws ParseException {
        referredVariables = new HashSet<Variable>();
        encoding = new ArrayList<Atom>();
        currentVar = null;
        rStatementTreeProperty = new ParseTreeProperty<Variable>();
        final MRLexer lexer = new MRLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final MRParser parser = new MRParser(tokens);
        final ParserRuleContext tree = parser.rStatemnet();
        final ParseTreeWalker walker = new ParseTreeWalker();
        try {
            walker.walk(this, tree);
            return new PathExpression(MRModel.ID, referredVariables, encoding, currentVar,
                    new HashMap<String, String>());
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
    }

    @Override
    public void enterRStatemnet(MRParser.RStatemnetContext ctx) {
        final Variable var = cqVariableFactory.createFreshVar();
        //Base Matrix
        if (ctx.source().rMatrixConstruction() != null) {
            if (!ctx.source().rMatrixConstruction().getText().contains("read.table")) {
                encoding.add(new Atom(Predicate.NAMER.toString(), var, new StringConstant(ctx.source()
                        .rMatrixConstruction().rMatrixConstructionMatrixSource().mName().getText().replace("\"", ""))));
            } else {
                encoding.add(new Atom(Predicate.NAME.toString(), var, new StringConstant(ctx.source()
                        .rMatrixConstruction().rMatrixConstructionMatrixSource().mName().getText().replace("\"", ""))));
            }

        }

        //indicatorMatrix
        if (ctx.source().rSarseExpression() != null) {
            //Left 
            final StringConstant startingNumber = new StringConstant(
                    ctx.source().rSarseExpression().rangeL().numericScalar().getText().replace("\"", ""));

            final Variable varNameLeft =
                    variableMapper.getVariable(ctx.source().rSarseExpression().rangeL().varName(1).getText());

            final Variable varNameLefti =
                    new Variable(ctx.source().rSarseExpression().rangeL().varName(0).getText().replace("\"", ""));
            final Variable nRowVar = new Variable(varNameLeft.getName() + "_i");
            encoding.add(new Atom(Predicate.NROW.toString(), varNameLeft, nRowVar));
            encoding.add(new Atom(Predicate.RANGE_CREATE.toString(), startingNumber, nRowVar, varNameLefti));

            //Right
            final Variable varNameLeftj =
                    new Variable(ctx.source().rSarseExpression().rangeR().varName(0).getText().replace("\"", ""));
            final StringConstant calName =
                    new StringConstant(ctx.source().rSarseExpression().rangeR().colName().getText().replace("\"", ""));
            encoding.add(new Atom(Predicate.VECTOR_CREATE.toString(), varNameLeft, new StringConstant("_"), calName,
                    varNameLeftj));
            encoding.add(new Atom(Predicate.INDICATOR_MATRIX.toString(), varNameLefti, varNameLeftj, var));
        }
        if (ctx.source().rSubsetExpression() != null) {
            final Variable varName = variableMapper.getVariable(ctx.source().rSubsetExpression().varName().getText());
            int size = ctx.source().rSubsetExpression().selectStatement().STRING().size();
            final List<StringConstant> cols = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                final StringConstant str1 = new StringConstant(
                        ctx.source().rSubsetExpression().selectStatement().STRING().get(i).getText().replace("\"", ""));
                cols.add(str1);
            }

            final List<Term> terms = new ArrayList<>();
            terms.add(varName);
            terms.addAll(cols);
            terms.add(var);
            encoding.add(new Atom(Predicate.SUBSET.toString() + "_" + size, terms));

        }
        if (ctx.source().rNormalizedMatrix() != null) {
            final Variable varName1 = variableMapper
                    .getVariable(ctx.source().rNormalizedMatrix().parameter().get(0).listBody().varName().getText());
            final Variable varName2 = variableMapper
                    .getVariable(ctx.source().rNormalizedMatrix().parameter().get(1).listBody().varName().getText());
            final Variable varName3 = variableMapper
                    .getVariable(ctx.source().rNormalizedMatrix().parameter().get(2).listBody().varName().getText());
            encoding.add(new Atom(Predicate.NORMALIZEDMATRIX.toString(), varName1, varName2, varName3, var));
        }
        currentVar = var;
    }

    @Override
    public void exitMatrixIdentifier(MRParser.MatrixIdentifierContext ctx) {
        if (ctx != null) {
            final Variable var = variableMapper.getVariable(ctx.getText());
            referredVariables.add(var);
            setObject(ctx, var);
            currentVar = var;
        }
    }

    @Override
    public void exitMatrixMulExpression(MRParser.MatrixMulExpressionContext ctx) {
        //LOGGER.debug("Entering MatrixMulExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable varLeft = retrieveVariable(ctx.rExpression().get(0));
            final Variable varRight = retrieveVariable(ctx.rExpression().get(1));
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.MULTI.toString(), varLeft, varRight, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    @Override
    public void exitMatrixAddExpression(MRParser.MatrixAddExpressionContext ctx) {
        //LOGGER.debug("Entering MatrixAddExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable varLeft = retrieveVariable(ctx.rExpression().get(0));
            final Variable varRight = retrieveVariable(ctx.rExpression().get(1));
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.ADD.toString(), varLeft, varRight, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    @Override
    public void exitMatrixSubExpression(MRParser.MatrixSubExpressionContext ctx) {
        //LOGGER.debug("Entering MatrixAddExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable varLeft = retrieveVariable(ctx.rExpression().get(0));
            final Variable varRight = retrieveVariable(ctx.rExpression().get(1));
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.SUB.toString(), varLeft, varRight, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    @Override
    public void exitMatrixDivision(MRParser.MatrixDivisionContext ctx) {
        //LOGGER.debug("Entering MatrixDivExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable varLeft = retrieveVariable(ctx.rExpression().get(0));
            final Variable varRight = retrieveVariable(ctx.rExpression().get(1));
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.DIV.toString(), varLeft, varRight, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    @Override
    public void exitRowsSumExperssion(MRParser.RowsSumExperssionContext ctx) {
        //LOGGER.debug("Entering MatrixTransposeExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable var = retrieveVariable(ctx.rExpression());
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.ROWSUM.toString(), var, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    @Override
    public void exitColumnsSumExperssion(MRParser.ColumnsSumExperssionContext ctx) {
        //LOGGER.debug("Entering MatrixTransposeExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable var = retrieveVariable(ctx.rExpression());
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.COLSUM.toString(), var, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    @Override
    public void exitSumExperssion(MRParser.SumExperssionContext ctx) {
        if (ctx != null) {
            final Variable var = retrieveVariable(ctx.rExpression());
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.SUM.toString(), var, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    @Override
    public void exitMatrixTransposeExpression(MRParser.MatrixTransposeExpressionContext ctx) {
        //LOGGER.debug("Entering MatrixTransposeExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable var = retrieveVariable(ctx.rExpression());
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.TRANS.toString(), var, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    public void exitMatrixInverseExpression(RParser.MatrixInverseExpressionContext ctx) {
        // LOGGER.debug("Entering MatrixTransposeExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable var = retrieveVariable(ctx.rExpression());
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.INVERSE.toString(), var, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    @Override
    public void exitMatrixTraceExpression(MRParser.MatrixTraceExpressionContext ctx) {
        // LOGGER.debug("Entering MatrixTransposeExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable var = retrieveVariable(ctx.rExpression());
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.TRACE.toString(), var, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    @Override
    public void exitMatrixDetExpression(MRParser.MatrixDetExpressionContext ctx) {
        // LOGGER.debug("Entering MatrixTransposeExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable var = retrieveVariable(ctx.rExpression());
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.DET.toString(), var, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    @Override
    public void exitMatrixDiagonalExpression(MRParser.MatrixDiagonalExpressionContext ctx) {
        //s LOGGER.debug("Entering MatrixDiagonalExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable var = retrieveVariable(ctx.rExpression());
            final Variable result = cqVariableFactory.createFreshVar();
            encoding.add(new Atom(Predicate.DIAG.toString(), var, result));
            currentVar = result;
            setObject(ctx, result);
        }
    }

    @Override
    public void exitAtomicExpression(MRParser.AtomicExpressionContext ctx) {
        //LOGGER.debug("Entering AtomicExpression: " + ctx.getText());
        if (ctx != null) {
            final Variable var = retrieveVariable(ctx.rExpression());
            currentVar = var;
            setObject(ctx, var);
        }
    }

    /**
     * Retrieve variable associated with each sub-parse tree node.
     * 
     * @param subtree
     *            the sub-parse tree node
     * @return The corresponding sub-parse tree node variable
     */
    private Variable retrieveVariable(ParseTree subtree) {

        return rStatementTreeProperty.get(subtree);
    }

    /**
     * @param subtree
     *            the sub-parse tree node
     * @param var
     *            The variable that corresponds subtree
     */
    private void setObject(ParseTree subtree, Variable var) {
        rStatementTreeProperty.put(subtree, var);
    }

}
