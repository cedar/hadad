/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.mr;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.PathExpression;
import hadad.base.compiler.Structural;
import hadad.base.compiler.Tuple;
import hadad.base.compiler.VariableFactory;
import hadad.base.compiler.VariableMapper;
import hadad.base.compiler.exceptions.ParseException;
import hadad.base.parsers.MRBaseListener;
import hadad.base.parsers.MRLexer;
import hadad.base.parsers.MRParser;
import hadad.commons.conjunctivequery.Variable;

/**
 * MR StructuralBaseListener
 * 
 * @author ranaalotaibi
 */
abstract class StructuralBaseListener extends MRBaseListener {
    private static final Logger LOGGER = Logger.getLogger(StructuralBaseListener.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    /* Used to create fresh aql variables */
    protected final VariableFactory dmlVariableFactory;
    /* Used to map sjql variables and the internal fresh variables */
    protected final VariableMapper variableMapper;
    /* Used to keep the introduced variables of the path expression being parsed */
    private List<Tuple<Variable, PathExpression>> definitions;
    /* Used to know the variable being introduced */
    protected Variable currentVar;
    /* Used to know the name of variable being introduced */
    protected String currentVarName;

    private List<String> models;
    /* Used to parse path expressions */
    protected final PathExpressionListener pathExpressionListener;

    public StructuralBaseListener(final PathExpressionListener pathExpressionlistener,
            final VariableFactory sjqlVariableFactory, final VariableMapper variableMapper) {
        this.pathExpressionListener = checkNotNull(pathExpressionlistener);
        this.dmlVariableFactory = checkNotNull(sjqlVariableFactory);
        this.variableMapper = checkNotNull(variableMapper);
    }

    public Structural parse(final String str) throws ParseException {
        definitions = new ArrayList<Tuple<Variable, PathExpression>>();
        currentVar = null;
        models = new ArrayList<String>();
        models.add(MRModel.ID);

        final MRLexer lexer = new MRLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final MRParser parser = new MRParser(tokens);
        final ParserRuleContext tree = createParseTree(parser);
        final ParseTreeWalker walker = new ParseTreeWalker();
        try {
            walker.walk(this, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        return new Structural(definitions, models);
    }

    protected void defineVariable(final PathExpression pathExpression) {
        if (currentVar == null) {
            throw new IllegalStateException("Variable expected.");
        }
        definitions.add(new Tuple<Variable, PathExpression>(currentVar, pathExpression));
        currentVar = null;
    }

    protected abstract ParserRuleContext createParseTree(final MRParser parser);
}
