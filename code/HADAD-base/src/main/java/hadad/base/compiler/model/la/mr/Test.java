/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.mr;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import hadad.base.compiler.exceptions.HadadException;
import hadad.base.compiler.exceptions.MetadataException;
import hadad.base.compiler.model.la.metadata.Metadata;

public class Test {
    private static final Logger LOGGER = Logger.getLogger(Test.class);
    private static final String BASE_DIR = "src/main/resources/testMR";

    private static final int ESTIM = 1;

    public static void main(String[] args) throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter("src/main/resources/testMR/out.out", true));
        initializeMeta();
        for (File file : getTestsDirectories()) {
            LOGGER.debug("Test file: " + file.getName());
            MRRunnerWrapper test = new MRRunnerWrapper(file.getAbsolutePath(), ESTIM);
            test.run();
            System.out.println("RW:- " + test.getRW());
            System.out.println("Time:- " + test.getTime());
            writer.write(file.getName() + " : " + test.getTime() + "\n");
            writer.write(file.getName() + " : " + test.getRW() + "\n");
        }
        writer.close();
    }

    private static List<File> getTestsDirectories() {
        final File directory = new File(BASE_DIR);
        LOGGER.debug("Test home path: " + directory.toString());
        final List<File> testsDirectory = new ArrayList<File>();
        for (final File file : directory.listFiles()) {
            if (file.isDirectory()) {
                testsDirectory.add(file);
            }
        }
        return testsDirectory;
    }

    private static void initializeMeta() throws HadadException {
        try {
            Metadata.load();
            Metadata.getInstance();
            Metadata.instance.delete();
            Metadata.instance.add("syn1.csv", 50000, 100, 5000000, "");
            Metadata.instance.add("syn2.csv", 100, 50000, 5000000, "");
            Metadata.instance.add("syn3a.csv", 1000000, 100, 100000000, "");
            Metadata.instance.add("syn3b.csv", 1000000, 100, 100000000, "");
            Metadata.instance.add("syn4a.csv", 5000000, 100, 100000000, "");
            Metadata.instance.add("syn4b.csv", 5000000, 100, 100000000, "");
            Metadata.instance.add("syn5c.csv", 10000, 10000, 100000000, "");
            Metadata.instance.add("syn5d.csv", 10000, 10000, 100000000, "");
            Metadata.instance.add("syn5e.csv", 10000, 10000, 100000000, "");
            Metadata.instance.add("syn6c.csv", 20000, 20000, 400000000, "");
            Metadata.instance.add("syn6d.csv", 20000, 20000, 400000000, "");
            Metadata.instance.add("syn6e.csv", 20000, 20000, 400000000, "");
            Metadata.instance.add("syn7.csv", 100, 1, 100, "");
            Metadata.instance.add("syn8.csv", 50000, 1, 50000, "");
            Metadata.instance.add("syn9.csv", 100000, 1, 100000, "");
            Metadata.instance.add("syn10.csv", 100, 100, 10000, "");
            Metadata.instance.add("AS.mtx", 50000, 100, 5000000, "");

            Metadata.instance.add("V1.csv", 10000, 10000, 100000000, "");
            Metadata.instance.add("V2.csv", 10000, 10000, 100000000, "");
            Metadata.instance.add("V3.csv", 100, 100, 10000, "");
            Metadata.instance.add("V4.csv", 100000, 50000, 2000000000, "");//5B > INTEGER.max. So far put it as 2000000000.
            Metadata.instance.add("V5A.csv", 10000, 10000, 100000000, "");
            Metadata.instance.add("V5B.csv", 20000, 20000, 400000000, "");
            Metadata.instance.add("V6A.csv", 1000000, 100, 100000000, "");
            Metadata.instance.add("V6B.csv", 5000000, 100, 500000000, "");
            Metadata.instance.add("V7.csv", 10000, 10000, 100000000, "");
            Metadata.instance.add("V8A.csv", 10000, 10000, 100000000, "");
            Metadata.instance.add("V8B.csv", 10000, 10000, 100000000, "");
            Metadata.instance.add("V9.csv", 10000, 10000, 100000000, "");
            Metadata.instance.add("V10.csv", 1, 1, 1, "");
            Metadata.instance.add("V11.csv", 1, 1, 1, "");
            Metadata.instance.add("V12A.csv", 1000000, 100, 100000000, "");
            Metadata.instance.add("V12B.csv", 5000000, 100, 500000000, "");
            Metadata.instance.add("R.csv", 4, 3, 12, "");
            Metadata.instance.add("S.csv", 5, 3, 15, "");
            Metadata.instance.add("A.csv", 4, 2, 8, "");

        } catch (MetadataException e) {
            throw e;
        }
    }

}
