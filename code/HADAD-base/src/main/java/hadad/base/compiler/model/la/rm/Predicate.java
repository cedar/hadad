/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.rm;

/**
 * RM predicates
 * 
 * @author ranaalotaibi
 */
public enum Predicate {
    NAME("name"),
    SIZE("size"),
    ZERO("zero"),
    IDENTITY("identity"),
    ADD("add"),
    SUB("sub"),
    MULTI("multi"),
    MULTIS("multi_s"),
    MULTIE("multi_e"),
    DIV("div"),
    DIVS("div_s"),
    TRANS("tr"),
    INVERSE("in"),
    DET("det"),
    ADJ("adj"),
    COFA("cof"),
    TRACE("trace"),
    DIAG("diag"),
    POW("pow"),
    AVG("avg"),
    MEAN("mean"),
    VAR("var"),
    SD("sd"),
    COLSUM("colSums"),
    COLMEAN("colMeans"),
    COLVARS("colVars"),
    COLSDS("colSds"),
    COLMAX("colMaxs"),
    COLMINS("colMins"),
    ROWSUM("rowSums"),
    ROWMEAN("rowMeans"),
    ROWVARS("rowVars"),
    ROWSDS("rowSds"),
    ROWMAX("rowMaxs"),
    ROWMIN("rowMins"),
    CUMSUM("cumsum"),
    CUMPROD("cumprod"),
    CUMMIN("cummin"),
    CUMMAX("cummax"),
    SUM("sum"),
    EQUALS("eq");

    private final String str;

    private Predicate(final String str) {
        this.str = str;
    }

    @Override
    public String toString() {
        return str;
    }
}
