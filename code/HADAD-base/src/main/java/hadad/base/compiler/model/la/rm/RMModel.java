/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.rm;

import com.google.inject.Inject;

import hadad.base.compiler.IBlockEncoder;
import hadad.base.compiler.Format;
import hadad.base.compiler.Language;
import hadad.base.compiler.Model;
import hadad.base.compiler.IQueryBlockTreeBuilder;

/**
 * Represents matrix model and language R
 * 
 * @author ranaalotaibi
 **/
public class RMModel extends Model {
    public final static String ID = "RM";

    @Inject
    public RMModel(final IQueryBlockTreeBuilder queryBlockTreeBuilder, final IBlockEncoder blockEncoder) {
        super(ID, Format.MATRIX, Language.R, queryBlockTreeBuilder, blockEncoder);
    }
}
