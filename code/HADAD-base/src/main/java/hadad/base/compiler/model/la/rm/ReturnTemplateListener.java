/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.rm;

import static com.google.common.base.Preconditions.checkNotNull;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.VariableMapper;
import hadad.base.parsers.RBaseListener;

/**
 * RM Return Template Listener
 * 
 * @author ranaalotaibi
 *
 */
@Singleton
final class ReturnTemplateListener extends RBaseListener {
    private static final Logger LOGGER = Logger.getLogger(ReturnTemplateListener.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    @SuppressWarnings("unused")
    private final VariableMapper variableMapper;

    @Inject
    public ReturnTemplateListener(final VariableMapper variableMapper) {
        this.variableMapper = checkNotNull(variableMapper);
    }
}
