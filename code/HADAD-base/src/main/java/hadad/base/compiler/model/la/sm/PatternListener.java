/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.sm;

import java.util.ArrayList;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.AntlrUtils;
import hadad.base.compiler.Condition;
import hadad.base.compiler.Conditional;
import hadad.base.compiler.PathExpression;
import hadad.base.compiler.Pattern;
import hadad.base.compiler.VariableMapper;
import hadad.base.compiler.exceptions.ParseException;
import hadad.base.parsers.DMLBaseListener;
import hadad.base.parsers.DMLLexer;
import hadad.base.parsers.DMLParser;

/**
 * SM Pattern Listener
 * 
 * @author ranaalotaibi
 */
@Singleton
final class PatternListener {
    private static final Logger LOGGER = Logger.getLogger(PatternListener.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    private final StructuralListener structuralListener;
    private final ConditionalListener conditionalListener;
    private Pattern constructedPattern;

    @Inject
    public PatternListener(final StructuralListener structuralListener, final ConditionalListener conditionalListener) {
        this.structuralListener = structuralListener;
        this.conditionalListener = conditionalListener;
    }

    /**
     * Parse the query pattern.
     * 
     * @param str
     *            The pattern to be parsed.
     * @return constructedPattern The constructed pattern.
     * @throws ParseException
     */
    public Pattern parse(final String str) throws ParseException {
        final DMLLexer lexer = new DMLLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final DMLParser parser = new DMLParser(tokens);
        final ParserRuleContext tree = parser.dmlScript();
        final ParseTreeWalker walker = new ParseTreeWalker();
        final PatternListenerAux listener = new PatternListenerAux();
        try {
            walker.walk(listener, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        if (listener.getStructural() == null) {
            throw new ParseException(new IllegalStateException("Structural expected."));
        }
        constructedPattern = new Pattern(structuralListener.parse(listener.getStructural()),
                new Conditional(new ArrayList<Condition>(), new ArrayList<PathExpression>(), new ArrayList<String>()));
        return constructedPattern;
    }

    /**
     * Get the VariableMapper.
     * 
     * @return variableMapper. The VariableMapper for the Pattern listener.
     */
    public VariableMapper getVariableMapper() {
        return structuralListener.variableMapper;

    }

    /**
     * Get the constructed pattern.
     * 
     * @return constructedPattern. The constructed pattern.
     */
    public Pattern getPattern() {
        return constructedPattern;
    }

    private class PatternListenerAux extends DMLBaseListener {
        private String structural;

        public String getStructural() {
            return structural;
        }

        @Override
        public void enterDmlScript(DMLParser.DmlScriptContext ctx) {
            if (structural == null) {
                structural = AntlrUtils.getFullText(ctx);
            }
        }
    }
}
