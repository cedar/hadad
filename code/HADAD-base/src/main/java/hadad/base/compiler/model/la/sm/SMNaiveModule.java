package hadad.base.compiler.model.la.sm;

/**
 * SMNaiveModule
 * 
 * @author ranaalotaibi
 *
 */
public class SMNaiveModule extends SMModule {
    @Override
    protected String getPropertiesFileName() {
        return "sm.naive.properties";
    }
}
