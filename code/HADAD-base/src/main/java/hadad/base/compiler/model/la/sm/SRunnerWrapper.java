/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.sm;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;

import hadad.base.compiler.Language;
import hadad.base.compiler.PathExpression;
import hadad.base.compiler.QueryBlockTree;
import hadad.base.compiler.Utils;
import hadad.base.compiler.exceptions.HadadException;
import hadad.base.compiler.model.la.builder.LAPlanBuilder;
import hadad.base.compiler.model.la.builder.estim.MNCCostEstimator;
import hadad.base.compiler.model.la.builder.estim.MatrixBlock;
import hadad.base.compiler.model.la.builder.estim.MatrixHistogram;
import hadad.base.compiler.model.la.builder.estim.NaiveCostEstimator;
import hadad.base.compiler.model.la.builder.operators.BinaryOperator;
import hadad.base.compiler.model.la.builder.operators.IOperator;
import hadad.base.compiler.model.la.builder.operators.UnaryOperator;
import hadad.base.compiler.model.la.metadata.Metadata;
import hadad.base.loader.DataLoaderCSV;
import hadad.base.loader.DataLoaderMTX;
import hadad.base.loader.Serializer;
import hadad.base.rewriter.Context;
import hadad.base.rewriter.PACBRewriter;
import hadad.base.runner.RunnerWrapper;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.ConjunctiveQuery;
import hadad.commons.constraints.Constraint;
import hadad.commons.constraints.parser.ConstraintParser;
import hadad.commons.relationalschema.RelationalSchema;
import hadad.commons.relationalschema.parser.RelSchemaParser;

/**
 * SRunnerWrapper
 */
public class SRunnerWrapper extends RunnerWrapper {
    private static final Logger LOGGER = Logger.getLogger(SRunnerWrapper.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    private static final String TEST_DATA_Syn = "../../data/synthetic/";
    private static final String TEST_DATA_Views = "../../data/views/";
    private static final String TEST_DATA_Real = "../../data/real-data/";
    private double time = 0.00;
    private int flag = 0;
    private ConjunctiveQuery rw = null;
    private final String BASE_DIR;

    public SRunnerWrapper(final String BASE_DIR, final int falg) {
        this.BASE_DIR = BASE_DIR;
        this.flag = falg;
    }

    public void run(int costModel) throws HadadException {
        computeRewritings(costModel);
    }

    public void computeRewritings(int costModel) throws HadadException {

        try {
            computeRewritings(Utils.parseQuery(getQuery().toString()), costModel);
        } catch (Exception e) {
            throw new HadadException(e);
        }

    }

    private StringBuilder getQuery() throws HadadException {
        Injector injector = null;
        injector = Guice.createInjector(new SMNaiveModule());
        SMQueryBuilder builder = injector.getInstance(SMQueryBuilder.class);
        QueryBlockTree nbt = null;
        try {
            nbt = builder.buildQueryBlockTree(FileUtils.readFileToString(new File(BASE_DIR + "/SMQ")));
        } catch (IOException e) {
            throw new HadadException(e);
        }
        final StringBuilder querystrBuilder = new StringBuilder();
        final Collection<PathExpression> paths = nbt.getRoot().getPattern().getStructural().getPathExpressions();
        final List<Atom> atoms = new ArrayList<>();
        final String queryName = nbt.getRoot().getQueryName();
        final String returnVar =
                nbt.getRoot().getReturnTemplate().getTerms().get(0).getParent().getElement().toString();
        querystrBuilder.append(queryName);
        querystrBuilder.append("<");
        querystrBuilder.append(returnVar);
        querystrBuilder.append(">:-");

        for (final PathExpression path : paths) {
            atoms.addAll(path.encoding());
        }
        int i = 0;
        for (final Atom atom : atoms) {
            querystrBuilder.append(atom);
            if (i != atoms.size() - 1)
                querystrBuilder.append(",");
            i++;
        }

        querystrBuilder.append(";");
        return querystrBuilder;
    }

    private Context getContext() throws Exception {
        final List<Constraint> fw = parseConstraints(BASE_DIR + "/constraints_chase");
        final List<Constraint> bw = parseConstraints(BASE_DIR + "/constraints_bkchase");
        final List<RelationalSchema> schemas = parseSchemas(BASE_DIR + "/schemas");
        return new Context(schemas.get(0), schemas.get(1), fw, bw);
    }

    private List<Constraint> parseConstraints(final String fileName)
            throws IOException, hadad.commons.constraints.parser.ParseException {
        final FileReader fr = new FileReader(fileName);
        final ConstraintParser parser = new ConstraintParser(fr);
        final List<Constraint> constraints = parser.parse();
        fr.close();
        return constraints;
    }

    private static List<RelationalSchema> parseSchemas(final String fileName)
            throws IOException, hadad.commons.relationalschema.parser.ParseException {
        final FileReader fr = new FileReader(fileName);
        final RelSchemaParser parser = new RelSchemaParser(fr);
        final List<RelationalSchema> constraints = parser.parse();
        fr.close();
        return constraints;
    }

    private PACBRewriter getRewriter() throws Exception {
        return new PACBRewriter(getContext());
    }

    //    private PACBConjunctiveQueryRewriter getRewriter() throws Exception {
    //        return new PACBConjunctiveQueryRewriter(getContext());
    //    }

    private void computeRewritings(final ConjunctiveQuery query, int costModel) throws HadadException {
        List<ConjunctiveQuery> rws = new ArrayList<>();
        if (costModel == 2) {
            try {
                MNCInt();
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        try {
            rws = getRewriter().getReformulations(query, Language.DML, costModel);

            long rwTime = 0;
            for (int i = 0; i < 100; i++) {
                if (i == 0) {
                    rws = getRewriter().getReformulations(query, Language.DML, costModel);
                } else {
                    long start = System.nanoTime();
                    rws = getRewriter().getReformulations(query, Language.DML, costModel);
                    long end = System.nanoTime();
                    rwTime += (end - start);
                }
            }
            time += ((rwTime / 99) * 1e-9);

        } catch (Exception e) {
            throw new HadadException(e);
        }
        rw = rws.get(0);
    }

    private void computeRewritings(final ConjunctiveQuery query) throws HadadException {
        List<ConjunctiveQuery> rws;
        try {
            rws = getRewriter().getReformulations(query);

            long rwTime = 0;
            for (int i = 0; i < 100; i++) {
                if (i == 0) {
                    rws = getRewriter().getReformulations(query);
                } else {
                    long start = System.nanoTime();
                    rws = getRewriter().getReformulations(query);
                    long end = System.nanoTime();
                    rwTime += (end - start);
                }
            }
            time += ((rwTime / 99) * 1e-9);
            if (flag == 1) {
                naive(rws);
            } else {
                MNCInt();
                mnc(rws);
            }

        } catch (Exception e) {
            throw new HadadException(e);
        }
    }

    private void naive(List<ConjunctiveQuery> rws) {
        double minCost = Double.MAX_VALUE;
        ConjunctiveQuery minRW = null;
        // warm-up
        for (final ConjunctiveQuery rw : rws) {
            final IOperator root = LAPlanBuilder.constructPlan(rw);
            NaiveCostEstimator estimator = null;
            if (root != null) {
                estimator = new NaiveCostEstimator();
                if (root instanceof UnaryOperator) {
                    estimator.visit((UnaryOperator) root, null);

                }
                if (root instanceof BinaryOperator) {
                    estimator.visit((BinaryOperator) root, null);

                }
                double currentCost = estimator.getEstimatedCost();
                if (currentCost <= minCost) {
                    minCost = currentCost;
                    minRW = rw;
                }
            }
        }

        long startTime = System.nanoTime();
        for (int i = 0; i < 10; i++) {
            minCost = Double.MAX_VALUE;
            minRW = null;
            for (final ConjunctiveQuery rw : rws) {
                final IOperator root = LAPlanBuilder.constructPlan(rw);
                NaiveCostEstimator estimator = null;
                if (root != null) {
                    estimator = new NaiveCostEstimator();
                    if (root instanceof UnaryOperator) {
                        estimator.visit((UnaryOperator) root, null);

                    }
                    if (root instanceof BinaryOperator) {
                        estimator.visit((BinaryOperator) root, null);

                    }
                    double currentCost = estimator.getEstimatedCost();
                    if (currentCost <= minCost) {
                        minCost = currentCost;
                        minRW = rw;
                    }
                }
            }

        }
        long endTime = System.nanoTime();
        time += (((endTime - startTime) / 10) * 1e-9);
        rw = minRW;
    }

    private void mnc(List<ConjunctiveQuery> rws) {
        double minCost = Double.MAX_VALUE;
        ConjunctiveQuery minRW = null;
        // warm-up
        for (final ConjunctiveQuery rw : rws) {
            final IOperator root = LAPlanBuilder.constructPlan(rw);
            if (root != null) {
                MNCCostEstimator estimator = null;
                estimator = new MNCCostEstimator();
                if (root instanceof UnaryOperator) {
                    estimator.visit((UnaryOperator) root, null);

                }
                if (root instanceof BinaryOperator) {
                    estimator.visit((BinaryOperator) root, null);

                }
                double currentCost = estimator.getEstimatedCost();
                if (currentCost <= minCost) {
                    minCost = currentCost;
                    minRW = rw;
                }
            }
        }

        long startTime = System.nanoTime();
        for (int i = 0; i < 10; i++) {
            minCost = Double.MAX_VALUE;
            minRW = null;
            for (final ConjunctiveQuery rw : rws) {
                final IOperator root = LAPlanBuilder.constructPlan(rw);
                MNCCostEstimator estimator = null;
                if (root != null) {
                    estimator = new MNCCostEstimator();
                    if (root instanceof UnaryOperator) {
                        estimator.visit((UnaryOperator) root, null);

                    }
                    if (root instanceof BinaryOperator) {
                        estimator.visit((BinaryOperator) root, null);

                    }
                    double currentCost = estimator.getEstimatedCost();
                    if (currentCost <= minCost) {
                        minCost = currentCost;
                        minRW = rw;
                    }
                }
            }

        }
        long endTime = System.nanoTime();
        time += (((endTime - startTime) / 10) * 1e-9);
        rw = minRW;
    }

    public double getTime() {
        return time;
    }

    public ConjunctiveQuery getRW() {
        return rw;
    }

    private static void MNCInt() throws Exception {
        File[] testFilesSyn = new File(TEST_DATA_Syn).listFiles();
        for (final File file : testFilesSyn) {
            if (file.getName().contains(".csv") && !file.getName().contains("matrix")) {
                final MatrixBlock data = DataLoaderCSV.load(file);
                final MatrixHistogram histogram = new MatrixHistogram(data, true);
                final String serPath = Files.createTempDirectory("TempFolder").resolve(file.getName()).toString();
                Metadata.instance.update(file.getName().trim(), serPath);
                Metadata.instance.write();
                Serializer.serialize(serPath, histogram);
                Metadata.load();
            }
        }
        File[] testFilesReal = new File(TEST_DATA_Real).listFiles();
        for (final File file : testFilesReal) {
            if (file.getName().contains("AL3.mtx") || file.getName().contains("AL2.mtx")
                    || file.getName().contains("NL2.mtx") || file.getName().contains("NL3.mtx")) {
                continue;
            }
            if (file.getName().contains(".mtx")) {
                LOGGER.debug(file.getName());
                final MatrixBlock data = DataLoaderMTX.load(file);
                final MatrixHistogram histogram = new MatrixHistogram(data, true);
                final String serPath = Files.createTempDirectory("TempFolder").resolve(file.getName()).toString();
                Metadata.instance.update(file.getName(), serPath);
                Metadata.instance.write();
                Serializer.serialize(serPath, histogram);
                Metadata.load();
            }
        }

        File[] testFileViews = new File(TEST_DATA_Views).listFiles();
        for (final File file : testFileViews) {
            if (file.getName().contains(".csv")) {
                LOGGER.debug(file.getName());
                final MatrixBlock data = DataLoaderCSV.load(file);
                final MatrixHistogram histogram = new MatrixHistogram(data, true);
                final String serPath = Files.createTempDirectory("TempFolder").resolve(file.getName()).toString();
                Metadata.instance.update(file.getName(), serPath);
                Metadata.instance.write();
                Serializer.serialize(serPath, histogram);
                Metadata.load();
            }
        }
    }

    @Override
    public void run() throws HadadException {
        // TODO Auto-generated method stub

    }

}
