/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.spdml;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Guice;
import com.google.inject.Injector;

import hadad.base.compiler.PathExpression;
import hadad.base.compiler.QueryBlockTree;
import hadad.base.compiler.exceptions.HadadException;
import hadad.base.runner.RunnerWrapper;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.ConjunctiveQuery;

/**
 * MRRunnerWrapper
 * 
 * @author ranaalotaibi
 *
 */
public class SPDMLRunnerWrapper extends RunnerWrapper {
    private static final Logger LOGGER = Logger.getLogger(SPDMLRunnerWrapper.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }

    private final String BASE_DIR;

    public SPDMLRunnerWrapper(final String BASE_DIR, final int falg) {
        this.BASE_DIR = BASE_DIR;
    }

    public String getCQQuery() throws HadadException {
        try {
            return getQuery().toString();
        } catch (Exception e) {
            throw new HadadException(e);
        }
    }

    private StringBuilder getQuery() throws HadadException {
        Injector injector = null;
        injector = Guice.createInjector(new SPDMLNaiveModule());
        SPDMLQueryBuilder builder = injector.getInstance(SPDMLQueryBuilder.class);
        QueryBlockTree nbt = null;
        try {
            System.out.println(FileUtils.readFileToString(new File(BASE_DIR + "/SPDML")));
            nbt = builder.buildQueryBlockTree(FileUtils.readFileToString(new File(BASE_DIR + "/SPDML")));
        } catch (IOException e) {
            throw new HadadException(e);
        }
        final StringBuilder querystrBuilder = new StringBuilder();
        final Collection<PathExpression> paths = nbt.getRoot().getPattern().getStructural().getPathExpressions();
        final List<Atom> atoms = new ArrayList<>();
        final String queryName = nbt.getRoot().getQueryName();
        final String returnVar =
                nbt.getRoot().getReturnTemplate().getTerms().get(0).getParent().getElement().toString();
        querystrBuilder.append(queryName);
        querystrBuilder.append("<");
        querystrBuilder.append(returnVar);
        querystrBuilder.append(">:-");

        for (final PathExpression path : paths) {
            atoms.addAll(path.encoding());
        }
        int i = 0;
        for (final Atom atom : atoms) {
            querystrBuilder.append(atom);
            if (i != atoms.size() - 1)
                querystrBuilder.append(",");
            i++;
        }

        querystrBuilder.append(";");
        return querystrBuilder;
    }

    @Override
    public ConjunctiveQuery getRW() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public double getTime() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void run() throws HadadException {
        // TODO Auto-generated method stub

    }

    @Override
    public void run(int costModel) throws HadadException {
        // TODO Auto-generated method stub

    }

}
