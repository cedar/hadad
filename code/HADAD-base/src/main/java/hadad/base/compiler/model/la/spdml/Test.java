/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.spdml;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class Test {
    private static final Logger LOGGER = Logger.getLogger(Test.class);
    private static final String BASE_DIR = "src/main/resources/testSPDML";

    private static final int ESTIM = 1;

    public static void main(String[] args) throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter("src/main/resources/testSPDML/out.out", true));

        for (File file : getTestsDirectories()) {
            LOGGER.debug("Test file: " + file.getName());
            SPDMLRunnerWrapper test = new SPDMLRunnerWrapper(file.getAbsolutePath(), ESTIM);
            System.out.println("CQ:- " + test.getCQQuery());
        }
        writer.close();
    }

    private static List<File> getTestsDirectories() {
        final File directory = new File(BASE_DIR);
        LOGGER.debug("Test home path: " + directory.toString());
        final List<File> testsDirectory = new ArrayList<File>();
        for (final File file : directory.listFiles()) {
            if (file.isDirectory()) {
                testsDirectory.add(file);
            }
        }
        return testsDirectory;
    }

}
