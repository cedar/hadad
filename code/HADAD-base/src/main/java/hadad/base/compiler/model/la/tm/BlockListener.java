/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.tm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.AntlrUtils;
import hadad.base.compiler.Block;
import hadad.base.compiler.ChildBlock;
import hadad.base.compiler.Pattern;
import hadad.base.compiler.ReturnConstructTerm;
import hadad.base.compiler.IReturnLeafTerm;
import hadad.base.compiler.ReturnTemplate;
import hadad.base.compiler.ReturnTerm;
import hadad.base.compiler.RootBlock;
import hadad.base.compiler.StringElement;
import hadad.base.compiler.VariableElement;
import hadad.base.compiler.exceptions.ParseException;
import hadad.base.parsers.TFMBaseListener;
import hadad.base.parsers.TFMLexer;
import hadad.base.parsers.TFMParser;
import hadad.commons.conjunctivequery.Variable;

/**
 * TM BlockListener
 * 
 * @author ranaalotaibi
 * 
 */
@Singleton
final class BlockListener {
    private static final Logger LOGGER = Logger.getLogger(BlockListener.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    private final PatternListener patternListener;
    private final ReturnTemplateListener returnTemplateListener;

    @Inject
    public BlockListener(final PatternListener patternListener, final ReturnTemplateListener returnTemplateListener) {
        this.patternListener = patternListener;
        this.returnTemplateListener = returnTemplateListener;
        LOGGER.setLevel(Level.OFF);
    }

    /**
     * Parse DML script
     * 
     * @param queryName
     *            a query name
     * @param strQuery
     *            dml script
     * @return rootblock constructed root block
     * @throws ParseException
     */
    public RootBlock parse(final String queryName, final String strQuery) throws ParseException {
        final BlockListenerAux listener = _parse(strQuery);
        final Pattern pattern = patternListener.parse(listener.getPattern());

        return new RootBlock(queryName, pattern,
                constructReturnTemplate(pattern.getStructural().getDefinedVariables()));
    }

    /**
     * Parse DML
     * 
     * @param parent
     * @param createdNode
     * @param str
     * @return childblook
     * @throws ParseException
     */
    public ChildBlock parse(final Block parent, final String createdNode, final String str) throws ParseException {
        final BlockListenerAux listener = _parse(str);
        final Pattern pattern = patternListener.parse(listener.getPattern());
        return new ChildBlock(parent, createdNode, pattern,
                constructReturnTemplate(pattern.getStructural().getDefinedVariables()));
    }

    /**
     * Construct return template for DML script
     * 
     * @param patternVars
     *            the variables introduced in the pattern structural (ordered)
     * @return return template
     */
    private ReturnTemplate constructReturnTemplate(final List<Variable> patternVars) {
        final List<ReturnTerm> terms = new ArrayList<ReturnTerm>();
        final Variable var = patternVars.get(patternVars.size() - 1);
        final ReturnConstructTerm term = new ReturnConstructTerm(
                new ReturnConstructTerm(new VariableElement(var), new HashMap<String, IReturnLeafTerm>()),
                new StringElement(var.getName()), new HashMap<String, IReturnLeafTerm>());
        terms.add(term);
        return new ReturnTemplate(TMModel.ID, terms);
    }

    private BlockListenerAux _parse(final String str) throws ParseException {
        final TFMLexer lexer = new TFMLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final TFMParser parser = new TFMParser(tokens);
        final ParserRuleContext tree = parser.tfmScript();
        final ParseTreeWalker walker = new ParseTreeWalker();
        final BlockListenerAux listener = new BlockListenerAux();
        try {
            walker.walk(listener, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        if (listener.getPattern() == null) {
            throw new ParseException(new IllegalStateException("Pattern expected."));
        }
        return listener;
    }

    private class BlockListenerAux extends TFMBaseListener {
        private String pattern;

        /**
         * Get the pattern
         * 
         * @return pattern. The constructed pattern
         */
        public String getPattern() {
            return pattern;
        }

        @Override
        public void enterTfmScript(TFMParser.TfmScriptContext ctx) {
            LOGGER.debug("Entering dml script block listener: " + ctx.getText());
            if (pattern == null) {
                pattern = AntlrUtils.getFullText(ctx);
            }
        }
    }
}
