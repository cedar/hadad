/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this 

    http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF 
le except in compliance with the License.
   * You may obtain a copy of the License at
   *ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.tm;

import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.ReturnConstructTerm;
import hadad.base.compiler.ReturnStringTerm;
import hadad.base.compiler.ReturnTemplate;
import hadad.base.compiler.IReturnTemplateVisitor;
import hadad.base.compiler.ReturnVariableTerm;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.Variable;

/**
 * TMExtractVariableToCreatedNodeVisitor
 * 
 * @author ranaalotaibi
 *
 */
@Singleton
public class TMExtractVariableToCreatedNodeVisitor implements IReturnTemplateVisitor {
    private static final Logger LOGGER = Logger.getLogger(TMExtractVariableToCreatedNodeVisitor.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    private ImmutableList.Builder<Atom> builder;
    private Variable viewID;
    private String viewName;

    @Inject
    public TMExtractVariableToCreatedNodeVisitor() {
    }

    public List<Atom> encode(final ReturnTemplate template) {
        builder = ImmutableList.builder();
        template.accept(this);
        return builder.build();
    }

    public void encode(final Variable viewID, final String viewName) {
        this.viewID = viewID;
        this.viewName = viewName;

    }

    @Override
    public void visit(final ReturnTemplate template) {
        // NOP (no encoding for the template optionals)
    }

    @Override
    public void visitPre(final ReturnConstructTerm term) {
        // NOP (no encoding before the children are processed)
    }

    @Override
    public void visitPost(final ReturnConstructTerm term) {

    }

    @Override
    public void visit(final ReturnVariableTerm term) {

    }

    @Override
    public void visit(ReturnStringTerm term) {
        // NOP (no encoding for string terms)
    }
}