/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.tm;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.PathExpression;
import hadad.base.compiler.Pattern;
import hadad.base.compiler.QueryBlockTree;
import hadad.base.compiler.IQueryBlockTreeBuilder;
import hadad.base.compiler.ReturnTemplate;
import hadad.base.compiler.VariableMapper;
import hadad.base.compiler.exceptions.ParseException;
import hadad.commons.conjunctivequery.Variable;

/**
 * TM QueryBlockTreeBuilder which implements {@link IQueryBlockTreeBuilder}.
 * 
 * @author ranaalotaibi
 */
@Singleton
public final class TMQueryBuilder implements IQueryBlockTreeBuilder {
    private static final Logger LOGGER = Logger.getLogger(TMQueryBuilder.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    private final PathExpressionListener pathExpressionListener;
    private final ReturnTemplateListener returnTemplateListener;
    private final PatternListener patternListener;
    private final QueryBlockTreeListener queryBlockTreeListener;

    @Inject
    public TMQueryBuilder(final PathExpressionListener pathExpressionListener,
            final ReturnTemplateListener returnTemplateListener, final QueryBlockTreeListener queryBlockTreeListener,
            final PatternListener patternListener) {
        this.pathExpressionListener = pathExpressionListener;
        this.returnTemplateListener = returnTemplateListener;
        this.queryBlockTreeListener = queryBlockTreeListener;
        this.patternListener = patternListener;
    }

    @Override
    public PathExpression buildPathExpression(final String str) throws ParseException {
        return pathExpressionListener.parse(str);
    }

    @Override
    public ReturnTemplate buildReturnTemplate(ImmutableMap<Variable, PathExpression> definitions, final String str)
            throws ParseException {
        return null;
    }

    @Override
    public QueryBlockTree buildQueryBlockTree(final String str) throws ParseException {
        return queryBlockTreeListener.parse(str);
    }

    @Override
    public Pattern buildPattern(final String str) throws ParseException {

        return patternListener.parse(str);
    }

    public VariableMapper getVariableMapper() {

        return patternListener.getVariableMapper();
    }
}
