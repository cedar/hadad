/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.pj;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import hadad.base.compiler.Block;
import hadad.base.compiler.IBlockEncoder;
import hadad.base.compiler.Condition;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.ConjunctiveQuery;
import hadad.commons.conjunctivequery.Variable;

/**
 * PJ BlockEncoder
 * 
 * @author ranaalotaibi
 *
 */
public class PJBlockEncoder implements IBlockEncoder {
    private static final Logger LOGGER = Logger.getLogger(PJBlockEncoder.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    private final String encodingHeadVariablePosfix;

    @Inject
    public PJBlockEncoder(
            @Named("fresh_conjunctive_query_head_variable_posfix") final String encodingHeadVariablePosfix) {
        this.encodingHeadVariablePosfix = encodingHeadVariablePosfix;
    }

    @Override
    public ConjunctiveQuery encode(final Block block) {
        final List<Atom> body = new ArrayList<>();
        final List<Variable> head = new ArrayList<Variable>();
        for (final Variable var : block.getPattern().getLocalDefinedVariables()) {
            final Variable newVar = new Variable(var.getName() + encodingHeadVariablePosfix);
            head.add(newVar);
            body.add(new Atom(Predicate.EQUALS.toString(), var, newVar));
        }
        body.addAll(block.getPattern().encoding(conditionEncoder()));
        return new ConjunctiveQuery(block.getId(), head, body);
    }

    public Function<Condition, List<Atom>> conditionEncoder() {
        return Utils.conditionEncoding;
    }
}
