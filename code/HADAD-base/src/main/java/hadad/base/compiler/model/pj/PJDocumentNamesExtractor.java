/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.pj;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import hadad.base.compiler.Block;
import hadad.base.compiler.ChildBlock;
import hadad.base.compiler.QueryBlockTree;
import hadad.base.compiler.IQueryBlockTreeVisitor;
import hadad.base.compiler.RootBlock;

/**
 * PJ DocumentNamesExtractor which extends {@link IQueryBlockTreeVisitor}
 * 
 * @author ranaalotaibi
 */
@Singleton
public class PJDocumentNamesExtractor implements IQueryBlockTreeVisitor {
    private static final Logger LOGGER = Logger.getLogger(PJDocumentNamesExtractor.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    private final String documentNamePrefix;
    private Set<String> documentNames;

    @Inject
    public PJDocumentNamesExtractor(@Named("document_name_prefix") final String documentNamePrefix) {
        this.documentNamePrefix = documentNamePrefix;
    }

    public Set<String> getDocumentNames(final QueryBlockTree nbt) {
        documentNames = new HashSet<String>();
        nbt.accept(this);
        return documentNames;
    }

    @Override
    public void visit(final QueryBlockTree tree) {
        // NOP
    }

    @Override
    public void visit(final RootBlock block) {
        _visit(block);
    }

    @Override
    public void visit(final ChildBlock block) {
        _visit(block);
    }

    private void _visit(final Block block) {
        block.getPattern().encoding(Utils.conditionEncoding).stream()
                .filter(a -> a.getPredicate().startsWith(Predicate.ROOT.toString()))
                .map(a -> a.getPredicate().substring(a.getPredicate().indexOf(documentNamePrefix) + 1))
                .forEach(documentNames::add);
    }
}
