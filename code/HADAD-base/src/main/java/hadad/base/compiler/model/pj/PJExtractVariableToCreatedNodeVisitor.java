/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of

plicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * l
he License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by aimitations under the License.
**/
package hadad.base.compiler.model.pj;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.ReturnConstructTerm;
import hadad.base.compiler.ReturnStringTerm;
import hadad.base.compiler.ReturnTemplate;
import hadad.base.compiler.IReturnTemplateVisitor;
import hadad.base.compiler.ReturnVariableTerm;
import hadad.base.compiler.VariableCopier;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.Variable;

/**
 * PJ ExtractVariableToCreatedNodeVisitor which implements {@link IReturnTemplateVisitor}
 * 
 * @author ranaalotaibi
 */
@Singleton
public class PJExtractVariableToCreatedNodeVisitor implements IReturnTemplateVisitor {
    private static final Logger LOGGER = Logger.getLogger(PJExtractVariableToCreatedNodeVisitor.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    private final VariableCopier variableCopier;
    private ImmutableList.Builder<Atom> builder;
    private String queryName;
    private Set<Variable> copiedVariables;

    @Inject
    public PJExtractVariableToCreatedNodeVisitor(final VariableCopier variableCopier) {
        this.variableCopier = variableCopier;
    }

    public List<Atom> encode(final ReturnTemplate template, final String queryName) {
        builder = ImmutableList.builder();
        this.queryName = queryName;
        copiedVariables = new HashSet<Variable>();
        template.accept(this);
        return builder.build();
    }

    public List<Atom> encode(Variable viewID) {
        final List<Atom> viewConclusion = new ArrayList<Atom>();
        viewConclusion.add(new Atom(queryName, viewID));
        return viewConclusion;
    }

    @Override
    public void visit(final ReturnTemplate template) {
        // NOP (no encoding for the template optionals)
    }

    @Override
    public void visitPre(final ReturnConstructTerm term) {
        // NOP (no encoding before the children are processed)
    }

    @Override
    public void visitPost(final ReturnConstructTerm term) {
        // NOP (no encoding after the children are processed)
    }

    @Override
    public void visit(final ReturnVariableTerm term) {
        if (!copiedVariables.contains(term.getVariable())) {
            copiedVariables.add(term.getVariable());
            builder.add(new Atom(Predicate.COPY.toString() + "_" + queryName, term.toTerm(),
                    variableCopier.getCopy(term.getVariable())));
        }
    }

    @Override
    public void visit(ReturnStringTerm term) {
        // NOP (no encoding for string terms)
    }
}