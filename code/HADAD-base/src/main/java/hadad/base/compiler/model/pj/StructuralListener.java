/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.pj;

import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import hadad.base.compiler.AntlrUtils;
import hadad.base.compiler.VariableFactory;
import hadad.base.compiler.VariableMapper;
import hadad.base.compiler.exceptions.HadadCompilationException;
import hadad.base.compiler.exceptions.ParseException;
import hadad.commons.conjunctivequery.Variable;

/**
 * PJ StructuralListener which extends {@link StructuralBaseListener}.
 * 
 * @author ranaalotaibi
 */
@Singleton
public final class StructuralListener extends StructuralBaseListener {
    private static final Logger LOGGER = Logger.getLogger(StructuralListener.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }

    @Inject
    public StructuralListener(final PathExpressionListener listener,
            @Named("PJQLVariableFactory") VariableFactory pjqlVariableFactory, VariableMapper variableMapper) {
        super(listener, pjqlVariableFactory, variableMapper);
        LOGGER.setLevel(Level.OFF);
    }

    @Override
    public void enterPjBinding(PJQLParser.PjBindingContext ctx) {
        LOGGER.debug("Entering ForClauseBindingVar: " + ctx.getText());
        if (currentVar != null) {
            throw new IllegalStateException("Path expression expected.");
        }
        final Variable var = pjqlVariableFactory.createFreshVar();
        variableMapper.define(ctx.pjFromClauseBindingVar().getText(), var);
        currentVar = var;
        try {
            defineVariable(pathExpressionListener.parse(AntlrUtils.getFullText(ctx.pjFromClauseBindingSource()))
                    .copy(currentVar));
        } catch (ParseException e) {
            throw new HadadCompilationException(e);
        }

    }

    @Override
    protected ParserRuleContext createParseTree(final PJQLParser parser) {
        return parser.pjFromClause();
    }
}
