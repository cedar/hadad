/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.pj.full;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.AntlrUtils;
import hadad.base.compiler.ConditionTerm;
import hadad.base.compiler.PathExpression;
import hadad.base.compiler.VariableMapper;
import hadad.base.compiler.exceptions.HadadCompilationException;
import hadad.base.compiler.exceptions.ParseException;
import hadad.base.compiler.model.qbt.QBTQueryBlockTreeBuilder;
import hadad.commons.conjunctivequery.StringConstant;
import hadad.commons.conjunctivequery.Term;
import hadad.commons.conjunctivequery.Variable;

/**
 * PJ ConditionTermListener which extends {@link PJQLBaseListener}.
 * 
 * @author ranaalotaibi
 */
@Singleton
final class ConditionTermListener extends PJQLBaseListener {
    private static final Logger LOGGER = Logger.getLogger(ConditionTermListener.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    protected final PathExpressionListener pathExpressionListener;
    protected final VariableMapper variableMapper;
    private Set<Variable> referredVariables;
    private Term term;
    private List<PathExpression> pathExpressions;

    @Inject
    public ConditionTermListener(final PathExpressionListener listener, final VariableMapper variableMapper) {
        this.pathExpressionListener = listener;
        this.variableMapper = variableMapper;
    }

    /**
     * Parse the condition term.
     * 
     * @param str
     *            the condition term that needs to be parsed
     * @return the parsed condition term
     */
    public ConditionTerm parse(final String str) throws ParseException {
        referredVariables = new HashSet<Variable>();
        term = null;
        pathExpressions = new ArrayList<PathExpression>();

        final PJQLLexer lexer = new PJQLLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final PJQLParser parser = new PJQLParser(tokens);
        final ParserRuleContext tree = parser.pjCTerm();
        final ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(this, tree);
        return new ConditionTerm(term, referredVariables, pathExpressions);

    }

    @Override
    public void enterPjTermVar(PJQLParser.PjTermVarContext ctx) {
        LOGGER.debug("Entering TermVar: " + ctx.getText());
        final Variable var;
        if (variableMapper.isNotDefined(ctx.getText()) && QBTQueryBlockTreeBuilder.variableMapper != null) {
            var = QBTQueryBlockTreeBuilder.variableMapper.getVariable(ctx.getText());
        } else {
            var = variableMapper.getVariable(ctx.getText());
        }
        referredVariables.add(var);
        term = var;
    }

    @Override
    public void enterPjTermConstant(PJQLParser.PjTermConstantContext ctx) {
        LOGGER.debug("Entering TermConstant: " + ctx.getText());
        term = new StringConstant(ctx.getText().substring(1, ctx.getText().length() - 1));
    }

    @Override
    public void enterPjTermPath(PJQLParser.PjTermPathContext ctx) {
        LOGGER.debug("Entering TermPath: " + ctx.getText());
        PathExpression pathExpression;
        try {
            pathExpression = pathExpressionListener.parse(AntlrUtils.getFullText(ctx));
            term = pathExpression.getReturnVar();
            pathExpressions.add(pathExpression);
        } catch (ParseException e) {
            throw new HadadCompilationException(e);
        }

    }
}
