/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.pj.full;

import com.google.inject.Inject;

import hadad.base.compiler.IBlockEncoder;
import hadad.base.compiler.Format;
import hadad.base.compiler.Language;
import hadad.base.compiler.Model;
import hadad.base.compiler.IQueryBlockTreeBuilder;

/**
 * PJModel
 * 
 * @author ranaalotaibi
 *
 */
public class PJModel extends Model {
    public final static String ID = "PJ";

    @Inject
    public PJModel(final IQueryBlockTreeBuilder queryBlockTreeBuilder, final IBlockEncoder blockEncoder) {
        super(ID, Format.JSON, Language.PJSQL, queryBlockTreeBuilder, blockEncoder);
    }
}
