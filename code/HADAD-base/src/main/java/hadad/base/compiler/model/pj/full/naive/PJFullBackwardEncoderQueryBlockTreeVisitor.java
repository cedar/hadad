/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.pj.full.naive;

import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.ChildBlock;
import hadad.base.compiler.QueryBlockTree;
import hadad.base.compiler.IQueryBlockTreeVisitor;
import hadad.base.compiler.RootBlock;
import hadad.base.compiler.model.pj.full.PJFullExtractVariableToCreatedNodeVisitor;
import hadad.base.rewriter.Comment;
import hadad.commons.conjunctivequery.Variable;
import hadad.commons.constraints.Constraint;

@Singleton
class PJFullBackwardEncoderQueryBlockTreeVisitor implements IQueryBlockTreeVisitor {
    private final PJFullExtractVariableToCreatedNodeVisitor pJExtractVariableToCreatedNodeVisitor;
    private Variable viewID;
    private ImmutableList.Builder<Constraint> builder;
    private boolean includeComments;

    @Inject
    public PJFullBackwardEncoderQueryBlockTreeVisitor(
            final PJFullExtractVariableToCreatedNodeVisitor pJExtractVariableToCreatedNodeVisitor) {
        this.pJExtractVariableToCreatedNodeVisitor = pJExtractVariableToCreatedNodeVisitor;
        this.viewID = null;
    }

    public List<Constraint> compileConstraints(final QueryBlockTree nbt, final Variable viewID,
            boolean includeComments) {
        this.viewID = viewID;
        builder = ImmutableList.builder();
        this.includeComments = includeComments;
        nbt.accept(this);
        return builder.build();
    }

    @Override
    public void visit(final QueryBlockTree tree) {
        // TODO Auto-generated method stub
    }

    @Override
    public void visit(final RootBlock block) {
        if (!block.getPattern().isEmpty()) {
            if (includeComments) {
                builder.add(new Comment(block.getQueryName() + " constraint for Body Encoding"));
            }
            //builder.add(getBackwardConstraintForBodyEncoding(block));
        }
    }

    @Override
    public void visit(final ChildBlock block) {

    }

    //    private Constraint getBackwardConstraintForBodyEncoding(final RootBlock block) {
    //        final String viewName = block.getQueryName();
    //        final List<Atom> premise = new ArrayList<Atom>();
    //        final List<Atom> conclusion = new ArrayList<Atom>();
    //        conclusion.addAll(block.getPattern().encoding(Utils.conditionEncoding));
    //        pJExtractVariableToCreatedNodeVisitor.encode(viewID, viewName);
    //        premise.addAll(pJExtractVariableToCreatedNodeVisitor.encode(block.getReturnTemplate()));
    //        premise.add(Utils.createRootAtom(viewID, viewName));
    //        return new Tgd(premise, conclusion);
    //    }

}