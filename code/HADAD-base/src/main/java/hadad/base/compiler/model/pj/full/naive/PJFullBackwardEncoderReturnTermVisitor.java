/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.pj.full.naive;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import hadad.base.compiler.ReturnConstructTerm;
import hadad.base.compiler.ReturnStringTerm;
import hadad.base.compiler.ReturnTemplate;
import hadad.base.compiler.IReturnTemplateVisitor;
import hadad.base.compiler.ReturnVariableTerm;
import hadad.base.compiler.VariableFactory;
import hadad.base.compiler.model.pj.ArrayElementFactory;
import hadad.base.compiler.model.pj.Predicate;
import hadad.base.compiler.model.pj.Utils;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.Term;
import hadad.commons.conjunctivequery.Variable;

@Singleton
public final class PJFullBackwardEncoderReturnTermVisitor implements IReturnTemplateVisitor {
    private final ArrayElementFactory arrayElementFactory;
    private final VariableFactory elementVariableFactory;

    private ImmutableList.Builder<Atom> builder;
    private Map<Variable, ImmutableList.Builder<Atom>> auxiliarBuilders;
    private String viewName;
    private Variable viewSetID;
    private Variable parentBlockCreatedNode;
    private Term rootElementCreatedNode;

    @Inject
    public PJFullBackwardEncoderReturnTermVisitor(final ArrayElementFactory arrayElementFactory,
            @Named("ElementVariableFactory") final VariableFactory elementVariableFactory) {
        this.elementVariableFactory = elementVariableFactory;
        this.arrayElementFactory = arrayElementFactory;
    }

    public List<Atom> encode(final Variable parentBlockCreatedNode, final ReturnTemplate template,
            final String viewName) {
        return _encode(parentBlockCreatedNode, template, viewName, null);
    }

    public List<Atom> encode(final ReturnTemplate template, final String viewName, final Variable viewSetID) {
        return _encode(null, template, viewName, viewSetID);
    }

    private List<Atom> _encode(final Variable parentBlockCreatedNode, final ReturnTemplate template,
            final String viewName, final Variable viewSetID) {
        builder = ImmutableList.builder();
        this.auxiliarBuilders = new HashMap<Variable, ImmutableList.Builder<Atom>>();
        this.parentBlockCreatedNode = parentBlockCreatedNode;
        this.viewName = viewName;
        this.viewSetID = viewSetID;
        template.accept(this);
        return builder.build();
    }

    private boolean isRootBlock() {
        return parentBlockCreatedNode == null;
    }

    private boolean isChildBlock() {
        return !isRootBlock();
    }

    @Override
    public void visit(final ReturnTemplate template) {
        // NOP (no encoding for the template optionals)
    }

    @Override
    public void visitPost(final ReturnConstructTerm term) {
        // NOP (no encoding after the children are processed)
    }

    @Override
    public void visitPre(final ReturnConstructTerm term) {

    }

    @Override
    public void visit(final ReturnVariableTerm term) {
        builder.add(new Atom(Predicate.CHILD.toString() + "_" + viewName, viewSetID, term.getVariable(),
                term.getParent().getElement().toTerm(), Utils.getDataType(term)));
    }

    @Override
    public void visit(final ReturnStringTerm term) {

    }
}