/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.pj.naive;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import hadad.base.compiler.Block;
import hadad.base.compiler.ChildBlock;
import hadad.base.compiler.QueryBlockTree;
import hadad.base.compiler.IQueryBlockTreeVisitor;
import hadad.base.compiler.RootBlock;
import hadad.base.compiler.exceptions.HadadCompilationException;
import hadad.base.compiler.exceptions.ParseException;
import hadad.base.compiler.model.pj.PJExtractVariableToCreatedNodeVisitor;
import hadad.base.compiler.model.pj.Utils;
import hadad.base.rewriter.Comment;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.constraints.Constraint;
import hadad.commons.constraints.Tgd;

@Singleton
class PJBackwardEncoderNestedBlockTreeVisitor implements IQueryBlockTreeVisitor {
    private final PJExtractVariableToCreatedNodeVisitor pJExtractVariableToCreatedNodeVisitor;
    private final PJBlockBackwardEncoderReturnTermVisitor pJBlockBackwardEncoderReturnTermVisitor;
    private final String skolemFunctionVariableCopySuffix;

    private ImmutableList.Builder<Constraint> builder;
    private boolean includeComments;

    @Inject
    public PJBackwardEncoderNestedBlockTreeVisitor(
            final PJExtractVariableToCreatedNodeVisitor pJExtractVariableToCreatedNodeVisitor,
            @Named("skolem_function_variable_copy_suffix") final String skolemFunctionVariableCopySuffix,
            final PJBlockBackwardEncoderReturnTermVisitor pJBlockBackwardEncoderReturnTermVisitor) {
        this.pJExtractVariableToCreatedNodeVisitor = pJExtractVariableToCreatedNodeVisitor;
        this.pJBlockBackwardEncoderReturnTermVisitor = pJBlockBackwardEncoderReturnTermVisitor;
        this.skolemFunctionVariableCopySuffix = skolemFunctionVariableCopySuffix;
    }

    public List<Constraint> compileConstraints(final QueryBlockTree nbt, boolean includeComments) {
        builder = ImmutableList.builder();
        this.includeComments = includeComments;
        nbt.accept(this);
        return builder.build();
    }

    @Override
    public void visit(final QueryBlockTree tree) {
        // TODO Auto-generated method stub
    }

    @Override
    public void visit(final RootBlock block) {
        if (!block.getPattern().isEmpty()) {
            final List<Constraint> skolemFunctions =
                    Utils.getConstraintForSkolemFunctions(block, skolemFunctionVariableCopySuffix);
            if (includeComments && !skolemFunctions.isEmpty()) {
                builder.add(new Comment(block.getId() + " constraints for skolem functions"));
            }
            builder.addAll(skolemFunctions);
        }
        if (includeComments) {
            builder.add(new Comment(block.getId() + " constraint connecting create to JSON constructed by view"));
        }
        builder.addAll(getConstraintConnectingCreateToJSONConstructedByView(block));
        if (!block.getPattern().isEmpty()) {
            if (includeComments) {
                builder.add(new Comment(block.getId() + " constraint relating extract with create"));
            }
            builder.add(getConstraintRelatingExtractWithCreate(block));
            if (includeComments) {
                builder.add(new Comment(block.getId() + " constraint for extract"));
            }
            builder.add(getConstraintForExtract(block));
        }
    }

    @Override
    public void visit(final ChildBlock block) {
        final List<Constraint> skolemFunctions =
                Utils.getConstraintForSkolemFunctions(block, skolemFunctionVariableCopySuffix);
        if (includeComments && !skolemFunctions.isEmpty()) {
            builder.add(new Comment(block.getId() + " constraints for skolem functions"));
        }
        builder.addAll(skolemFunctions);
        if (includeComments) {
            builder.add(new Comment(block.getId() + " constraint connecting create to JSON constructed by view"));
        }
        try {
            builder.addAll(getConstraintConnectingCreateToJSONConstructedByView(block));
        } catch (ParseException e) {
            throw new HadadCompilationException(e);
        }
        if (includeComments) {
            builder.add(new Comment(block.getId() + " constraint relating extract with create"));
        }
        builder.add(getConstraintRelatingExtractWithCreate(block));
        if (includeComments) {
            builder.add(new Comment(block.getId() + " constraint for extract"));
        }
        builder.add(getConstraintForExtract(block));
    }

    private Constraint getConstraintForExtract(final RootBlock block) {
        final List<Atom> premise = new ArrayList<Atom>();
        premise.add(Utils.getExtractAtom(block));
        final List<Atom> conclusion = new ArrayList<Atom>();
        conclusion.addAll(block.getPattern().encoding(Utils.conditionEncoding));
        return new Tgd(premise, conclusion);
    }

    private Constraint getConstraintForExtract(final ChildBlock block) {
        final List<Atom> premise = new ArrayList<Atom>();
        premise.add(Utils.getExtractAtom(block));
        final List<Atom> conclusion = new ArrayList<Atom>();
        if (!block.getParent().getPattern().isEmpty()) {
            conclusion.add(Utils.getExtractAtom(block.getParent()));
        }
        conclusion.addAll(block.getPattern().encoding(Utils.conditionEncoding));
        return new Tgd(premise, conclusion);
    }

    private Constraint getConstraintRelatingExtractWithCreate(final Block block) {
        final String queryName = block.getQueryName();
        final List<Atom> premise = new ArrayList<Atom>();
        premise.add(Utils.getCreateAtom(block));
        final List<Atom> conclusion = new ArrayList<Atom>();
        conclusion.addAll(pJExtractVariableToCreatedNodeVisitor.encode(block.getReturnTemplate(), queryName));
        conclusion.add(Utils.getExtractAtom(block));
        for (final Atom skolemFunction : Utils.getSkolemFunctions(block)) {
            conclusion.add(skolemFunction);
        }
        return new Tgd(premise, conclusion);
    }

    private List<Constraint> getConstraintConnectingCreateToJSONConstructedByView(final RootBlock block) {
        final List<List<Atom>> premises =
                pJBlockBackwardEncoderReturnTermVisitor.encode(block.getReturnTemplate(), block.getQueryName());

        final List<Atom> conclusion = new ArrayList<Atom>();
        conclusion.add(Utils.getCreateAtom(block));

        final List<Constraint> constraints = new ArrayList<Constraint>();
        for (final List<Atom> premise : premises) {
            constraints.add(new Tgd(premise, conclusion));
        }
        return constraints;
    }

    private List<Constraint> getConstraintConnectingCreateToJSONConstructedByView(final ChildBlock block)
            throws ParseException {
        List<List<Atom>> premises;
        try {
            premises = pJBlockBackwardEncoderReturnTermVisitor.encode(block.getCreatedNode(), block.getReturnTemplate(),
                    block.getQueryName());
        } catch (ParseException e) {
            throw e;
        }

        final List<Atom> conclusion = new ArrayList<Atom>();
        conclusion.add(Utils.getCreateAtom(block));
        conclusion.addAll(Utils.getSkolemFunctions(block));

        final List<Constraint> constraints = new ArrayList<Constraint>();
        for (final List<Atom> premise : premises) {
            final List<Atom> premiseAux = new ArrayList<Atom>();
            if (!block.getParent().getPattern().isEmpty()) {
                premiseAux.add(Utils.getParentCreateSkolemFunction(block));
            }
            premiseAux.add(Utils.getCreateAtom(block.getParent()));
            premiseAux.addAll(premise);
            constraints.add(new Tgd(premiseAux, conclusion));
        }
        return constraints;
    }
}