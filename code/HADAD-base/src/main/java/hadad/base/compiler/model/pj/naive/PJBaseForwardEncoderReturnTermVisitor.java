/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.pj.naive;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;

import hadad.base.compiler.IElement;
import hadad.base.compiler.ReturnConstructTerm;
import hadad.base.compiler.ReturnStringTerm;
import hadad.base.compiler.ReturnTemplate;
import hadad.base.compiler.IReturnTemplateVisitor;
import hadad.base.compiler.ReturnTerm;
import hadad.base.compiler.ReturnVariableTerm;
import hadad.base.compiler.model.pj.ArrayElementFactory;
import hadad.base.compiler.model.pj.Predicate;
import hadad.base.compiler.model.pj.Utils;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.Term;

abstract class PJBaseForwardEncoderReturnTermVisitor implements IReturnTemplateVisitor {
    protected final ArrayElementFactory arrayElementFactory;
    protected ImmutableList.Builder<Atom> builder;
    protected String viewName;

    @Inject
    public PJBaseForwardEncoderReturnTermVisitor(final ArrayElementFactory arrayElementFactory) {
        this.arrayElementFactory = arrayElementFactory;
    }

    @Override
    public void visit(final ReturnTemplate template) {
        // NOP (no encoding for the template optionals)
    }

    @Override
    public void visitPost(final ReturnConstructTerm term) {
        // NOP (no encoding after the children are processed)
    }

    @Override
    public void visitPre(final ReturnConstructTerm term) {

    }

    @Override
    public void visit(final ReturnVariableTerm term) {
        builder.add(new Atom(Predicate.CHILD.toString() + "_" + viewName, getParentBlockCreatedNode(term),
                term.getCreatedNode(),
                Utils.isObject(term) ? getParentBlockElement(term).toTerm() : arrayElementFactory.getElement(term),
                Utils.getDataType(term)));
    }

    @Override
    public void visit(final ReturnStringTerm term) {
        builder.add(new Atom(Predicate.CHILD.toString() + "_" + viewName, getParentBlockCreatedNode(term),
                term.toTerm(),
                Utils.isObject(term) ? getParentBlockElement(term).toTerm() : arrayElementFactory.getElement(term),
                Utils.getDataType(term)));
    }

    protected abstract IElement getParentBlockElement(final ReturnTerm term);

    protected abstract Term getParentBlockCreatedNode(final ReturnTerm term);
}