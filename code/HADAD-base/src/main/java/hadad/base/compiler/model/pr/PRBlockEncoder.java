/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.pr;

import java.util.List;
import java.util.function.Function;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.Block;
import hadad.base.compiler.IBlockEncoder;
import hadad.base.compiler.Condition;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.ConjunctiveQuery;

/**
 * PRBlockEncoder
 * 
 * @author ranaalotaibi
 *
 */
public class PRBlockEncoder implements IBlockEncoder {
    private static final Logger LOGGER = Logger.getLogger(PRBlockEncoder.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }

    @Override
    public ConjunctiveQuery encode(final Block block) {
        return new ConjunctiveQuery(block.getId(), block.getPattern().getLocalDefinedVariables(),
                block.getPattern().encoding(conditionEncoder()));
    }

    public Function<Condition, List<Atom>> conditionEncoder() {
        return Utils.conditionEncoding;
    }
}
