/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance w

 License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for 
h the License.
   * You may obtain a copy of ththe specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.pr;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.PathExpression;
import hadad.base.compiler.Pattern;
import hadad.base.compiler.QueryBlockTree;
import hadad.base.compiler.IQueryBlockTreeBuilder;
import hadad.base.compiler.ReturnTemplate;
import hadad.base.compiler.VariableMapper;
import hadad.base.compiler.exceptions.ParseException;
import hadad.commons.conjunctivequery.Variable;

/**
 * PRQueryBlockTreeBuilderAlternative which implements {@link IQueryBlockTreeBuilder}.
 * 
 * @author ranaalotaibi
 *
 */
@Singleton
public final class PRQueryBlockTreeBuilderAlternative implements IQueryBlockTreeBuilder {
    private static final Logger LOGGER = Logger.getLogger(PRQueryBlockTreeBuilderAlternative.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    private final ReturnTemplateListener returnTemplateListener;
    private final QueryBlockTreeListener rootBlock;
    private final PatternListener patetrnListener;

    public static Map<String, Relation> relations;
    public static Map<String, Map<String, Object>> realtionsFreshVaribales;

    @Inject
    public PRQueryBlockTreeBuilderAlternative(final ReturnTemplateListener returnTemplateListener,
            final QueryBlockTreeListener rootBlock, final PatternListener patternListener) {

        this.returnTemplateListener = returnTemplateListener;
        this.rootBlock = rootBlock;
        this.patetrnListener = patternListener;
        PRQueryBlockTreeBuilderAlternative.realtionsFreshVaribales = new LinkedHashMap<String, Map<String, Object>>();
    }

    @Override
    public PathExpression buildPathExpression(final String str) throws ParseException {
        return null;
    }

    /**
     * The definitions are not used.
     */
    @Override
    public ReturnTemplate buildReturnTemplate(ImmutableMap<Variable, PathExpression> definitions, final String str)
            throws ParseException {
        return returnTemplateListener.parse(str);
    }

    @Override
    public QueryBlockTree buildQueryBlockTree(final String str) throws ParseException {

        return rootBlock.parse(str);
    }

    @Override
    public Pattern buildPattern(String str) throws ParseException {
        return patetrnListener.parse(str);
    }

    /**
     * Gets Variable Mapping in a pattern
     * 
     * @return variableMapping in this pattern
     */
    public VariableMapper getVariableMapper() {
        return patetrnListener.getVariableMapper();
    }

    /**
     * Sets the relations that are used in a current NBTbuilder
     * 
     * @param relations
     */
    public void setRelations(Map<String, Relation> relations) {
        PRQueryBlockTreeBuilderAlternative.relations = relations;

    }

    /**
     * Reset variables
     */
    public void resetVariables() {
        realtionsFreshVaribales.clear();
    }
}
