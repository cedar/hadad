/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.pr;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.ReturnConstructTerm;
import hadad.base.compiler.ReturnStringTerm;
import hadad.base.compiler.ReturnTemplate;
import hadad.base.compiler.IReturnTemplateConstructor;
import hadad.base.compiler.IReturnTemplateVisitor;
import hadad.base.compiler.ReturnVariableTerm;
import hadad.base.compiler.Row;
import hadad.base.compiler.exceptions.ReturnConstructException;

/**
 * PR ReturnTemplateConstructor which implements {@link IReturnTemplateConstructor}.
 * 
 * @author ranaalotaibi
 */
public class PRReturnTemplateConstructor implements IReturnTemplateConstructor {
    private static final Logger LOGGER = Logger.getLogger(PRReturnTemplateConstructor.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    private final PRConstructReturnTermVisitor constructVisitor;

    public PRReturnTemplateConstructor() {
        constructVisitor = new PRConstructReturnTermVisitor();
    }

    @Override
    public final String construct(final ReturnTemplate template, final Row row) throws ReturnConstructException {
        return constructVisitor.construct(template, row);
    }

    private class PRConstructReturnTermVisitor implements IReturnTemplateVisitor {
        private StringBuilder builder;
        private Row row;

        public String construct(final ReturnTemplate template, final Row row) throws ReturnConstructException {
            builder = new StringBuilder();
            this.row = row;
            template.accept(this);
            return builder.toString();
        }

        @Override
        public void visit(final ReturnTemplate template) {
            // NOP (no construction of the optionals)
        }

        @Override
        public void visitPre(final ReturnConstructTerm term) {
            // NOP (no construction)
        }

        @Override
        public void visitPost(final ReturnConstructTerm term) {
            // NOP (no construction)
        }

        @Override
        public void visit(final ReturnVariableTerm term) {
            if (builder.length() > 0) {
                builder.append(",");
            }
            if (row == null) {
                builder.append(term.getVariable().toString());
            } else {
                builder.append(row.getValue(term.getVariable()).toString());
            }
        }

        @Override
        public void visit(final ReturnStringTerm term) {
            if (builder.length() > 0) {
                builder.append(",");
            }
            builder.append(term.toString());
        }
    }
}
