/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.pr;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import hadad.base.compiler.Condition;
import hadad.base.compiler.ReturnTerm;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.StringConstant;
import hadad.commons.conjunctivequery.Variable;

public class Utils {
    public final static String NOLOOKUP = "0";

    public static StringConstant toTerm(final String str) {
        return new StringConstant(str);
    }

    public static Variable toTermVar(final String str) {
        return new Variable(str);
    }

    public static Function<Condition, List<Atom>> conditionEncoding = c -> new ArrayList<Atom>(
            Arrays.asList(new Atom(Predicate.EQUALS.toString(), c.getLeftOp().getTerm(), c.getRightOp().getTerm())));

    public static boolean isObject(final ReturnTerm term) {
        return term.hasParent() && !term.getParent().getElement().isEmpty();
    }
}
