/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.qbt;

import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.google.inject.Singleton;

import hadad.base.compiler.PathExpression;
import hadad.base.compiler.Pattern;
import hadad.base.compiler.QueryBlockTree;
import hadad.base.compiler.IQueryBlockTreeBuilder;
import hadad.base.compiler.ReturnTemplate;
import hadad.base.compiler.VariableMapper;
import hadad.base.compiler.exceptions.ParseException;
import hadad.base.compiler.model.aj.AJQueryBlockTreeBuilder;
import hadad.base.compiler.model.aj.naive.AJNaiveModule;
import hadad.base.compiler.model.pj.full.PJFULLQueryBlockTreeBuilder;
import hadad.base.compiler.model.pj.naive.PJNaiveModule;
import hadad.base.compiler.model.pr.PRNaiveModule;
import hadad.base.compiler.model.pr.PRQueryBlockTreeBuilderAlternative;
import hadad.base.compiler.model.pr.Relation;
import hadad.base.compiler.model.sj.SJQueryBlockTreeBuilder;
import hadad.base.compiler.model.sj.naive.SJNaiveModule;
import hadad.commons.conjunctivequery.Variable;

/**
 * QBT QueryBlockTreeBuilder
 * 
 * @author ranaalotaibi
 */
@Singleton
public final class QBTQueryBlockTreeBuilder implements IQueryBlockTreeBuilder {
    private static final Logger LOGGER = Logger.getLogger(QBTQueryBlockTreeBuilder.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    static AJQueryBlockTreeBuilder ajBlockNestedTreeBuilder;
    static PRQueryBlockTreeBuilderAlternative prBlockNestedTreeBuilder;
    static PJFULLQueryBlockTreeBuilder pjBlockNestedTreeBuilder;
    static SJQueryBlockTreeBuilder sjBlockNestedTreeBuilder;
    public static VariableMapper variableMapper;
    public static Map<String, Relation> relations;
    private final PatternListener patternListener;
    private final ReturnTemplateListener returnTemplateListener;
    private final QBTQueryBlockTreeListener queryBlockTreeListener;

    @Inject
    public QBTQueryBlockTreeBuilder(final QBTQueryBlockTreeListener queryBlockTreeListener,
            final PatternListener patternListener, final ReturnTemplateListener returnTemplateListener,
            final VariableMapper variableMapper) {
        Injector injectorAJ = Guice.createInjector(new AJNaiveModule());
        Injector injectorRQ = Guice.createInjector(new PRNaiveModule());
        Injector injectorPJ = Guice.createInjector(new PJNaiveModule());
        Injector injectorSJ = Guice.createInjector(new SJNaiveModule());

        this.queryBlockTreeListener = queryBlockTreeListener;
        this.patternListener = patternListener;
        this.returnTemplateListener = returnTemplateListener;
        QBTQueryBlockTreeBuilder.variableMapper = variableMapper;
        QBTQueryBlockTreeBuilder.ajBlockNestedTreeBuilder = injectorAJ.getInstance(AJQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder.prBlockNestedTreeBuilder =
                injectorRQ.getInstance(PRQueryBlockTreeBuilderAlternative.class);
        QBTQueryBlockTreeBuilder.pjBlockNestedTreeBuilder = injectorPJ.getInstance(PJFULLQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder.sjBlockNestedTreeBuilder = injectorSJ.getInstance(SJQueryBlockTreeBuilder.class);

    }

    @Override
    public QueryBlockTree buildQueryBlockTree(final String str) throws ParseException {
        QueryBlockTree mixedModelNestedBlockTree = queryBlockTreeListener.parse(str);
        return mixedModelNestedBlockTree;
    }

    @Override
    public Pattern buildPattern(String str) throws ParseException {

        return patternListener.parse(str);
    }

    @Override
    public PathExpression buildPathExpression(String str) throws ParseException {
        return null;
    }

    @Override
    public ReturnTemplate buildReturnTemplate(ImmutableMap<Variable, PathExpression> definitions, String str)
            throws ParseException {
        return returnTemplateListener.parse(str);
    }

    public void setRealtions(Map<String, Relation> relations) {
        PRQueryBlockTreeBuilderAlternative.relations = relations;
    }

    public static void reIntilize() {
        Injector injectorAJ = Guice.createInjector(new AJNaiveModule());
        Injector injectorRQ = Guice.createInjector(new PRNaiveModule());
        Injector injectorPJ = Guice.createInjector(new PJNaiveModule());
        Injector injectorSJ = Guice.createInjector(new SJNaiveModule());
        QBTQueryBlockTreeBuilder.ajBlockNestedTreeBuilder = injectorAJ.getInstance(AJQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder.prBlockNestedTreeBuilder =
                injectorRQ.getInstance(PRQueryBlockTreeBuilderAlternative.class);
        QBTQueryBlockTreeBuilder.pjBlockNestedTreeBuilder = injectorPJ.getInstance(PJFULLQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder.sjBlockNestedTreeBuilder = injectorSJ.getInstance(SJQueryBlockTreeBuilder.class);
        QBTQueryBlockTreeBuilder.variableMapper = new VariableMapper();
        PRQueryBlockTreeBuilderAlternative.relations = null;
    }

}
