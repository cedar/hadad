/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.qbt;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.AntlrUtils;
import hadad.base.compiler.ReturnTemplate;
import hadad.base.compiler.exceptions.HadadCompilationException;
import hadad.base.compiler.exceptions.ParseException;

/**
 * QBT ReturnTemplateListener
 * 
 * @author ranaalotaibi
 */
@Singleton
final class ReturnTemplateListener {
    private static final Logger LOGGER = Logger.getLogger(ReturnTemplateListener.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }

    @Inject
    public ReturnTemplateListener() {
    }

    public ReturnTemplate parse(final String str) throws ParseException {

        final QBTLexer lexer = new QBTLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final QBTParser parser = new QBTParser(tokens);
        final ParserRuleContext tree = parser.qbtReturnPattern();
        final ParseTreeWalker walker = new ParseTreeWalker();
        final ReturnListenerAux listener = new ReturnListenerAux();

        try {
            walker.walk(listener, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        if (listener.getReturnTemplate() == null) {
            throw new ParseException(new IllegalStateException("Return Pattern expected."));
        }
        return listener.getReturnTemplate();
    }

    private class ReturnListenerAux extends QBTBaseListener {
        private ReturnTemplate returnTemplate;

        public ReturnTemplate getReturnTemplate() {
            return returnTemplate;
        }

        @Override
        public void enterConstructor(QBTParser.ConstructorContext ctx) {
            LOGGER.debug("Entering ReturnPattern: " + ctx.getText());
            try {
                switch (ctx.annotation().getText()) {
                    case "AJ":
                        returnTemplate = QBTQueryBlockTreeBuilder.ajBlockNestedTreeBuilder.buildReturnTemplate(null,
                                "return " + AntlrUtils.getFullText(ctx.modelConstructor().ajConstructor()));
                        break;
                    case "PJ":
                        returnTemplate = QBTQueryBlockTreeBuilder.pjBlockNestedTreeBuilder.buildReturnTemplate(null,
                                "SELECT " + AntlrUtils.getFullText(ctx.modelConstructor().pjConstructor()));
                        break;

                    case "PR":
                        returnTemplate = QBTQueryBlockTreeBuilder.prBlockNestedTreeBuilder.buildReturnTemplate(null,
                                "SELECT " + AntlrUtils.getFullText(ctx.modelConstructor().prConstructor()));
                        break;
                    case "SJ":
                        returnTemplate = QBTQueryBlockTreeBuilder.sjBlockNestedTreeBuilder.buildReturnTemplate(null,
                                AntlrUtils.getFullText(ctx.modelConstructor().sjConstructor()));
                        break;
                    case "SPPJ":
                }

            } catch (ParseException e) {
                throw new HadadCompilationException(e);
            }

        }

    }
}
