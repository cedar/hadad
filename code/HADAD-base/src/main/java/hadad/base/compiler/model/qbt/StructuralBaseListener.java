/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.qbt;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import hadad.base.compiler.Pattern;
import hadad.base.compiler.exceptions.ParseException;

/**
 * QBT StructuralBaseListener which extends {@link QBTBaseListener}.
 * 
 * @author ranaalotaibi
 * 
 */
abstract class StructuralBaseListener extends QBTBaseListener {
    /*
     * Used to keep the introduced Patterns of each pattern being parsed
     */
    private Pattern patterns;

    public Pattern parse(final String str) throws ParseException {
        patterns = null;
        final QBTLexer lexer = new QBTLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final QBTParser parser = new QBTParser(tokens);
        final ParserRuleContext tree = createParseTree(parser);
        final ParseTreeWalker walker = new ParseTreeWalker();
        try {
            walker.walk(this, tree);
        } catch (IllegalStateException e) {
            throw new ParseException(e);
        }
        return patterns;
    }

    protected void addPattern(final Pattern pattern) {
        if (pattern == null) {
            throw new IllegalStateException("Pattern expected.");
        }
        if (patterns == null)
            patterns = pattern;
        else
            patterns = patterns.mergePattern(pattern);

    }

    protected abstract ParserRuleContext createParseTree(final QBTParser parser);
}
