/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.qbt;

import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.AntlrUtils;
import hadad.base.compiler.Pattern;
import hadad.base.compiler.exceptions.HadadCompilationException;
import hadad.base.compiler.exceptions.ParseException;

/**
 * QBT StructuralListener
 * 
 * @author ranaalotaibi
 */
@Singleton
public final class StructuralListener extends StructuralBaseListener {
    private static final Logger LOGGER = Logger.getLogger(StructuralListener.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }

    @Inject
    public StructuralListener() {
        LOGGER.setLevel(Level.OFF);
    }

    @Override
    public void enterQbtPattern(QBTParser.QbtPatternContext ctx) {
        try {
            LOGGER.debug("Pattern: " + ctx.getText());
            switch (ctx.annotation().getText()) {
                case "AJ":
                    addPattern(QBTQueryBlockTreeBuilder.ajBlockNestedTreeBuilder
                            .buildPattern("for " + AntlrUtils.getFullText(ctx.modelPattern().ajPattern())));
                    QBTQueryBlockTreeBuilder.variableMapper
                            .addAll((QBTQueryBlockTreeBuilder.ajBlockNestedTreeBuilder.getVariableMapper()));
                    break;
                case "PJ":
                    final Pattern pattern = QBTQueryBlockTreeBuilder.pjBlockNestedTreeBuilder
                            .buildQueryBlockTree(AntlrUtils.getFullText(ctx.modelPattern().pjPattern())).getRoot()
                            .getPattern();
                    addPattern(pattern);
                    QBTQueryBlockTreeBuilder.variableMapper
                            .addAll((QBTQueryBlockTreeBuilder.pjBlockNestedTreeBuilder.getVariableMapper()));
                    break;

                case "SJ":
                    addPattern(QBTQueryBlockTreeBuilder.sjBlockNestedTreeBuilder
                            .buildPattern(AntlrUtils.getFullText(ctx.modelPattern().sjPattern())));
                    QBTQueryBlockTreeBuilder.variableMapper
                            .addAll((QBTQueryBlockTreeBuilder.sjBlockNestedTreeBuilder.getVariableMapper()));
                    break;

            }
        } catch (ParseException e) {
            throw new HadadCompilationException(e);
        }
    }

    @Override
    protected ParserRuleContext createParseTree(QBTParser parser) {
        return parser.qbtForPattern();
    }
}
