/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.sj;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import hadad.base.compiler.ReturnTerm;
import hadad.base.compiler.VariableFactory;
import hadad.commons.conjunctivequery.Variable;

/**
 * Array Element Factory
 * 
 * @author ranaalotaibi
 *
 */
@Singleton
public class ArrayElementFactory {
    private static final Logger LOGGER = Logger.getLogger(ArrayElementFactory.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    private final VariableFactory variableFactory;
    private final Map<ReturnTerm, Variable> term2element;

    @Inject
    public ArrayElementFactory(@Named("ArrayElementVariableFactory") final VariableFactory variableFactory) {
        this.variableFactory = variableFactory;
        term2element = new HashMap<>();
    }

    public Variable getElement(final ReturnTerm term) {
        if (!term2element.containsKey(term)) {
            term2element.put(term, variableFactory.createFreshVar());
        }
        return term2element.get(term);
    }
}
