/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.sj;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.ConditionTerm;
import hadad.base.compiler.PathExpression;
import hadad.base.compiler.VariableMapper;
import hadad.base.compiler.exceptions.ParseException;
import hadad.commons.conjunctivequery.StringConstant;
import hadad.commons.conjunctivequery.Term;
import hadad.commons.conjunctivequery.Variable;

/**
 * SJ Condition Term Listener
 * 
 * @author ranaalotaibi
 */
@Singleton
final class ConditionTermListener extends SJQLBaseListener {
    private static final Logger LOGGER = Logger.getLogger(ConditionTermListener.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    protected final PathExpressionListener pathExpressionListener;
    protected final VariableMapper variableMapper;
    private Set<Variable> referredVariables;
    private Term term;
    private List<PathExpression> pathExpressions;

    @Inject
    public ConditionTermListener(final PathExpressionListener listener, final VariableMapper variableMapper) {
        this.pathExpressionListener = listener;
        this.variableMapper = variableMapper;
        LOGGER.setLevel(Level.OFF);
    }

    /**
     * Parse the term
     * 
     * @param str
     *            the term that needs to be parsed
     * @return constructed condition term
     */
    public ConditionTerm parse(final String str) throws ParseException {
        referredVariables = new HashSet<Variable>();
        term = null;
        pathExpressions = new ArrayList<PathExpression>();

        final SJQLLexer lexer = new SJQLLexer(CharStreams.fromString(str));
        final CommonTokenStream tokens = new CommonTokenStream(lexer);
        final SJQLParser parser = new SJQLParser(tokens);
        final ParserRuleContext tree = parser.sjTerm();
        final ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(this, tree);
        return new ConditionTerm(term, referredVariables, pathExpressions);

    }

    @Override
    public void enterSjFieldName(SJQLParser.SjFieldNameContext ctx) {
        LOGGER.debug("Entering FieldName: " + ctx.getText());
        final Variable var = variableMapper.getVariable(ctx.getText());
        referredVariables.add(var);
        term = var;
    }

    @Override
    public void enterSjConstant(SJQLParser.SjConstantContext ctx) {
        LOGGER.debug("Entering Constant: " + ctx.getText());
        term = new StringConstant(ctx.getText().substring(1, ctx.getText().length() - 1));

    }
}
