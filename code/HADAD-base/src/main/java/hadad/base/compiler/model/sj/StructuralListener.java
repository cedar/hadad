/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.sj;

import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import hadad.base.compiler.AntlrUtils;
import hadad.base.compiler.PathExpression;
import hadad.base.compiler.VariableFactory;
import hadad.base.compiler.VariableMapper;
import hadad.base.compiler.exceptions.HadadCompilationException;
import hadad.base.compiler.exceptions.ParseException;
import hadad.commons.conjunctivequery.Variable;

/**
 * SJ Structural Listener
 * 
 * @author ranaalotaibi
 *
 */
@Singleton
public final class StructuralListener extends StructuralBaseListener {
    private static final Logger LOGGER = Logger.getLogger(StructuralListener.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }

    @Inject
    public StructuralListener(final PathExpressionListener pathExpressionlistener,
            @Named("SJQLVariableFactory") VariableFactory sjqlVariableFactory, VariableMapper variableMapper) {
        super(pathExpressionlistener, sjqlVariableFactory, variableMapper);
        LOGGER.setLevel(Level.OFF);
    }

    @Override
    public void enterSjQuery(SJQLParser.SjQueryContext ctx) {
        LOGGER.debug("Entering Query: " + ctx.getText());
        if (currentVar != null) {
            throw new IllegalStateException("Path expression expected.");
        }
        final Variable var = sjqlVariableFactory.createFreshVar();
        variableMapper.define(ctx.sjCollectionName().getText(), var);
        currentVar = var;
        PathExpression expr;
        try {
            expr = pathExpressionListener.parse(AntlrUtils.getFullText(ctx)).copy(currentVar);
            defineVariable(expr);
            for (Map.Entry<String, Variable> entry : variableMapper.getVariableMapping().entrySet()) {
                if (!entry.getKey().equals(ctx.sjCollectionName().getText())) {
                    currentVar = entry.getValue();
                    defineVariable(expr);
                }
            }
        } catch (ParseException e) {
            throw new HadadCompilationException(e);
        }

    }

    @Override
    protected ParserRuleContext createParseTree(final SJQLParser parser) {
        return parser.sjQuery();
    }
}
