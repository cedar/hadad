/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.sj.naive;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import hadad.base.compiler.ChildBlock;
import hadad.base.compiler.QueryBlockTree;
import hadad.base.compiler.IQueryBlockTreeVisitor;
import hadad.base.compiler.RootBlock;
import hadad.base.compiler.model.sj.SJExtractVariableToCreatedNodeVisitor;
import hadad.base.compiler.model.sj.Utils;
import hadad.base.rewriter.Comment;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.Variable;
import hadad.commons.constraints.Constraint;
import hadad.commons.constraints.Tgd;

/**
 * SJ Forward Encoder Nested Block Tree Visitor
 * 
 * 
 *
 */
@Singleton
class SJForwardEncoderQueryBlockTreeVisitor implements IQueryBlockTreeVisitor {
    private final SJExtractVariableToCreatedNodeVisitor sJExtractVariableToCreatedNodeVisitor;
    private Variable viewID;
    private ImmutableList.Builder<Constraint> builder;
    private boolean includeComments;

    @Inject
    public SJForwardEncoderQueryBlockTreeVisitor(
            final SJExtractVariableToCreatedNodeVisitor extractVariableToCreatedNodeVisit) {
        this.sJExtractVariableToCreatedNodeVisitor = extractVariableToCreatedNodeVisit;
        this.viewID = null;

    }

    public List<Constraint> compileConstraints(final QueryBlockTree nbt, final Variable viewID,
            boolean includeComments) {
        this.viewID = viewID;
        builder = ImmutableList.builder();
        this.includeComments = includeComments;
        nbt.accept(this);
        return builder.build();
    }

    @Override
    public void visit(final QueryBlockTree tree) {
        // TODO Auto-generated method stub
    }

    @Override
    public void visit(final RootBlock block) {
        if (!block.getPattern().isEmpty()) {
            if (includeComments) {
                builder.add(new Comment(block.getQueryName() + " constraint for Body Encoding"));
            }
            builder.add(getForwardConstraintForBodyEncoding(block));
        }
    }

    @Override
    public void visit(final ChildBlock block) {
        // TODO Auto-generated method stub
    }

    /**
     * Get forward constraints
     * 
     * @param qbtBlock
     *            the qbt block
     * @return the constraints
     */
    private Constraint getForwardConstraintForBodyEncoding(final RootBlock qbtBlock) {
        final String viewName = qbtBlock.getQueryName();
        final List<Atom> premise = new ArrayList<Atom>();
        premise.addAll(qbtBlock.getPattern().encoding(Utils.conditionEncoding));
        final List<Atom> conclusion = new ArrayList<Atom>();
        sJExtractVariableToCreatedNodeVisitor.encode(viewID, viewName);
        conclusion.addAll(sJExtractVariableToCreatedNodeVisitor.encode(qbtBlock.getReturnTemplate()));
        conclusion.add(Utils.createRootAtom(viewID, viewName));
        return new Tgd(premise, conclusion);
    }

}