/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.expression.generator;

/**
 * Binary Expresssion
 * 
 * @author ranaalotaibi
 *
 */
public class BinaryExpression implements Expression {

    final private String operator;
    final private IRule rule;

    /**
     * Constructor
     * 
     * @param operator
     *            the operator
     * @param rule
     *            the non-terminal rule
     */
    public BinaryExpression(final String operator, final IRule rule) {
        this.operator = operator;
        this.rule = rule;
    }

    /**
     * Get rule
     * 
     * @return the rule
     */
    public IRule getRule() {
        return rule;
    }

    /**
     * Get operator
     * 
     * @return the operator
     */
    public String getOperator() {
        return operator;
    }

    @Override
    public String toString() {
        final StringBuffer str = new StringBuffer();
        str.append(operator);
        str.append(rule.getRuleHead());
        return str.toString();
    }

    @Override
    public ExpressionType getType() {
        return ExpressionType.BINARYXPRESSION;
    }
}
