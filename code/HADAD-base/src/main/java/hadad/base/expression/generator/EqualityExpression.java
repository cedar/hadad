/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.expression.generator;

import java.util.List;

/**
 * Equality Expression
 * 
 * @author ranaalotaibi
 *
 */
public class EqualityExpression implements Expression {

    final static String EQ = "=";
    final private IRule left;
    final private List<IRule> right;

    /** Constructor **/
    public EqualityExpression(final IRule left, final List<IRule> right) {
        this.left = left;
        this.right = right;
    }

    public IRule getLeft() {
        return left;
    }

    public List<IRule> getRight() {
        return right;
    }

    @Override
    public String toString() {
        final StringBuffer str = new StringBuffer();
        str.append(left.getRuleHead().toString());
        str.append(EQ);
        for (final IRule rule : right) {
            str.append(rule.getRuleHead().toString());
        }
        return str.toString();
    }

    @Override
    public ExpressionType getType() {
        return ExpressionType.EQUALITYEXPRESSION;
    }
}
