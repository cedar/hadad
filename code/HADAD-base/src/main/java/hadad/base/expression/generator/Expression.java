package hadad.base.expression.generator;

public interface Expression {
    /**
     * Get expression type
     * 
     * @return expression type
     */
    public ExpressionType getType();
}
