/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.expression.generator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * Quick implementation
 * TODO: To be refined.
 * 
 * @author ranaalotaibi
 *
 */
public class IRGCFParser {
    private final Stack<IRule> rulesStack = new Stack<>();
    private final Map<String, IRule> rulesMapping = new HashMap<>();

    public void parse() throws IOException {
        InputStream inputStream = null;
        try {
            final File file = new File("src/main/resources/exprGenertor/IR-GCF-LA.gr");
            final BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String line;
            int count = 1;
            while ((line = br.readLine()) != null) {

                switch (count) {
                    case 1:
                    case 2:
                        final IRule rule = processTermianl(line);
                        rulesStack.push(rule);
                        rulesMapping.put(rule.getRuleHead().getRuleHeadName(), rule);
                        break;
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 10:
                    case 11:
                        final IRule unarRule = processUnaryExpr(line);
                        rulesStack.push(unarRule);
                        rulesMapping.put(unarRule.getRuleHead().getRuleHeadName(), unarRule);
                        break;
                    case 12:
                        final IRule tRule = processTRule(line);
                        rulesStack.push(tRule);
                        rulesMapping.put(tRule.getRuleHead().getRuleHeadName(), tRule);
                        break;
                    case 13:
                    case 14:
                    case 15:
                    case 16:
                    case 17:
                        final IRule binaryRule = processBinaryExpr(line);
                        rulesStack.push(binaryRule);
                        rulesMapping.put(binaryRule.getRuleHead().getRuleHeadName(), binaryRule);
                        break;
                    case 18:
                    case 19:
                        final IRule binOrUnRule = processBinorUnRule(line);
                        rulesStack.push(binOrUnRule);
                        rulesMapping.put(binOrUnRule.getRuleHead().getRuleHeadName(), binOrUnRule);
                        break;
                    case 20:
                        final IRule eRule = processERule(line);
                        rulesStack.push(eRule);
                        rulesMapping.put(eRule.getRuleHead().getRuleHeadName(), eRule);
                        break;
                    case 21:
                        final IRule sRule = processSRule(line);
                        rulesStack.push(sRule);
                        rulesMapping.put(sRule.getRuleHead().getRuleHeadName(), sRule);
                        break;

                }
                count++;
            }

        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Stack<IRule> getRules() {
        return rulesStack;
    }

    private IRule processTermianl(final String str) {
        //Parse
        final String[] rule = str.split("->");
        final String head = rule[0];
        final String[] body = rule[1].split(",");
        final String nameMatrix = body[0];
        final int numofIteration = Integer.parseInt(body[1]);
        //Construct the rule
        final RuleHead ruleHead = new RuleHead(head);
        final StringExpression strExp = new StringExpression(nameMatrix);
        final RuleBody ruleBody = new RuleBody(strExp);
        return new TerminalRule(ruleHead, ruleBody, numofIteration);

    }

    private IRule processTRule(final String str) {
        //Parse
        final String[] rule = str.split("->");
        final String head = rule[0];
        final String[] body = rule[1].split(",");
        final String[] listofNames = body[0].split("\\|");
        final List<String> names = Arrays.asList(listofNames);

        final int numofIteration = Integer.parseInt(body[1]);
        //Construct the rule
        final RuleHead ruleHead = new RuleHead(head);
        final OrExpressionsLiterals expression = new OrExpressionsLiterals(names);
        final RuleBody ruleBody = new RuleBody(expression);
        return new NonTerminalRule(ruleHead, ruleBody, numofIteration);
    }

    private IRule processBinaryExpr(final String str) {
        //Parse
        final String[] rule = str.split("->");
        final String head = rule[0];
        final String[] body = rule[1].split(",");
        final String operator = String.valueOf(body[0].charAt(0));
        final String tRule = String.valueOf(body[0].charAt(1));
        final int numofIteration = Integer.parseInt(body[1]);
        //Construct the rule
        final RuleHead ruleHead = new RuleHead(head);
        final BinaryExpression binaryExpr = new BinaryExpression(operator, rulesMapping.get(tRule));
        final RuleBody ruleBody = new RuleBody(binaryExpr);
        return new NonTerminalRule(ruleHead, ruleBody, numofIteration);
    }

    private IRule processUnaryExpr(final String str) {
        //Parse
        final String[] rule = str.split("->");
        final String head = rule[0];
        final String[] body = rule[1].split(",");
        final String operator = body[0];
        final int numofIteration = Integer.parseInt(body[1]);
        //Construct the rule
        final RuleHead ruleHead = new RuleHead(head);
        final UnaryExpression binaryExpr = new UnaryExpression(operator);
        final RuleBody ruleBody = new RuleBody(binaryExpr);
        return new NonTerminalRule(ruleHead, ruleBody, numofIteration);
    }

    private IRule processBinorUnRule(final String str) {
        //Parse
        final String[] rule = str.split("->");
        final String head = rule[0];
        final String[] body = rule[1].split(",");
        final String[] listofNames = body[0].split("\\|");
        final List<String> names = Arrays.asList(listofNames);
        final List<IRule> orExprRules = new ArrayList<>();

        for (String name : names) {
            orExprRules.add(rulesMapping.get(name));
        }
        final int numofIteration = Integer.parseInt(body[1]);
        //Construct the rule
        final RuleHead ruleHead = new RuleHead(head);
        final OrExpressionsRules expression = new OrExpressionsRules(orExprRules);
        final RuleBody ruleBody = new RuleBody(expression);
        return new NonTerminalRule(ruleHead, ruleBody, numofIteration);
    }

    private IRule processERule(final String str) {
        //Parse
        final String[] rule = str.split("->");
        final String head = rule[0];
        final String[] body = rule[1].split(",");
        final String[] listofNames = body[0].split("\\|");
        final List<String> names = Arrays.asList(listofNames);
        final List<IRule> orExprRules = new ArrayList<>();

        for (String name : names) {
            orExprRules.add(rulesMapping.get(name));
        }
        final int numofIteration = Integer.parseInt(body[1]);
        //Construct the rule
        final RuleHead ruleHead = new RuleHead(head);
        final OrExpressionsRules expression = new OrExpressionsRules(orExprRules);
        final RuleBody ruleBody = new RuleBody(expression);
        return new NonTerminalRule(ruleHead, ruleBody, numofIteration);
    }

    private IRule processSRule(final String str) {
        //Parse
        final String[] rule = str.split("->");
        final String head = rule[0];
        final String[] body = rule[1].split(",");
        final String[] equality = body[0].split("=");
        final int numofIteration = Integer.parseInt(body[1]);
        final String tRule = String.valueOf(equality[1].charAt(0));
        final String eRule = String.valueOf(equality[1].charAt(1));
        final List<IRule> rules = new ArrayList<>();
        rules.add(rulesMapping.get(tRule));
        rules.add(rulesMapping.get(eRule));

        //Construct the rule
        final RuleHead ruleHead = new RuleHead(head);
        final EqualityExpression expression = new EqualityExpression(rulesMapping.get(equality[0]), rules);
        final RuleBody ruleBody = new RuleBody(expression);
        return new NonTerminalRule(ruleHead, ruleBody, numofIteration);
    }

}
