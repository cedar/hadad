/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.expression.generator;

import java.util.logging.Logger;

/**
 * Production Rule Abstract Class
 * 
 * @author ranaalotaibi
 *
 */
public abstract class IRule {
    private final static Logger LOGGER = Logger.getLogger(IRule.class.getName());

    /** Start rule flag */
    protected boolean startRule = false;
    /** Rule head */
    private RuleHead ruleHead;
    /** Rule body **/
    private RuleBody ruleBody;
    /** Rule iteration **/
    private int ruleIteration;

    /** Constructor **/
    public IRule(final RuleHead ruleHead, final RuleBody ruleBody, final int ruleIteration) {
        this.ruleHead = ruleHead;
        this.ruleBody = ruleBody;
        this.ruleIteration = ruleIteration;
    }

    /**
     * Get rule head
     * 
     * @return rule head
     */
    public RuleHead getRuleHead() {
        return ruleHead;
    }

    /**
     * Get rule body
     * 
     * @return rule body
     */
    public RuleBody getRuleBody() {
        return ruleBody;
    }

    /**
     * Get rule iteration
     * 
     * @return rule iteration
     */
    public int getRuleIteration() {
        return ruleIteration;
    }

    /**
     * Get Rule type
     * 
     * @return rule type
     */
    public abstract RuleType getType();

    @Override
    public String toString() {
        final StringBuffer str = new StringBuffer();
        str.append(ruleHead.toString());
        str.append("->");
        str.append(ruleBody.toString());
        str.append(",");
        str.append(ruleIteration);
        str.append("\n");
        return str.toString();
    }
}
