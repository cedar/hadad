/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.expression.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LAExpressionGenerator {

    public static List<String> generate(final IRule startRule, final int numberofIterarion) {

        final List<String> str = new ArrayList<>();
        int count = 0;
        while (count < numberofIterarion) {
            str.add(construct(startRule));
            count++;
        }
        return str;

    }

    private static String construct(final IRule startRule) {
        final StringBuilder str = new StringBuilder();

        //Start Rule - left 
        final EqualityExpression eq = (EqualityExpression) startRule.getRuleBody().getRuleBodyExpression();
        final IRule nameRule = eq.getLeft();
        final StringExpression strExp = (StringExpression) nameRule.getRuleBody().getRuleBodyExpression();
        str.append(strExp.getStringExpression());
        str.append("=");

        //Start Rule - right
        final List<IRule> rightRules = eq.getRight();

        //Process T-rule
        final IRule tRule = rightRules.get(0);
        str.append(getTValue(tRule));

        //Process E-rule
        final IRule eRule = rightRules.get(1);

        for (int i = 0; i < eRule.getRuleIteration(); i++) {
            final IRule binorUnRule = getBinorUnRule(eRule);
            final IRule binorUnExp = getBinorUnExpr(binorUnRule);
            final Expression exp = binorUnExp.getRuleBody().getRuleBodyExpression();
            if (exp.getType().equals(ExpressionType.BINARYXPRESSION)) {
                final BinaryExpression binExpr = (BinaryExpression) exp;
                str.append(binExpr.getOperator());
                str.append(getTValue(binExpr.getRule()));
            } else {
                if (exp.getType().equals(ExpressionType.UNARYEXPRESSION)) {
                    final UnaryExpression binExpr = (UnaryExpression) exp;
                    str.append(binExpr.getUnaryExpression());
                }
            }
        }

        return str.toString();
    }

    private static String getTValue(final IRule tRule) {
        if (tRule.getRuleBody().getRuleBodyExpression() instanceof OrExpressionsLiterals) {
            final OrExpressionsLiterals tExpr = (OrExpressionsLiterals) tRule.getRuleBody().getRuleBodyExpression();
            final Random rand1 = new Random();
            int randomIndex1 = rand1.nextInt(tExpr.getRulesLiterals().size());
            return tExpr.getRulesLiterals().get(randomIndex1);
        } else {
            final StringExpression tExpr = (StringExpression) tRule.getRuleBody().getRuleBodyExpression();
            return tExpr.getStringExpression();
        }

    }

    private static IRule getBinorUnRule(final IRule eRule) {
        final OrExpressionsRules eExpr = (OrExpressionsRules) eRule.getRuleBody().getRuleBodyExpression();
        final Random rand2 = new Random();
        int randomIndex2 = rand2.nextInt(eExpr.getRules().size());
        return eExpr.getRules().get(randomIndex2);
    }

    private static IRule getBinorUnExpr(final IRule binorUnRule) {
        final OrExpressionsRules binorUnExps = (OrExpressionsRules) binorUnRule.getRuleBody().getRuleBodyExpression();
        final Random rand3 = new Random();
        int randomIndex3 = rand3.nextInt(binorUnExps.getRules().size());
        return binorUnExps.getRules().get(randomIndex3);
    }
}
