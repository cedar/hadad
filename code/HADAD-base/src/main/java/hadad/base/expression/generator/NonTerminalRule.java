/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.expression.generator;

import java.util.logging.Logger;

/**
 * No Terminal Rule
 * 
 * @author ranaalotaibi
 *
 */
public class NonTerminalRule extends IRule {
    private final static Logger LOGGER = Logger.getLogger(NonTerminalRule.class.getName());

    public NonTerminalRule(final RuleHead ruleHead, final RuleBody ruleBody, final int ruleIteration) {
        super(ruleHead, ruleBody, ruleIteration);
    }

    @Override
    public RuleType getType() {
        return RuleType.NONTERMINAL;
    }
}
