/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.expression.generator;

import java.util.List;

/**
 * OrExpresssion
 * 
 * @author ranaalotaibi
 *
 */
public class OrExpressionsLiterals implements Expression {
    /** Rules */
    final private List<String> literals;

    /**
     * Constructor
     * 
     * @param rules
     *            the set of rules
     */
    public OrExpressionsLiterals(final List<String> literals) {
        this.literals = literals;
    }

    /**
     * Get rules
     * 
     * @return the rules
     */
    public List<String> getRulesLiterals() {
        return literals;
    }

    @Override
    public String toString() {
        final StringBuffer str = new StringBuffer();
        str.append("[");
        int count = 1;
        for (final String literal : literals) {
            str.append(literal);
            if (count != literals.size())
                str.append("|");
            count++;
        }
        str.append("]");
        return str.toString();
    }

    @Override
    public ExpressionType getType() {
        return ExpressionType.OREXPRESSIONLITERALS;
    }
}
