/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.expression.generator;

import java.util.List;

/**
 * OrExpresssion
 * 
 * @author ranaalotaibi
 *
 */
public class OrExpressionsRules implements Expression {
    /** Rules */
    final private List<IRule> rules;

    /**
     * Constructor
     * 
     * @param rules
     *            the set of rules
     */
    public OrExpressionsRules(final List<IRule> rules) {
        this.rules = rules;
    }

    /**
     * Get rules
     * 
     * @return the rules
     */
    public List<IRule> getRules() {
        return rules;
    }

    @Override
    public String toString() {
        final StringBuffer str = new StringBuffer();
        str.append("[");
        int count = 1;
        for (final IRule rule : rules) {
            str.append(rule.getRuleHead());
            if (count != rules.size())
                str.append("|");
            count++;
        }
        str.append("]");
        return str.toString();
    }

    @Override
    public ExpressionType getType() {
        return ExpressionType.OREXPRESSIONRULES;
    }
}
