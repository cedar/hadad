/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.expression.generator;

/**
 * String Expression
 * 
 * @author ranaalotaibi
 *
 */
public class StringExpression implements Expression {
    private final String value;

    /**
     * Constructor
     * 
     * @param value
     *            string value
     */
    public StringExpression(final String value) {
        this.value = value;
    }

    /**
     * Get expression
     * 
     * @return expression
     */
    public String getStringExpression() {
        final StringBuilder str = new StringBuilder();
        str.append(value);
        return str.toString();
    }

    @Override
    public ExpressionType getType() {
        return ExpressionType.STRINGEXPRESSION;
    }

    @Override
    public String toString() {
        return value;
    }

}
