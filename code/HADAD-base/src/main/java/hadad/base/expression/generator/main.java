/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.expression.generator;

import java.io.IOException;
import java.util.List;

/**
 * LA Expression Genertaor
 * 
 * @author ranaalotaibi
 *
 */
public class main {
    public static void main(String[] args) throws IOException {
        final IRGCFParser parser = new IRGCFParser();
        parser.parse();
        final List<String> expressions = LAExpressionGenerator.generate(parser.getRules().pop(), 200);
        for (final String str : expressions) {
            System.out.println(str);
        }
    }
}
