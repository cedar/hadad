/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.loader;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.exceptions.HadadException;

/**
 * Abstract local file system data reader which implements {@link IDataReader} interface.
 * 
 * @author ranaalotaibi
 */
public abstract class AbstractLocalFileSystemDataReader<T> implements IMatrixReader<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractLocalFileSystemDataReader.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    /** File **/
    protected final File file;
    /** Line iterator file **/
    protected LineIterator iterator;

    /**
     * Constructor.
     * 
     * @param absolutePath
     *            file absolute path.
     */
    public AbstractLocalFileSystemDataReader(final String absolutePath) {
        this(new File(absolutePath));
    }

    /**
     * Constructor.
     * 
     * @param file
     *            file.
     */
    public AbstractLocalFileSystemDataReader(final File file) {

        this.file = file;
    }

    @Override
    public void open() throws HadadException {

        try {
            iterator = FileUtils.lineIterator(file, "UTF-8");
        } catch (IOException e) {
            LOGGER.error("Reading LFS File Faild:" + e.getMessage());
            throw new HadadException(e);
        }
    }

    @Override
    public void close() {

        iterator.close();

    }

    @Override
    public boolean hasNext() {

        return iterator.hasNext();
    }

}
