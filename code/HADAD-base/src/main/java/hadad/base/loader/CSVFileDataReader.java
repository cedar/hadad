/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.loader;

import java.io.File;

import hadad.base.compiler.exceptions.HadadException;

/**
 * CSV file data reader which extends {@link AbstractLocalFileSystemDataReader<T>} abstract class.
 * 
 * @author ranaalotaibi
 */
public class CSVFileDataReader extends AbstractLocalFileSystemDataReader<String[]> {

    private final static String COMMA_DELIMITER = ",";

    /**
     * Constructor.
     * 
     * @param absolutePath
     *            absoulte path.
     */
    public CSVFileDataReader(final String absolutePath) {
        super(absolutePath);
    }

    /**
     * Constructor.
     * 
     * @param file
     * 
     */
    public CSVFileDataReader(final File file) {
        super(file);
    }

    @Override
    public String[] next() throws HadadException {

        return iterator.nextLine().split(COMMA_DELIMITER);
    }

}
