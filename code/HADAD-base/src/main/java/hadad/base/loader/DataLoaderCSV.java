/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.loader;

import java.io.File;
import java.util.Arrays;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.exceptions.HadadException;
import hadad.base.compiler.model.la.builder.estim.MatrixBlock;

/**
 * Data loader with assumption that input matrices fit in memory (no spilling to disk)
 * 
 * @author ranaalotaibi
 */
public final class DataLoaderCSV {
    private static final Logger LOGGER = Logger.getLogger(DataLoaderCSV.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }

    public static MatrixBlock load(final File file) throws HadadException {
        final CSVFileDataReader reader = new CSVFileDataReader(file);
        //First pass for dimension.
        int nRows = 0;
        int nCols = 0;
        try {
            reader.open();
            while (reader.hasNext()) {
                if (nRows == 0) {
                    nCols = reader.next().length;
                }
                reader.next();
                nRows++;
            }
        } catch (HadadException e) {
            throw e;
        }
        reader.close();
        //Second pass to load
        double[] data = new double[(++nRows) * nCols];
        try {
            int ix = 0;
            reader.open();
            while (reader.hasNext()) {
                double[] row = Arrays.stream(reader.next()).mapToDouble(Double::parseDouble).toArray();
                System.arraycopy(row, 0, data, ix * (row.length), row.length);
                ix++;
            }

        } catch (HadadException e) {
            throw e;
        }
        reader.close();
        return new MatrixBlock(data, nRows, nCols);

    }

}
