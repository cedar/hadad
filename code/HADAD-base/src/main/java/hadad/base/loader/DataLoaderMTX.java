/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.loader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;

import hadad.base.compiler.model.la.builder.estim.MatrixBlock;

/**
 * Data loader with assumption that input matrices fit in memory
 * 
 * @author ranaalotaibi
 */
public final class DataLoaderMTX {
    /** Logger **/
    private static final Logger LOGGER = Logger.getLogger(DataLoaderMTX.class);

    public static MatrixBlock load(final File file) throws Exception {
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            String colRowSize = reader.readLine();
            String[] parameters = colRowSize.split(" ");
            int nRows = Integer.parseInt(parameters[0]);
            int nCols = Integer.parseInt(parameters[1]);
            int size = Integer.parseInt(parameters[2]);
            double[] data = new double[size];
            line = reader.readLine();
            while (line != null) {
                parameters = line.split(" ");
                int i = Integer.parseInt(parameters[0]);
                int j = Integer.parseInt(parameters[1]);
                //int val = Integer.parseInt(parameters[2]);
                double val = Double.parseDouble(parameters[2]);
                data[i * nCols + j] = val;
                line = reader.readLine();
            }
            reader.close();
            return new MatrixBlock(data, nRows, nCols);

        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }
}
