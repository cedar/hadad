/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.loader;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.exceptions.HadadException;

/**
 * A class to deserialize an object.
 * 
 * @author ranaalotaibi
 */
public class Deserializer {
    private static final Logger LOGGER = Logger.getLogger(Deserializer.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }

    /**
     * Deserialize an object of type <T>.
     * 
     * @param fileName
     *            a file name that stored the serialized object.
     * @return the deserialized object.
     * @throws HADADException
     */
    @SuppressWarnings("unchecked")
    public static <T> T deserialize(final String fileName) throws HadadException {
        FileInputStream file;
        ObjectInputStream in;
        T object;
        try {
            file = new FileInputStream(fileName);
            in = new ObjectInputStream(file);
            object = (T) in.readObject();
            in.close();
            file.close();
        } catch (IOException | ClassNotFoundException e) {
            LOGGER.error("Object derialization failed: " + e.getMessage());
            throw new HadadException(e);
        }
        return object;
    }
}
