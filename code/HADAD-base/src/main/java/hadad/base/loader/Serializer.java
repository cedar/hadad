/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.loader;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.exceptions.HadadException;

/**
 * A class to serialize an object.
 * 
 * @author ranaalotaibi
 *
 */
public class Serializer {
    private static final Logger LOGGER = Logger.getLogger(Serializer.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }

    /**
     * Serialize an object
     * 
     * @param fileName
     *            a file name to persist the serialized object.
     * @param object
     *            the object to be serialized.
     * @throws HADADException
     */
    public static void serialize(final String fileName, final Serializable object) throws HadadException {
        FileOutputStream file;
        ObjectOutputStream out;
        try {
            file = new FileOutputStream(fileName);
            out = new ObjectOutputStream(file);
            out.writeObject(object);
            out.close();
            file.close();
        } catch (IOException e) {
            LOGGER.error("Object serialization failed: " + e.getMessage());
            throw new HadadException(e);
        }
    }
}
