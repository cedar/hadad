/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriter;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * CB Engine Config
 * 
 * @author ranaalotaibi
 *
 */
public class CBConfig {

    static Properties properties = new Properties();
    final static String CONFIG_PATH = "src/main/resources/CBConfig.properties";

    /**
     * Load configurations
     * 
     * @throws IOException
     *             IOException
     */
    public static void loadConfig() throws IOException {
        try (InputStream input = new FileInputStream(CONFIG_PATH)) {
            properties = new Properties();
            properties.load(input);
        } catch (IOException exception) {
            throw exception;
        }
    }

    /**
     * Get property
     * 
     * @param property
     *            property name/key
     * @return the property value
     */
    public static String getProperty(final String property) {
        return (String) properties.get(property);
    }

    /**
     * Get properties
     * 
     * @return properties
     */
    public static Properties getProperties() {
        return properties;
    }
}
