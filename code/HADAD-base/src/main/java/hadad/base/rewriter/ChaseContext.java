/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriter;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.List;

import com.google.common.collect.ImmutableList;

import hadad.db.canonicaldb.Database;
import hadad.db.chase.Assertion;
import hadad.db.chase.EqualityGenConstraint;
import hadad.db.chase.TupleGenConstraint;

/**
 * ChaseContext
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 *
 */
public final class ChaseContext {
    /* The database. */
    private final Database db;
    /* The assertions for the chase. */
    private final ImmutableList<Assertion> assertions;
    /* The tgds for the chase. */
    private final ImmutableList<TupleGenConstraint> tgds;
    /* The egds for the chase. */
    private final ImmutableList<EqualityGenConstraint> egds;

    public ChaseContext(final Database db, final List<Assertion> assertions, final List<TupleGenConstraint> tgds,
            final List<EqualityGenConstraint> egds) {
        this.db = checkNotNull(db);
        this.assertions = ImmutableList.copyOf(checkNotNull(assertions));
        this.tgds = ImmutableList.copyOf(checkNotNull(tgds));
        this.egds = ImmutableList.copyOf(checkNotNull(egds));
    }

    public Database getDb() {
        return db;
    }

    public List<Assertion> getAssertions() {
        return assertions;
    }

    public List<TupleGenConstraint> getTgds() {
        return tgds;
    }

    public List<EqualityGenConstraint> getEgds() {
        return egds;
    }

    public void flush() {
        db.flush();
        assertions.stream().forEach(Assertion::flush);
        tgds.stream().forEach(TupleGenConstraint::flush);
        egds.stream().forEach(EqualityGenConstraint::flush);
    }
}
