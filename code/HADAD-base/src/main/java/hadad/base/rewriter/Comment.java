/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriter;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.Objects;

import hadad.commons.constraints.Constraint;

/**
 * Quick hack in order to add comments to the generated constraints files easily
 * and quickly.
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 *
 */
public final class Comment extends Constraint {
    /* The comment. */
    private final String comment;

    /**
     * Constructs a new comment constraint with the specified comment message.
     *
     * @param comment
     *            the comment
     */
    public Comment(final String comment) {
        super(null);
        this.comment = checkNotNull(comment);
    }

    @Override
    public String toString() {
        return "\n# " + comment + "\n";
    }

    @Override
    public int hashCode() {
        return Objects.hash(comment);
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Comment) && comment.equals(((Comment) o).comment);
    }

    @Override
    public boolean isEquivalent(Constraint o) {
        return this.equals(o);
    }
}
