/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriter;

import java.util.List;

import hadad.commons.conjunctivequery.ConjunctiveQuery;

/**
 * ConjunctiveQueryRewriter
 * 
 * @author Damian Bursztyn
 * @author ranaalotaibi
 *
 */
public interface ConjunctiveQueryRewriter {
    /**
     * Computes the reformulations of the specified conjunctive query.
     *
     * @param query
     *            The query to be reformulated
     *
     * @return The chase execution time, query assertion generation, restrict
     *         execution time, backchase execution time and reformulations.
     * @throws Exception
     *             exception
     */
    public TimedReformulations getTimedReformulations(final ConjunctiveQuery query) throws Exception;

    /**
     * Computes the reformulations of the specified conjunctive query.
     *
     * @param query
     *            The query to be reformulated.
     *
     * @return The reformulations of the specified conjunctive query with the
     *         specified chase context target schema and backward constraints.
     * @throws Exception
     *             exception
     */
    public List<ConjunctiveQuery> getReformulations(final ConjunctiveQuery query) throws Exception;
}
