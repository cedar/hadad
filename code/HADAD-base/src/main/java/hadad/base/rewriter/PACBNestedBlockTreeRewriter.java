/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org

he License is distributed on an "AS IS" BASIS
icenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under ,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriter;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import hadad.base.compiler.IRewritingSelector;
import hadad.commons.relationalschema.RelationalSchema;
import hadad.commons.relationalschema.parser.ParseException;

/**
 * Nested block tree PACB rewriter implementation.
 *
 * @author Damian Bursztyn
 * @author ranaalotaibi
 */
@Singleton
public class PACBNestedBlockTreeRewriter implements NestedBlockTreeRewriter {
    /*
     * The list of rewriters to be used to rewrite (compile) a conjunctive query
     * (nested block tree) with this compiler. Rewriters are in order of
     * appliance, with the initial query as input for the first one, its
     * rewriting as input for the second one and so on. The output of the last
     * rewriter is the rewriting for the query.
     */
    private final ImmutableList<ConjunctiveQueryRewriter> rewriters;
    /*
     * The selector to be used to select the conjunctive query rewriting.
     */
    private final IRewritingSelector selector;

    @Inject
    public PACBNestedBlockTreeRewriter(
            @Named("rewriters.forward_constraints_file_name_prefix") final String rewritersForwardConstraintsFileNamePrefix,
            @Named("rewriters.backward_constraints_file_name_prefix") final String rewritersBackwardConstraintsFileNamePrefix,
            @Named("rewriters.schemas_file_name_prefix") final String rewritersSchemasFileNamePrefix,
            @Named("rewriters.context_suffixes") final String rewritersContextSuffixes,
            final IRewritingSelector selector)
            throws IOException, ParseException, hadad.commons.constraints.parser.ParseException {
        this.rewriters =
                rewriters(rewritersForwardConstraintsFileNamePrefix, rewritersBackwardConstraintsFileNamePrefix,
                        rewritersSchemasFileNamePrefix, rewritersContextSuffixes.split(","));
        this.selector = checkNotNull(selector);
    }

    private static ImmutableList<ConjunctiveQueryRewriter> rewriters(final String forwardConstraintsFileNamePrefix,
            final String backwardConstraintsFileNamePrefix, final String schemasFileNamePrefix,
            final String[] contextSuffixes)
            throws IOException, ParseException, hadad.commons.constraints.parser.ParseException {
        final ImmutableList.Builder<ConjunctiveQueryRewriter> builder = ImmutableList.builder();
        for (final String contextSuffix : contextSuffixes) {
            final List<RelationalSchema> schemas =
                    hadad.base.rewriter.server.Utils.parseSchemas(schemasFileNamePrefix + contextSuffix);
            builder.add(new PACBConjunctiveQueryRewriter(new Context(schemas.get(0), schemas.get(1),
                    hadad.base.rewriter.server.Utils.parseConstraints(forwardConstraintsFileNamePrefix + contextSuffix),
                    hadad.base.rewriter.server.Utils
                            .parseConstraints(backwardConstraintsFileNamePrefix + contextSuffix))));
        }
        return builder.build();
    }

    @Override
    public List<ConjunctiveQueryRewriter> rewriters() {
        return rewriters;
    }

    @Override
    public IRewritingSelector selector() {
        return selector;
    }
}
