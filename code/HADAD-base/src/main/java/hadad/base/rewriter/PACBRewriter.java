/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriter;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.List;

import hadad.base.compiler.Language;
import hadad.base.compiler.Tuple;
import hadad.base.compiler.exceptions.HadadException;
import hadad.base.compiler.model.la.builder.LAPlanBuilder;
import hadad.base.compiler.model.la.builder.estim.MNCCostEstimator;
import hadad.base.compiler.model.la.builder.estim.NaiveCostEstimator;
import hadad.base.compiler.model.la.builder.estim.NaiveDenseCostEstimator;
import hadad.base.compiler.model.la.builder.operators.BinaryOperator;
import hadad.base.compiler.model.la.builder.operators.IOperator;
import hadad.base.compiler.model.la.builder.operators.UnaryOperator;
import hadad.base.rewriter.legacy.BackchaseWrapper;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.ConjunctiveQuery;
import hadad.commons.constraints.Constraint;
import hadad.commons.constraints.Tgd;
import hadad.commons.relationalschema.Relation;
import hadad.commons.relationalschema.RelationalSchema;
import hadad.db.chase.Assertion;
import hadad.db.chase.Chase;
import hadad.db.chase.DenialConstraint;
import hadad.db.datalogexpr.Value;

/**
 * PACBRewriter
 * 
 * @author ranaalotaibi
 *
 */
//TODO:RANA cleanup pass
public final class PACBRewriter implements ConjunctiveQueryRewriter {
    /* The chase context */
    private final ChaseContext chaseContext;
    /* The backchase context */
    private final BackchaseContext backchaseContext;

    private static List<? extends Constraint> fwConstraints;
    private List<? extends Constraint> bwConstraints;

    /**
     * Constructs a rewriter with the specified context.
     *
     * @param context
     *            The provenance aware chase and backchase algorithm context.
     */
    public PACBRewriter(final Context context) {
        fwConstraints = context.getForwardConstraints();
        bwConstraints = context.getBackwardConstraints();
        chaseContext = ChaseUtils.createChaseContext(checkNotNull(context));
        backchaseContext = BackchaseUtils.createBackchaseContext(checkNotNull(context));
    }

    @Override
    public TimedReformulations getTimedReformulations(final ConjunctiveQuery query) throws Exception {
        flush();

        long start = System.nanoTime();
        final Assertion queryAssertion = ChaseUtils.createQueryAssertion(chaseContext.getDb(), query);
        long finish = System.nanoTime();
        final long queryAssertionTimeInNano = finish - start;

        final List<Assertion> assertions = new ArrayList<Assertion>();
        assertions.add(queryAssertion);
        assertions.addAll(chaseContext.getAssertions());

        final Chase chase = new Chase(chaseContext.getTgds(), chaseContext.getEgds(), new ArrayList<DenialConstraint>(),
                assertions);

        start = System.nanoTime();
        try {
            //chase.run();
            chase.alternativeRunApplied();
        } catch (RuntimeException e) {
            finish = System.nanoTime();
            if (isDocumentNameRelationRelatedException(e)) {
                return new TimedReformulations(queryAssertionTimeInNano, finish - start, -1, -1,
                        new ArrayList<ConjunctiveQuery>());
            } else {
                throw e;
            }
        }
        final ConjunctiveQuery chasedQuery = ChaseUtils.collectResult(query, chaseContext.getDb());
        finish = System.nanoTime();
        final long chaseExecTimeInNano = finish - start;

        start = System.nanoTime();
        final ConjunctiveQuery universalPlan = restrict(chasedQuery, backchaseContext.getTargetSchema());
        finish = System.nanoTime();
        final long restrictExecTimeInNano = finish - start;
        @SuppressWarnings("unchecked")
        final Tuple<Long, List<ConjunctiveQuery>> backchase = BackchaseWrapper.Backchase(query, universalPlan,
                (ArrayList<Constraint>) backchaseContext.getBackwardConstraints(), CBConfig.getProperties());

        final long backchaseExecTimeInNano = backchase.first();
        final List<ConjunctiveQuery> rewritings = backchase.second();

        return new TimedReformulations(queryAssertionTimeInNano, chaseExecTimeInNano, restrictExecTimeInNano,
                backchaseExecTimeInNano, rewritings);
    }

    public List<ConjunctiveQuery> getReformulations(final ConjunctiveQuery query, final Language language,
            final int costmodel) throws Exception {
        flush();

        final Assertion queryAssertion = ChaseUtils.createQueryAssertion(chaseContext.getDb(), query);

        final List<Assertion> assertions = new ArrayList<Assertion>();
        assertions.add(queryAssertion);
        assertions.addAll(chaseContext.getAssertions());

        final Chase chase = new Chase(chaseContext.getTgds(), chaseContext.getEgds(), new ArrayList<DenialConstraint>(),
                assertions);

        try {
            //chase.run();
            chase.alternativeRunApplied();
        } catch (RuntimeException e) {
            if (isDocumentNameRelationRelatedException(e)) {
                return new ArrayList<ConjunctiveQuery>();
            } else {
                throw e;
            }
        }
        final ConjunctiveQuery chasedQuery = ChaseUtils.collectResult(query, chaseContext.getDb());

        final ConjunctiveQuery universalPlan = restrict(chasedQuery, backchaseContext.getTargetSchema());
        @SuppressWarnings("unchecked")
        final ArrayList<Constraint> applicableConstraints =
                getApplicableConstraints(backchaseContext.getBackwardConstraints(), chase.getAppliedTGDs());
        final List<ConjunctiveQuery> rewritings = BackchaseWrapper
                .Backchase(query, universalPlan, applicableConstraints, CBConfig.getProperties()).second();

        if (costmodel == 0) {
            return rewritings;
        }
        if (costmodel == 1 && !language.equals(Language.MR)) {
            return naive(rewritings);
        }
        if (costmodel == 1 && language.equals(Language.MR)) {
            return naiveDense(rewritings);
        }
        if (costmodel == 2) {
            return mnc(rewritings);
        }
        return null;
    }

    private boolean isDocumentNameRelationRelatedException(RuntimeException e) {
        return e.getMessage().startsWith("Retrieving relation \"root_");
    }

    private void flush() {
        Value.restartVarIndex();
        chaseContext.flush();
    }

    /**
     * Returns the restricted query wrt a target schema for the specified query
     * and schema.
     *
     * @param query
     *            The query to be restricted.
     * @param schema
     *            The target schema.
     * @return The restricted query with name "U".
     */
    /*
     * TODO: should we restrict also with the relation arity and access patterns
     * (ie. Relation::equals)?
     *
     * Currently the arity check is handled by the PACB and the access patterns
     * are handled in the encoding.
     */
    private static ConjunctiveQuery restrict(final ConjunctiveQuery query, final RelationalSchema targetSchema) {
        final List<Atom> body = new ArrayList<Atom>();

        for (final Atom atom : query.getBody()) {
            for (final Relation relation : targetSchema.getRelations()) {
                if (relation.getName().equals(atom.getPredicate())) {
                    body.add(atom);
                    break;
                }
            }
        }
        return new ConjunctiveQuery("U", query.getHead(), body);
    }

    private static ArrayList<Constraint> getApplicableConstraints(final List<? extends Constraint> bw,
            final List<String> applied) {
        ArrayList<Constraint> applicable = new ArrayList<Constraint>();
        ArrayList<Tgd> temp = new ArrayList<Tgd>();

        List<Constraint> appliedConstraints = new ArrayList<Constraint>();
        if (applied.isEmpty()) {
            return applicable;
        }
        for (String id : applied) {
            Constraint constraint = findConstraint(id);
            appliedConstraints.add(constraint);

        }
        for (Constraint constraint : bw) {
            if (constraint instanceof Tgd) {
                Tgd tgd = (Tgd) constraint;
                if (tgd.getPremise().size() == 1 && (tgd.getPremise().get(0).getPredicate().contains("add")
                        || tgd.getPremise().get(0).getPredicate().contains("add_s")
                        || tgd.getPremise().get(0).getPredicate().contains("name"))) {
                    applicable.add(tgd);//constraint
                } else {
                    if (!appliedConstraints.contains(tgd)) {
                        applicable.add(tgd);//constraint
                    } else {
                        temp.add(tgd);
                    }
                }
            } else {
                applicable.add(constraint);
            }
        }
        for (Tgd constraint : temp) {
            if (constraint.getPremise().size() == 1 && (constraint.getPremise().get(0).getPredicate().contains("add")
                    || constraint.getPremise().get(0).getPredicate().contains("add_s")
                    || constraint.getPremise().get(0).getPredicate().contains("name"))) {
                continue;
            }
            Tgd tgd = new Tgd(constraint.getConclusion(), constraint.getPremise());
            if (!applicable.contains(constraint) && appliedConstraints.contains(constraint)
                    && appliedConstraints.contains(tgd)) {
                applicable.add(constraint);
            }
        }
        return applicable;

    }

    private static Constraint findConstraint(String id) {
        for (Constraint fw : fwConstraints) {
            if (fw instanceof Tgd) {
                Tgd tgd = (Tgd) fw;
                if (id.equals(tgd.getID())) {
                    return fw;
                }
            }
        }
        return null;
    }

    /**
     * Select rw based on naive estimator (the selection happens after enumerating all rewritings.
     * another implementation is done within PACB) - replace that with the implemntation within the PACB
     * 
     * @param rws
     * @return selected rewriting
     */
    private static List<ConjunctiveQuery> naive(List<ConjunctiveQuery> rws) {
        double minCost = Double.MAX_VALUE;
        ConjunctiveQuery minRW = null;
        // warm-up
        for (final ConjunctiveQuery rw : rws) {
            final IOperator root = LAPlanBuilder.constructPlan(rw);
            NaiveCostEstimator estimator = null;
            if (root != null) {
                estimator = new NaiveCostEstimator();
                if (root instanceof UnaryOperator) {
                    estimator.visit((UnaryOperator) root, null);

                }
                if (root instanceof BinaryOperator) {
                    estimator.visit((BinaryOperator) root, null);

                }
                double currentCost = estimator.getEstimatedCost();
                if (currentCost <= minCost) {
                    minCost = currentCost;
                    minRW = rw;
                }
            }
        }
        List<ConjunctiveQuery> rewritings = new ArrayList<>();
        rewritings.add(minRW);
        return rewritings;
    }

    /**
     * Select rw based on MNC estimator (selection happens after enumerating all rewritings.
     * another implementation is done within PACB) - replace that with the implemntation within the PACB
     * 
     * @param rws
     * @return selected rewriting
     * @throws HadadException
     */
    private static List<ConjunctiveQuery> mnc(List<ConjunctiveQuery> rws) throws HadadException {
        double minCost = Double.MAX_VALUE;
        ConjunctiveQuery minRW = null;
        for (final ConjunctiveQuery rw : rws) {
            final IOperator root = LAPlanBuilder.constructPlan(rw);
            if (root != null) {
                MNCCostEstimator estimator = null;
                estimator = new MNCCostEstimator();
                if (root instanceof UnaryOperator) {
                    estimator.visit((UnaryOperator) root, null);

                }
                if (root instanceof BinaryOperator) {
                    estimator.visit((BinaryOperator) root, null);

                }
                double currentCost = estimator.getEstimatedCost();
                if (currentCost <= minCost) {
                    minCost = currentCost;
                    minRW = rw;
                }
            }
        }
        List<ConjunctiveQuery> rewritings = new ArrayList<>();
        rewritings.add(minRW);
        return rewritings;
    }

    private static List<ConjunctiveQuery> naiveDense(final List<ConjunctiveQuery> rws) {
        double minCost = Double.MAX_VALUE;
        ConjunctiveQuery minRW = null;
        // warm-up
        for (final ConjunctiveQuery rw : rws) {
            final IOperator root = LAPlanBuilder.constructPlan(rw);
            if (root != null) {
                NaiveDenseCostEstimator estimator = new NaiveDenseCostEstimator();
                if (root instanceof UnaryOperator) {
                    estimator.visit((UnaryOperator) root, null);

                }
                if (root instanceof BinaryOperator) {
                    estimator.visit((BinaryOperator) root, null);

                }
                double currentCost = estimator.getEstimatedCost();
                if (currentCost <= minCost) {
                    minCost = currentCost;
                    minRW = rw;
                }
            }
        }

        List<ConjunctiveQuery> rewritings = new ArrayList<>();
        rewritings.add(minRW);
        return rewritings;
    }

    @Override
    public List<ConjunctiveQuery> getReformulations(ConjunctiveQuery query) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

}
