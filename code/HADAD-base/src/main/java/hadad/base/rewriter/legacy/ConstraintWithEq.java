/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriter.legacy;

import java.util.ArrayList;

import hadad.commons.conjunctivequery.Atom;
import hadad.commons.constraints.Equality;

/**
 * A constraint with explicit equalities
 */
public abstract class ConstraintWithEq {

    /** The relational atoms in the premise of the constraint */
    protected ArrayList<Atom> premiseRel;

    /** The equalities in the premise of the constraint */
    protected ArrayList<Equality> premiseEq;

    /**
     * Constructor (to be used by subclasses)
     */
    public ConstraintWithEq() {
        this.premiseRel = new ArrayList<Atom>();
        this.premiseEq = new ArrayList<Equality>();
    }

    /**
     * Gets the relational atoms in the premise of the constraint
     * 
     * @return the relational atoms in the premise of the constraint
     */
    public ArrayList<Atom> getPremiseRel() {
        return premiseRel;
    }

    /**
     * Gets the equalities in the premise of the constraint
     * 
     * @return the equalities in the premise of the constraint
     */
    public ArrayList<Equality> getPremiseEq() {
        return premiseEq;
    }

}
