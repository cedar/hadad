/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriter.legacy;

import java.util.ArrayList;

import hadad.commons.conjunctivequery.Atom;
import hadad.commons.constraints.False;

/**
 * A Denial constraint with explicit equalities
 */
public class DenialWithEq extends ConstraintWithEq {
    /**
     * Constructor
     * 
     * @param denial
     *            The denial without equalities
     */
    public DenialWithEq(final False denial) {
        super();
        Utils.FromBodyToBodyWithEq(denial.getPremise(), premiseRel, premiseEq);
    }

    /**
     * Gets the Deianl's conclusion
     * 
     * @return the TGD's conclusion
     */
    public ArrayList<Atom> getConclusion() {
        return new ArrayList<>();
    }

    @Override
    public String toString() {

        final StringBuilder str = new StringBuilder();
        str.append(Utils.fromAtomsToString(premiseRel));
        str.append(",");
        str.append(Utils.fromEqsToString(premiseEq));
        str.append("->");
        str.append("false");
        return str.toString();
    }
}
