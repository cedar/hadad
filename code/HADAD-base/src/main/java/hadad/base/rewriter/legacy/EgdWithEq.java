/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriter.legacy;

import java.util.ArrayList;

import hadad.commons.constraints.Egd;
import hadad.commons.constraints.Equality;

/**
 * An EGD with explicit equalities
 */
public class EgdWithEq extends ConstraintWithEq {

    private ArrayList<Equality> conclusion;

    /**
     * Constructor
     * 
     * @param egd
     *            The EGD without equalities
     */
    public EgdWithEq(final Egd egd) {
        super();
        Utils.FromBodyToBodyWithEq(egd.getPremise(), premiseRel, premiseEq);
        conclusion = new ArrayList<Equality>();
        for (Equality oldEq : egd.getConclusion()) {
            Equality newEq = Utils.GetTransformedEquality(oldEq, egd.getPremise(), premiseRel);
            conclusion.add(newEq);
        }
    }

    /**
     * Gets the conclusion of the EGD
     * 
     * @return the conclusion of the EGD
     */
    public ArrayList<Equality> getConclusion() {
        return conclusion;
    }

    @Override
    public String toString() {
        return Utils.fromAtomsToString(premiseRel) + "," + Utils.fromEqsToString(premiseEq) + "->"
                + Utils.fromEqsToString(conclusion);
    }

}