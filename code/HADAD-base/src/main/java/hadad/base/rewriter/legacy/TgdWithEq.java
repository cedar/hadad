/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriter.legacy;

import java.util.ArrayList;

import hadad.commons.conjunctivequery.Atom;
import hadad.commons.constraints.Tgd;

/**
 * A TGD with explicit equalities
 */
public class TgdWithEq extends ConstraintWithEq {
    private ArrayList<Atom> conclusion;

    /**
     * Constructor
     * 
     * @param tgd
     *            The TGD without equalities
     */
    public TgdWithEq(Tgd tgd) {
        super();
        Utils.FromBodyToBodyWithEq(tgd.getPremise(), premiseRel, premiseEq);
        conclusion = new ArrayList<Atom>();
        for (Atom oldAtom : tgd.getConclusion()) {
            Atom newAtom = Utils.GetTransformedRelational(oldAtom, tgd.getPremise(), premiseRel);
            conclusion.add(newAtom);
        }
    }

    /**
     * Gets the TGD's conclusion
     * 
     * @return the TGD's conclusion
     */
    public ArrayList<Atom> getConclusion() {
        return conclusion;
    }

    public String toString() {
        return Utils.fromAtomsToString(premiseRel) + "," + Utils.fromEqsToString(premiseEq) + "->"
                + Utils.fromAtomsToString(conclusion);
    }
}
