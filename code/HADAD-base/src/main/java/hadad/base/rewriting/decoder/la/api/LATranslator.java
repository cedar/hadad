/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriting.decoder.la.api;

import static com.google.common.base.Preconditions.checkNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import hadad.base.compiler.Language;
import hadad.base.compiler.exceptions.MetadataException;
import hadad.base.compiler.exceptions.TranslatorException;
import hadad.base.compiler.model.la.metadata.Metadata;
import hadad.base.rewriting.decoder.la.mr.MRTranslator;
import hadad.base.rewriting.decoder.la.nm.NMTranslator;
import hadad.base.rewriting.decoder.la.rm.RMTranslator;
import hadad.base.rewriting.decoder.la.sm.SMTranslator;
import hadad.base.rewriting.decoder.la.tm.TMTranslator;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.ConjunctiveQuery;
import hadad.commons.conjunctivequery.Term;

/**
 * Abstract LA language translator
 * 
 * @author ranaalotaibi
 *
 */
public abstract class LATranslator {
    protected static final Character LParenthesis = '(';
    protected static final Character RParenthesis = ')';

    protected ConjunctiveQuery rewriting;

    protected Map<Term, String> varaibleMapping;
    protected Map<Term, String> baseMaticesMapping;
    protected Map<Term, String> idDecodingMapping;
    protected Map<Term, Atom> idAtomMapping;

    protected List<Atom> sizeMatrices;
    protected String headAfter;
    protected StringBuilder laodMatrices;

    protected Iterator<Map.Entry<Term, Atom>> iterator;

    private static LATranslator instance;

    /**
     * Constructor
     * 
     * @throws TranslatorException
     *             translation exception
     */
    public LATranslator() throws TranslatorException {
        varaibleMapping = new HashMap<>();
        idAtomMapping = new HashMap<>();
        idDecodingMapping = new HashMap<>();
        laodMatrices = new StringBuilder();
        baseMaticesMapping = new HashMap<>();
        sizeMatrices = new ArrayList<>();
        headAfter = null;
        try {
            Metadata.load();
            Metadata.getInstance();
        } catch (MetadataException e) {
            throw new TranslatorException(e);
        }
    }

    public static LATranslator createInstance(final Language language) throws TranslatorException {

        switch (language) {
            case R:
                instance = new RMTranslator();
                return instance;
            case NUMPY:
                instance = new NMTranslator();
                return instance;
            case DML:
                instance = new SMTranslator();
                return instance;
            case TFM:
                instance = new TMTranslator();
                return instance;
            case MR:
                instance = new MRTranslator();
                return instance;
            default:
                throw new TranslatorException("language not supported!");

        }
    }

    /**
     * Set rewriting
     * 
     * @param rewriting
     *            query rewriting
     */
    public void setRewriting(final ConjunctiveQuery rewriting) {
        this.rewriting = checkNotNull(rewriting);
    }

    /**
     * Translate rewriting
     * 
     * @return translated rewriting
     */
    public abstract String translate();

}
