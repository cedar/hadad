/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriting.decoder.la.nm;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import hadad.base.compiler.Language;
import hadad.base.compiler.exceptions.HadadException;
import hadad.base.compiler.exceptions.TranslatorException;
import hadad.base.rewriting.decoder.la.api.LATranslator;
import hadad.commons.conjunctivequery.ConjunctiveQuery;

/** Temp implementation. TODO: to be refactored and generalized **/
public final class NMGen {
    private static final String GEN_PATH = "generated/NM/";
    private static final String TEMP_PATH = "src/main/resources/template/NM/temp1";
    private static final String META_DATA_PATH = "src/main/resources/metadata.json";

    public static void run(final String id, final ConjunctiveQuery rw) throws HadadException {
        NMTranslator mTanslator;
        try {
            mTanslator = (NMTranslator) LATranslator.createInstance(Language.NUMPY);
            mTanslator.setRewriting(rw);
        } catch (TranslatorException e) {
            throw e;
        }

        //TODO:fix this 
        if (!(id.equals("P2.14") || id.equals("P2.20"))) {
            File out = new File(GEN_PATH + id + ".py");
            if (out.exists()) {
                out.delete();
                out = new File(GEN_PATH + id + ".py");
            }

            FileWriter writer;
            try {
                writer = new FileWriter(out);
                writer.write(FileUtils.readFileToString(new File(TEMP_PATH)) + "\n");
                final StringBuilder str = new StringBuilder();
                str.append(mTanslator.translate());
                writer.write(str.toString());
                writer.close();
            } catch (IOException e) {
                throw new HadadException(e);
            }

        }
    }
}
