/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriting.decoder.la.nm;

/**
 * NumPy Operations
 * 
 * @author ranaalotaibi
 */
public enum NMOperation {
    ADD("np.add"),
    ADDS("+"),
    SUM("sum"),
    ROWSUM("sum"),
    COLSUM("sum"),
    MULTI("np.matmul"),
    MULTIS("np.multiply"),
    MULTIE("np.multiply"),
    DIV("np.divide"),
    TRANS("np.transpose"),
    INVERSE("np.linalg.inv"),
    DET("np.linalg.det"),
    TRACE("np.trace"),
    DIAG("np.diag"),
    MEAN("np.mean");

    private final String str;

    private NMOperation(final String str) {
        this.str = str;
    }

    public String OperationValue() {
        return str;
    }

    @Override
    public String toString() {
        return str;
    }
}
