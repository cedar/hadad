/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriting.decoder.la.nm;

import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.exceptions.TranslatorException;
import hadad.base.compiler.model.la.nm.Predicate;
import hadad.base.rewriting.decoder.la.api.LATranslator;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.Term;

/**
 * 
 * This class translates the conjunctive rewriting into NumPy syntax (LA pipelines)
 * 
 * @author ranaalotaibi
 */
public class NMTranslator extends LATranslator {

    private static final Logger LOGGER = Logger.getLogger(NMTranslator.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }

    /**
     * default constructor
     * 
     * @throws TranslatorException
     *             translation exception
     */
    public NMTranslator() throws TranslatorException {
        super();
    }

    /**
     * Translate the given rewriting into DML query
     * 
     * @return the translated rewriting
     */
    public String translate() {
        preProcess();
        do {
            for (iterator = idAtomMapping.entrySet().iterator(); iterator.hasNext();) {
                final Map.Entry<Term, Atom> entry = iterator.next();
                switch (entry.getValue().getPredicate()) {
                    case "tr":
                        processTranspose(entry.getValue());
                        break;
                    case "in":
                        processInverse(entry.getValue());
                        break;
                    case "ins":
                        processInverseScalar(entry.getValue());
                        break;
                    case "trace":
                        processTrace(entry.getValue());
                        break;
                    case "sum":
                        processSum(entry.getValue());
                        break;
                    case "diag":
                        processDiag(entry.getValue());
                        break;
                    case "add":
                        processAdd(entry.getValue());
                        break;
                    case "multi":
                        processMulti(entry.getValue());
                        break;
                    case "det":
                        processDet(entry.getValue());
                        break;
                    case "add_s":
                        processAdds(entry.getValue());
                        break;
                    case "multi_s":
                        processMultis(entry.getValue());
                        break;
                    case "identityPrune":
                        procesPruneOut(entry.getValue());
                        break;
                    case "rowSums":
                        processRowSums(entry.getValue());
                        break;
                    case "colSums":
                        processColSums(entry.getValue());
                        break;
                    default:
                        break;
                }
            }

        } while (idAtomMapping.size() != 0);
        final Term head = rewriting.getHead().get(0);
        final StringBuilder str = new StringBuilder();
        processNameRelation();
        if (laodMatrices.length() != 0) {
            str.append(laodMatrices);
        }
        if (headAfter == null)
            str.append(rewriting.getName());
        else {
            str.append(headAfter);
        }

        str.append("=");
        if (idDecodingMapping.isEmpty()) {
            str.append(head);
        } else {
            str.append(idDecodingMapping.get(head));
        }
        varaibleMapping.clear();
        idAtomMapping.clear();
        idDecodingMapping.clear();
        laodMatrices.setLength(0);
        baseMaticesMapping.clear();
        sizeMatrices.clear();
        headAfter = null;
        return str.toString();
    }

    /**
     * Pre-process the rewriting
     */
    private void preProcess() {
        for (final Atom atom : rewriting.getBody()) {
            final String atomPredicte = atom.getPredicate();
            if (atomPredicte.equals(Predicate.SIZE.toString())) {
                sizeMatrices.add(atom);
            } else {
                if (atomPredicte.equals(Predicate.NAME.toString())) {
                    varaibleMapping.put(atom.getTerm(0), atom.getTerm(0).toString());
                    baseMaticesMapping.put(atom.getTerm(0), atom.getTerm(1).toString());
                } else {
                    if (atom.getTerms().size() == 2) {
                        idAtomMapping.put(atom.getTerm(0), atom);
                    }
                    if (atom.getTerms().size() == 3) {
                        idAtomMapping.put(atom.getTerm(0), atom);
                        idAtomMapping.put(atom.getTerm(1), atom);
                    }
                }
            }
        }
    }

    /**
     * Process transpose operation
     * 
     * @param atom
     *            transpose atom
     */
    private void processTranspose(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(NMOperation.TRANS);
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;

                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(NMOperation.TRANS);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(idDecodingMapping.get(term));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process determinaint operation
     * 
     * @param atom
     *            determinaint atom
     */
    private void processDet(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingDet = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingDet.append(LParenthesis);
            decodingDet.append(NMOperation.DET);
            decodingDet.append(LParenthesis);
            decodingDet.append(varaibleMapping.get(term));
            decodingDet.append(RParenthesis);
            decodingDet.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingDet.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;

                decodingDet.append(LParenthesis);
                decodingDet.append(NMOperation.DET);
                decodingDet.append(LParenthesis);
                decodingDet.append(idDecodingMapping.get(term));
                decodingDet.append(RParenthesis);
                decodingDet.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingDet.toString());
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process inverse operation
     * 
     * @param atom
     *            inverse atom
     */
    private void processInverse(final Atom atom) {
        if (atom.toString().equals("in(c_7,b_3)") || atom.toString().equals("in(b_3,c_7)")) { // TODO:to be fixed
            idDecodingMapping.put(atom.getTerm(1), "");
            iterator.remove();
        } else {
            final Term term = atom.getTerm(0);
            boolean status = false;
            final StringBuilder decodingInverse = new StringBuilder();
            if (varaibleMapping.containsKey(term)) {
                status = true;
                decodingInverse.append(LParenthesis);
                decodingInverse.append(NMOperation.INVERSE);
                decodingInverse.append(LParenthesis);
                decodingInverse.append(varaibleMapping.get(term));
                decodingInverse.append(RParenthesis);
                decodingInverse.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingInverse.toString());
            } else {
                if (idDecodingMapping.containsKey(term)) {
                    status = true;
                    decodingInverse.append(LParenthesis);
                    decodingInverse.append(NMOperation.INVERSE);
                    decodingInverse.append(LParenthesis);
                    decodingInverse.append(idDecodingMapping.get(term));
                    decodingInverse.append(RParenthesis);
                    decodingInverse.append(RParenthesis);
                    idDecodingMapping.put(atom.getTerm(1), decodingInverse.toString());
                }
            }
            if (status) {
                iterator.remove();
            }
        }
    }

    /**
     * Process inverse scalar
     * 
     * @param atom
     *            inverse scalar atom
     */
    private void processInverseScalar(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingInverse = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingInverse.append(LParenthesis);
            decodingInverse.append("1/");
            decodingInverse.append(LParenthesis);
            decodingInverse.append(varaibleMapping.get(term));
            decodingInverse.append(RParenthesis);
            decodingInverse.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingInverse.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;
                decodingInverse.append(LParenthesis);
                decodingInverse.append("1/");
                decodingInverse.append(LParenthesis);
                decodingInverse.append(idDecodingMapping.get(term));
                decodingInverse.append(RParenthesis);
                decodingInverse.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingInverse.toString());
            }
        }
        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process name relation
     * 
     * @param atom
     *            size atom
     */
    private void processNameRelation() {

        for (Map.Entry<Term, String> entry : baseMaticesMapping.entrySet()) {
            final StringBuilder decodingSize = new StringBuilder();
            decodingSize.append("genfromtxt");
            decodingSize.append(LParenthesis);
            decodingSize.append(baseMaticesMapping.get(entry.getKey()));
            decodingSize.append(", delimiter=','");
            decodingSize.append(RParenthesis);
            laodMatrices.append(entry.getKey());
            laodMatrices.append("=");
            laodMatrices.append(decodingSize);
            laodMatrices.append("\n");
        }
    }

    /**
     * Process trace operation
     * 
     * @param atom
     *            trace atom
     */
    private void processTrace(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(NMOperation.TRACE);
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;

                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(NMOperation.TRACE);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(idDecodingMapping.get(term));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
            }
        }

        if (status) {
            iterator.remove();

        }
    }

    /**
     * Process sum operation
     * 
     * @param atom
     *            sum atom
     */
    private void processSum(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(NMOperation.SUM);
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;

                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(NMOperation.SUM);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(idDecodingMapping.get(term));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
            }
        }

        if (status) {
            iterator.remove();

        }
    }

    /**
     * Process procesPruneOut
     * 
     * @param atom
     *            tprocesPruneOut
     */
    private void procesPruneOut(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        if (varaibleMapping.containsKey(term)) {

            idDecodingMapping.put(atom.getTerm(1), atom.getTerm(0).toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;
                idDecodingMapping.put(atom.getTerm(1), atom.getTerm(0).toString());
            }
        }

        if (status) {
            iterator.remove();

        }
    }

    /**
     * Process diag operation
     * 
     * @param atom
     *            diag atom
     */
    private void processDiag(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(NMOperation.DIAG);
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;

                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(NMOperation.DIAG);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(idDecodingMapping.get(term));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process add operation
     * 
     * @param atom
     *            add atom
     */
    private void processAdd(final Atom atom) {
        final Term term1 = atom.getTerm(0);
        final Term term2 = atom.getTerm(1);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(NMOperation.ADD);
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term1));
            decodingTranspsoe.append(",");
            decodingTranspsoe.append(varaibleMapping.get(term2));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
        } else {
            if (varaibleMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                status = true;
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(NMOperation.ADD);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(varaibleMapping.get(term1));
                decodingTranspsoe.append(",");
                decodingTranspsoe.append(idDecodingMapping.get(term2));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
            } else {
                if (idDecodingMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
                    status = true;
                    decodingTranspsoe.append(LParenthesis);
                    decodingTranspsoe.append(NMOperation.ADD);
                    decodingTranspsoe.append(LParenthesis);
                    decodingTranspsoe.append(idDecodingMapping.get(term1));
                    decodingTranspsoe.append(",");
                    decodingTranspsoe.append(varaibleMapping.get(term2));
                    decodingTranspsoe.append(RParenthesis);
                    decodingTranspsoe.append(RParenthesis);
                    idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                } else {
                    if (idDecodingMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                        status = true;
                        decodingTranspsoe.append(LParenthesis);
                        decodingTranspsoe.append(NMOperation.ADD);
                        decodingTranspsoe.append(LParenthesis);
                        decodingTranspsoe.append(idDecodingMapping.get(term1));
                        decodingTranspsoe.append(",");
                        decodingTranspsoe.append(idDecodingMapping.get(term2));
                        decodingTranspsoe.append(RParenthesis);
                        decodingTranspsoe.append(RParenthesis);
                        idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                    }
                }
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process add_s operation
     * 
     * @param atom
     *            add atom
     */
    private void processAdds(final Atom atom) {
        final Term term1 = atom.getTerm(0);
        final Term term2 = atom.getTerm(1);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term1));
            decodingTranspsoe.append(NMOperation.ADDS);
            decodingTranspsoe.append(varaibleMapping.get(term2));
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
        } else {
            if (varaibleMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                status = true;
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(varaibleMapping.get(term1));
                decodingTranspsoe.append(NMOperation.ADDS);
                decodingTranspsoe.append(idDecodingMapping.get(term2));
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
            } else {
                if (idDecodingMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
                    status = true;
                    decodingTranspsoe.append(LParenthesis);
                    decodingTranspsoe.append(idDecodingMapping.get(term1));
                    decodingTranspsoe.append(NMOperation.ADDS);
                    decodingTranspsoe.append(varaibleMapping.get(term2));
                    decodingTranspsoe.append(RParenthesis);
                    idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                } else {
                    if (idDecodingMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                        status = true;
                        decodingTranspsoe.append(LParenthesis);
                        decodingTranspsoe.append(idDecodingMapping.get(term1));
                        decodingTranspsoe.append(NMOperation.ADDS);
                        decodingTranspsoe.append(idDecodingMapping.get(term2));
                        decodingTranspsoe.append(RParenthesis);
                        idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                    }
                }
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process multi_s operation
     * 
     * @param atom
     *            add atom
     */
    private void processMultis(final Atom atom) {
        final Term term1 = atom.getTerm(0);
        final Term term2 = atom.getTerm(1);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term1));
            decodingTranspsoe.append(NMOperation.MULTIS);
            decodingTranspsoe.append(varaibleMapping.get(term2));
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
        } else {
            if (varaibleMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                status = true;
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(varaibleMapping.get(term1));
                decodingTranspsoe.append(NMOperation.MULTIS);
                decodingTranspsoe.append(idDecodingMapping.get(term2));
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
            } else {
                if (idDecodingMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
                    status = true;
                    decodingTranspsoe.append(LParenthesis);
                    decodingTranspsoe.append(idDecodingMapping.get(term1));
                    decodingTranspsoe.append(NMOperation.MULTIS);
                    decodingTranspsoe.append(varaibleMapping.get(term2));
                    decodingTranspsoe.append(RParenthesis);
                    idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                } else {
                    if (idDecodingMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                        status = true;
                        decodingTranspsoe.append(LParenthesis);
                        decodingTranspsoe.append(idDecodingMapping.get(term1));
                        decodingTranspsoe.append(NMOperation.MULTIS);
                        decodingTranspsoe.append(idDecodingMapping.get(term2));
                        decodingTranspsoe.append(RParenthesis);
                        idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                    }
                }
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process add operation
     * 
     * @param atom
     *            add atom
     */
    /**
     * Process add operation
     * 
     * @param atom
     *            add atom
     */
    private void processMulti(final Atom atom) {
        final Term term1 = atom.getTerm(0);
        final Term term2 = atom.getTerm(1);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(NMOperation.MULTI);
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term1));
            decodingTranspsoe.append(",");
            decodingTranspsoe.append(varaibleMapping.get(term2));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
        } else {
            if (varaibleMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                status = true;
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(NMOperation.MULTI);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(varaibleMapping.get(term1));
                decodingTranspsoe.append(",");
                decodingTranspsoe.append(idDecodingMapping.get(term2));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
            } else {
                if (idDecodingMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
                    status = true;
                    decodingTranspsoe.append(LParenthesis);
                    decodingTranspsoe.append(NMOperation.MULTI);
                    decodingTranspsoe.append(LParenthesis);
                    decodingTranspsoe.append(idDecodingMapping.get(term1));
                    decodingTranspsoe.append(",");
                    decodingTranspsoe.append(varaibleMapping.get(term2));
                    decodingTranspsoe.append(RParenthesis);
                    decodingTranspsoe.append(RParenthesis);
                    idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                } else {
                    if (idDecodingMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                        status = true;
                        decodingTranspsoe.append(LParenthesis);
                        decodingTranspsoe.append(NMOperation.MULTI);
                        decodingTranspsoe.append(LParenthesis);
                        decodingTranspsoe.append(idDecodingMapping.get(term1));
                        decodingTranspsoe.append(",");
                        decodingTranspsoe.append(idDecodingMapping.get(term2));
                        decodingTranspsoe.append(RParenthesis);
                        decodingTranspsoe.append(RParenthesis);
                        idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                    }
                }
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process rowsums operation
     * 
     * @param atom
     *            rowSumss atom
     */
    private void processRowSums(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(NMOperation.ROWSUM);
            decodingTranspsoe.append(", axies=1");
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;

                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(NMOperation.ROWSUM);
                decodingTranspsoe.append(", axies=1");
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(idDecodingMapping.get(term));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process colsums operation
     * 
     * @param atom
     *            colsums atom
     */
    private void processColSums(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(NMOperation.COLSUM);
            decodingTranspsoe.append(", axis=0");
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;

                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(NMOperation.COLSUM);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(idDecodingMapping.get(term));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
            }
        }

        if (status) {
            iterator.remove();
        }
    }
}