/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriting.decoder.la.rm;

/**
 * R Operations
 * 
 * @author ranaalotaibi
 */
public enum RMOperation {
    ADD("+"),
    SUB("-"),
    MULTI("%*%"),
    MULTIS("*"),
    MULTIE("*"),
    DIV("%/%"),
    DIVS("/"),
    TRANS("t"),
    INVERSE("solve"),
    DET("det"),
    ADJ("adj"),
    COFA("cof"),
    TRACE("trace"),
    DIAG("diag"),
    POW("^"),
    AVG("avg"),
    MEAN("mean"),
    VAR("var"),
    SD("sd"),
    SUM("sum"),
    ADDS("+"),
    COLSUM("colSums"),
    COLMEAN("colMeans"),
    COLVARS("colVars"),
    COLSDS("colSds"),
    COLMAX("colMaxs"),
    COLMINS("colMins"),
    ROWSUM("rowSums"),
    ROWMEAN("rowMeans"),
    ROWVARS("rowVars"),
    ROWSDS("rowSds"),
    ROWMAX("rowMaxs"),
    ROWMIN("rowMins"),
    CUMSUM("cumsum"),
    CUMPROD("cumprod"),
    CUMMIN("cummin"),
    CUMMAX("cummax"),
    EQUALS("eq");

    private final String str;

    private RMOperation(final String str) {
        this.str = str;
    }

    public String OperationValue() {
        return str;
    }

    @Override
    public String toString() {
        return str;
    }
}
