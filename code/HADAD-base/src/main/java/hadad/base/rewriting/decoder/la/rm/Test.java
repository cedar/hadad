/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriting.decoder.la.rm;

import java.io.File;

import hadad.base.compiler.Language;
import hadad.base.compiler.Utils;
import hadad.base.rewriting.decoder.la.api.LATranslator;
import hadad.commons.conjunctivequery.ConjunctiveQuery;

/** Temporary Main Test **/
public class Test {
    private static final File INPUT_QUERY_FILE = new File("src/main/resources/testRM/decoderTest");

    public static void main(String[] args) throws Exception {
        final ConjunctiveQuery rewriting = Utils.parseQuery((INPUT_QUERY_FILE));
        RMTranslator rmTanslator = (RMTranslator) LATranslator.createInstance(Language.R);
        rmTanslator.setRewriting(rewriting);
        System.out.print(rmTanslator.translate());
    }

}
