/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriting.decoder.la.sm;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

import hadad.base.compiler.Language;
import hadad.base.compiler.exceptions.HadadException;
import hadad.base.compiler.exceptions.TranslatorException;
import hadad.base.rewriting.decoder.la.api.LATranslator;
import hadad.commons.conjunctivequery.ConjunctiveQuery;

/** Temp implementation. TODO: to be refactored and generalized **/
public final class SMGen {
    private static final String GEN_PATH = "generated/SM/";
    private static final String TEMP_PATH = "src/main/resources/template/SM/temp1";
    private static final String DML_QUERY = "val dmlTextQuery =";

    public static void run(final String id, final ConjunctiveQuery rw) throws HadadException {
        SMTranslator smTanslator;
        try {
            smTanslator = (SMTranslator) LATranslator.createInstance(Language.DML);
            smTanslator.setRewriting(rw);
        } catch (TranslatorException e) {
            throw e;
        }

        //TODO:fix this 
        if (!(id.equals("P2.14") || id.equals("P2.20"))) {
            File out = new File(GEN_PATH + id + ".scala");
            if (out.exists()) {
                out.delete();
                out = new File(GEN_PATH + id + ".scala");
            }
            try {
                FileWriter writer = new FileWriter(out);
                writer.write(FileUtils.readFileToString(new File(TEMP_PATH)) + "\n");
                final StringBuilder str = new StringBuilder();
                str.append(DML_QUERY);
                str.append("\"\"\"");
                str.append(smTanslator.translate());
                str.append("\"\"\"");
                str.append("\n");
                str.append("val scriptQ=");
                str.append("dml(dmlTextQuery)");
                str.append("\n");
                str.append("val resQ = ml.execute(scriptQ)");
                writer.write(str.toString());
                writer.close();
            } catch (IOException e) {
                throw new HadadException(e);
            }
        }
    }
}
