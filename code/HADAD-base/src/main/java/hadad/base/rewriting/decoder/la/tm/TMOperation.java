/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriting.decoder.la.tm;

/**
 * TensorFlow Operations
 * 
 * @author ranaalotaibi
 *
 */
public enum TMOperation {
    ADD("tf.math.add"),
    MULTI("tf.linalg.matmul"),
    MULTIS("tf.math.scalar_mul"),
    MULTIE("tf.math.matmul"),
    DIV("tf.math.divide"),
    TRANS("tf.linalg.matrix_transpose"),
    INVERSE("tf.linalg.inv"),
    DET("tf.linalg.det"),
    ADJ("tf.linalg.adjoint"),
    TRACE("tf.linalg.trace"),
    DIAG("tf.linalg.diag"),
    MEAN("tf.reduce_mean");

    private final String str;

    private TMOperation(final String str) {
        this.str = str;
    }

    public String OperationValue() {
        return str;
    }

    @Override
    public String toString() {
        return str;
    }
}
