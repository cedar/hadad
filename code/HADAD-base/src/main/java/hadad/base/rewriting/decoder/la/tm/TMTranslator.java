/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.rewriting.decoder.la.tm;

import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.exceptions.TranslatorException;
import hadad.base.compiler.model.la.tm.Predicate;
import hadad.base.rewriting.decoder.la.api.LATranslator;
import hadad.base.rewriting.decoder.la.sm.SMTranslator;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.Term;

/**
 * 
 * This class translates the conjunctive rewriting into DML query syntax
 * 
 * 
 *
 */
public class TMTranslator extends LATranslator {

    private static final Logger LOGGER = Logger.getLogger(SMTranslator.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }

    /**
     * default constructor
     * 
     * @throws TranslatorException
     *             translation exception
     */
    public TMTranslator() throws TranslatorException {
        super();
    }

    /**
     * Translate the given rewriting into DML query
     * 
     * @return the translated rewriting
     */
    public String translate() {
        preProcess();
        do {
            for (iterator = idAtomMapping.entrySet().iterator(); iterator.hasNext();) {
                final Map.Entry<Term, Atom> entry = iterator.next();
                switch (entry.getValue().getPredicate()) {
                    case "tr":
                        processTranspose(entry.getValue());
                        break;
                    case "trace":
                        processTrace(entry.getValue());
                        break;
                    case "diag":
                        processDiag(entry.getValue());
                        break;
                    case "add":
                        processAdd(entry.getValue());
                        break;
                    case "multi":
                        processMulti(entry.getValue());
                        break;
                    default:
                        break;
                }
            }

        } while (idAtomMapping.size() != 0);
        final Term head = rewriting.getHead().get(0);
        return rewriting.getName() + "=" + idDecodingMapping.get(head);
    }

    /**
     * Pre-process the rewriting
     */
    private void preProcess() {
        for (final Atom atom : rewriting.getBody()) {
            final String atomPredicte = atom.getPredicate();
            if (atomPredicte.equals(Predicate.NAME.toString())) {
                varaibleMapping.put(atom.getTerm(0), atom.getTerm(1).toString().replace("\"", ""));
            } else {
                if (atom.getTerms().size() == 2) {
                    idAtomMapping.put(atom.getTerm(0), atom);
                }
                if (atom.getTerms().size() == 3) {
                    idAtomMapping.put(atom.getTerm(0), atom);
                    idAtomMapping.put(atom.getTerm(1), atom);
                }
            }
        }
    }

    /**
     * Process transpose operation
     * 
     * @param atom
     *            transpose atom
     */
    private void processTranspose(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(TMOperation.TRANS);
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;

                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(TMOperation.TRANS);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(idDecodingMapping.get(term));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process trace operation
     * 
     * @param atom
     *            trace atom
     */
    private void processTrace(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(TMOperation.TRACE);
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;

                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(TMOperation.TRACE);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(idDecodingMapping.get(term));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process diag operation
     * 
     * @param atom
     *            diag atom
     */
    private void processDiag(final Atom atom) {
        final Term term = atom.getTerm(0);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(TMOperation.DIAG);
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
        } else {
            if (idDecodingMapping.containsKey(term)) {
                status = true;

                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(TMOperation.DIAG);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(idDecodingMapping.get(term));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(1), decodingTranspsoe.toString());
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process add operation
     * 
     * @param atom
     *            add atom
     */
    private void processAdd(final Atom atom) {
        final Term term1 = atom.getTerm(0);
        final Term term2 = atom.getTerm(1);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(TMOperation.ADD);
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term1));
            decodingTranspsoe.append(",");
            decodingTranspsoe.append(varaibleMapping.get(term2));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
        } else {
            if (varaibleMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                status = true;
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(TMOperation.ADD);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(varaibleMapping.get(term1));
                decodingTranspsoe.append(",");
                decodingTranspsoe.append(idDecodingMapping.get(term2));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
            } else {
                if (idDecodingMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
                    status = true;
                    decodingTranspsoe.append(LParenthesis);
                    decodingTranspsoe.append(TMOperation.ADD);
                    decodingTranspsoe.append(LParenthesis);
                    decodingTranspsoe.append(idDecodingMapping.get(term1));
                    decodingTranspsoe.append(",");
                    decodingTranspsoe.append(varaibleMapping.get(term2));
                    decodingTranspsoe.append(RParenthesis);
                    decodingTranspsoe.append(RParenthesis);
                    idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                } else {
                    if (idDecodingMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                        status = true;
                        decodingTranspsoe.append(LParenthesis);
                        decodingTranspsoe.append(TMOperation.ADD);
                        decodingTranspsoe.append(LParenthesis);
                        decodingTranspsoe.append(idDecodingMapping.get(term1));
                        decodingTranspsoe.append(",");
                        decodingTranspsoe.append(idDecodingMapping.get(term2));
                        decodingTranspsoe.append(RParenthesis);
                        decodingTranspsoe.append(RParenthesis);
                        idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                    }
                }
            }
        }

        if (status) {
            iterator.remove();
        }
    }

    /**
     * Process add operation
     * 
     * @param atom
     *            add atom
     */
    private void processMulti(final Atom atom) {
        final Term term1 = atom.getTerm(0);
        final Term term2 = atom.getTerm(1);
        boolean status = false;
        final StringBuilder decodingTranspsoe = new StringBuilder();
        if (varaibleMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
            status = true;
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(TMOperation.ADD);
            decodingTranspsoe.append(LParenthesis);
            decodingTranspsoe.append(varaibleMapping.get(term1));
            decodingTranspsoe.append(",");
            decodingTranspsoe.append(varaibleMapping.get(term2));
            decodingTranspsoe.append(RParenthesis);
            decodingTranspsoe.append(RParenthesis);
            idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
        } else {
            if (varaibleMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                status = true;
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(TMOperation.MULTI);
                decodingTranspsoe.append(LParenthesis);
                decodingTranspsoe.append(varaibleMapping.get(term1));
                decodingTranspsoe.append(",");
                decodingTranspsoe.append(idDecodingMapping.get(term2));
                decodingTranspsoe.append(RParenthesis);
                decodingTranspsoe.append(RParenthesis);
                idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
            } else {
                if (idDecodingMapping.containsKey(term1) && varaibleMapping.containsKey(term2)) {
                    status = true;
                    decodingTranspsoe.append(LParenthesis);
                    decodingTranspsoe.append(TMOperation.MULTI);
                    decodingTranspsoe.append(LParenthesis);
                    decodingTranspsoe.append(idDecodingMapping.get(term1));
                    decodingTranspsoe.append(",");
                    decodingTranspsoe.append(varaibleMapping.get(term2));
                    decodingTranspsoe.append(RParenthesis);
                    decodingTranspsoe.append(RParenthesis);
                    idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                } else {
                    if (idDecodingMapping.containsKey(term1) && idDecodingMapping.containsKey(term2)) {
                        status = true;
                        decodingTranspsoe.append(LParenthesis);
                        decodingTranspsoe.append(TMOperation.MULTI);
                        decodingTranspsoe.append(LParenthesis);
                        decodingTranspsoe.append(idDecodingMapping.get(term1));
                        decodingTranspsoe.append(",");
                        decodingTranspsoe.append(idDecodingMapping.get(term2));
                        decodingTranspsoe.append(RParenthesis);
                        decodingTranspsoe.append(RParenthesis);
                        idDecodingMapping.put(atom.getTerm(2), decodingTranspsoe.toString());
                    }
                }
            }
        }

        if (status) {
            iterator.remove();
        }
    }

}
