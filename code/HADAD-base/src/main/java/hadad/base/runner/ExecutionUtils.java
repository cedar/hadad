/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu

"License");
   * you may not use this file exce
  * Licensed under the Apache License, Version 2.0 (thept in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.runner;

import java.util.List;
import java.util.stream.Collectors;

import hadad.base.rewriter.TimedReformulations;
import hadad.commons.conjunctivequery.ConjunctiveQuery;

/**
 * ExecutionUtils
 * 
 * @author ranaalotaibi
 *
 */
public final class ExecutionUtils {
    private final List<TimedReformulations> timedReformulations;

    public ExecutionUtils(final List<TimedReformulations> timedReformulations) {
        this.timedReformulations = timedReformulations;
    }

    public List<Long> getQueryAssertionTimes() {
        return timedReformulations.stream().map(TimedReformulations::getQueryAssertionTime)
                .collect(Collectors.toList());
    }

    public double getQueryAssertionTimesAverage() {
        return timedReformulations.stream().mapToLong(TimedReformulations::getQueryAssertionTime).average()
                .getAsDouble();
    }

    public List<Long> getChaseExecTimes() {
        return timedReformulations.stream().map(TimedReformulations::getChaseExecTime).collect(Collectors.toList());
    }

    public double getChaseExecTimesAverage() {
        return timedReformulations.stream().mapToLong(TimedReformulations::getChaseExecTime).average().getAsDouble();
    }

    public List<Long> getRestrictExecTimes() {
        return timedReformulations.stream().map(TimedReformulations::getRestrictExecTime).collect(Collectors.toList());
    }

    public double getRestrictExecTimesAverage() {
        return timedReformulations.stream().mapToLong(TimedReformulations::getRestrictExecTime).average().getAsDouble();
    }

    public List<Long> getBackchaseExecTimes() {
        return timedReformulations.stream().map(TimedReformulations::getBackchaseExecTime).collect(Collectors.toList());
    }

    public double getBackchaseExecTimesAverage() {
        return timedReformulations.stream().mapToLong(TimedReformulations::getBackchaseExecTime).average()
                .getAsDouble();
    }

    public double getExecTimesAverage() {
        return nano2ms(getQueryAssertionTimesAverage() + getChaseExecTimesAverage() + getRestrictExecTimesAverage()
                + getBackchaseExecTimesAverage());
    }

    public List<ConjunctiveQuery> getRewritings() {
        if (timedReformulations.isEmpty()) {
            return null;
        } else {
            return timedReformulations.get(0).getRewritings();
        }
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        builder.append("Query rewriting average time (ms): ").append(getExecTimesAverage()).append("\n");
        builder.append("Query assertion generation average time (ms): ")
                .append(nano2ms(getQueryAssertionTimesAverage())).append(" (").append(getQueryAssertionTimes().stream()
                        .map(ExecutionUtils::nano2ms).collect(Collectors.toList()).toString())
                .append(")").append("\n");
        builder.append("Query chase execution average time (ms): ").append(nano2ms(getChaseExecTimesAverage()))
                .append(" (").append(getChaseExecTimes().stream().map(ExecutionUtils::nano2ms)
                        .collect(Collectors.toList()).toString())
                .append(")").append("\n");
        builder.append("Query restrict execution average time (ms): ").append(nano2ms(getRestrictExecTimesAverage()))
                .append(" (").append(getRestrictExecTimes().stream().map(ExecutionUtils::nano2ms)
                        .collect(Collectors.toList()).toString())
                .append(")").append("\n");
        builder.append("Query backchase execution average time (ms): ").append(nano2ms(getBackchaseExecTimesAverage()))
                .append(" (").append(getBackchaseExecTimes().stream().map(ExecutionUtils::nano2ms)
                        .collect(Collectors.toList()).toString())
                .append(")").append("\n");
        builder.append("Rewritings: ").append(getRewritings().toString()).append("\n");
        return builder.toString();
    }

    @Override
    public int hashCode() {
        return timedReformulations.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof ExecutionUtils) && timedReformulations.equals(((ExecutionUtils) o).timedReformulations);
    }

    private static double nano2ms(double nano) {
        return nano / 1000 / 1000;
    }

    private static long nano2ms(long nano) {
        return nano / 1000 / 1000;
    }
}
