/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.runner;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import hadad.base.compiler.Language;
import hadad.base.compiler.exceptions.HadadException;
import hadad.base.compiler.exceptions.MetadataException;
import hadad.base.compiler.model.la.metadata.Metadata;
import hadad.base.compiler.model.la.mr.MRRunnerWrapper;

/**
 * HadadRunner
 * 
 * @author ranaalotaibi
 *
 */
@Singleton
public class HadadRunner {
    private static final Logger LOGGER = Logger.getLogger(HadadRunner.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    private static final String RESULT_NOVIEWS_NAIVE = "../../figures/LAPipe/Naive/results/resultsNaiveNoViewsRWTime";
    private static final String RESULT_NOVIEWS_NAIVE_RWS = "../../figures/LAPipe/Naive/results/resultsNaiveNoViewsRWS";

    private static final String RESULT_NOVIEWS_MNC = "../../figures/LAPipe/MNC/results/resultsNaiveNoViewsRWTime";
    private static final String RESULT_NOVIEWS_MNC_RWS = "../../figures/LAPipe/MNC/results/resultsNaiveNoViewsRWS";

    private static final String RESULT_VIEWS_NAIVE = "../../figures/LAViews/Naive/results/resultsNaiveViewsRWTime";
    private static final String RESULT_VIEWS_NAIVE_RWS = "../../figures/LAViews/Naive/results/resultsNaiveViewsRWS";

    private static final String RESULT_HYBRID_NAIVE_MR = "../../figures/Hybrid/Morpheus/results/resultsNaiveRWTime";
    private static final String RESULT_HYBRID_NAIVE_MR_RWS = "../../figures/Hybrid/Morpheus/results/resultsNaiveRW";

    private static final String RESULT_HYBRID_NAIVE_SP_TWITTER =
            "../../figures/Hybrid/Twitter/results/resultsNaiveRWTime";
    private static final String RESULT_HYBRID_NAIVE_SP_MIMIC = "../../figures/Hybrid/MIMIC/results/resultsNaiveRWTime";

    private final String metadataPath;

    private final String directoryNoViews;
    private final String directoryViews;
    private final String directoryHybridMR;
    private final String directoryHybridSPTwitter;
    private final String directoryHybridSPMIMIC;

    private BufferedWriter writerNoViewsNaive;
    private BufferedWriter writerNoViewsNaiveRWs;

    private BufferedWriter writerNoViewsMNC;
    private BufferedWriter writerNoViewsMNCRWs;

    private BufferedWriter writeViewsNaive;
    private BufferedWriter writeViewsNaiveRWS;

    private BufferedWriter writHybridNaiveMR;
    private BufferedWriter writHybridNaiveRWSMR;

    private BufferedWriter writHybridNaiveSR;

    private List<Language> LALanguages;

    @Inject
    private HadadRunner(@Named("cases.folder.noviews") final String directoryNoViews,
            @Named("cases.folder.views") final String directoryViews,
            @Named("cases.folder.hybrid.MR") final String directoryHybridMR,
            @Named("cases.folder.hybrid.SP.Twitter") final String directoryHybridSPTwitter,
            @Named("cases.folder.hybrid.SP.MIMIC") final String directoryHybridSPMIMIC,
            @Named("metadata.file.path") final String metadataPath) {
        this.directoryNoViews = checkNotNull(directoryNoViews);
        this.directoryViews = checkNotNull(directoryViews);
        this.directoryHybridMR = checkNotNull(directoryHybridMR);
        this.directoryHybridSPTwitter = checkNotNull(directoryHybridSPTwitter);
        this.directoryHybridSPMIMIC = checkNotNull(directoryHybridSPMIMIC);
        this.metadataPath = checkNotNull(metadataPath);
        LALanguages = new ArrayList<>();
        LALanguages.add(Language.R);
        LALanguages.add(Language.NUMPY);
        LALanguages.add(Language.DML);
        LALanguages.add(Language.TFM);

    }

    /**
     * Run all configuration
     * 
     * @throws HadadException
     *             HADAD exception
     */
    public void run() throws HadadException {
        runNoViewsNaive();
        runNoViewsMNC();
        runViewsNaive();
        runHybridMR();
    }

    /**
     * Run no-views pipeline (naive)
     * 
     * @throws HadadException
     */
    private void runNoViewsNaive() throws HadadException {
        try {
            writerNoViewsNaive = new BufferedWriter(new FileWriter(RESULT_NOVIEWS_NAIVE, true));
            writerNoViewsNaiveRWs = new BufferedWriter(new FileWriter(RESULT_NOVIEWS_NAIVE_RWS, true));

        } catch (IOException e) {
            final String message =
                    String.format("Couldn't find %s or %s file ", RESULT_NOVIEWS_NAIVE, RESULT_NOVIEWS_NAIVE_RWS);
            throw new HadadException(message);
        }
        initializeMeta();
        final File directory = new File(directoryNoViews);
        File[] directoryListing = directory.listFiles();
        if (directoryListing != null) {
            for (final File folder : directoryListing) {
                if (folder.getName().equals("P2.25") || folder.getName().equals("P2.27")
                        || folder.getName().equals("P2.21")) {
                    continue;
                }

                final String folderName = folder.getName();

                for (Language language : LALanguages) {
                    System.out.println("\nLang:" + language.toString());
                    System.out.println("TestFolder:" + folderName);
                    final RunnerWrapper runnerWrapper =
                            RunnerWrapper.createInstance(folder.getAbsolutePath(), 1, language);
                    runnerWrapper.run(1);

                    try {
                        writerNoViewsNaive
                                .write(language.toString() + "," + folderName + "," + runnerWrapper.getTime() + "\n");
                        writerNoViewsNaiveRWs
                                .write(language.toString() + "," + folderName + " : " + runnerWrapper.getRW() + "\n");
                        System.out.print(runnerWrapper.getRW());

                    } catch (IOException e) {
                        final String message = String.format("Couldn't write on %s or %s file", RESULT_NOVIEWS_NAIVE,
                                RESULT_NOVIEWS_NAIVE_RWS);
                        throw new HadadException(message);
                    }
                }

            }
        }
        try {
            writerNoViewsNaive.close();
            writerNoViewsNaiveRWs.close();
        } catch (IOException e) {
            final String message =
                    String.format("Couldn't close  %s or %s file", RESULT_NOVIEWS_NAIVE, RESULT_NOVIEWS_NAIVE_RWS);
            throw new HadadException(message);
        }

    }

    /**
     * Run no-views pipeline (MNC)
     * 
     * @throws HadadException
     */
    private void runNoViewsMNC() throws HadadException {
        try {
            writerNoViewsMNC = new BufferedWriter(new FileWriter(RESULT_NOVIEWS_MNC, true));
            writerNoViewsMNCRWs = new BufferedWriter(new FileWriter(RESULT_NOVIEWS_MNC_RWS, true));

        } catch (IOException e) {
            final String message =
                    String.format("Couldn't find %s or %s file ", RESULT_NOVIEWS_MNC, RESULT_NOVIEWS_MNC_RWS);
            throw new HadadException(message);
        }
        initializeMeta();
        final File directory = new File(directoryNoViews);
        File[] directoryListing = directory.listFiles();
        if (directoryListing != null) {
            for (final File folder : directoryListing) {
                if (folder.getName().equals("P2.25") || folder.getName().equals("P2.27")
                        || folder.getName().equals("P2.21")) {
                    continue;
                }

                final String folderName = folder.getName();

                for (Language language : LALanguages) {

                    System.out.println("\nLang:" + language.toString());
                    System.out.println("TestFolder:" + folderName);

                    final RunnerWrapper runnerWrapper =
                            RunnerWrapper.createInstance(folder.getAbsolutePath(), 2, language);
                    runnerWrapper.run(2);
                    try {
                        writerNoViewsMNC
                                .write(language.toString() + "," + folderName + "," + runnerWrapper.getTime() + "\n");
                        writerNoViewsMNCRWs
                                .write(language.toString() + "," + folderName + " : " + runnerWrapper.getRW() + "\n");
                        System.out.print(runnerWrapper.getRW());
                    } catch (IOException e) {
                        final String message = String.format("Couldn't write on %s or %s file", RESULT_NOVIEWS_MNC,
                                RESULT_NOVIEWS_MNC_RWS);
                        throw new HadadException(message);
                    }
                }

            }

        }
        try {
            writerNoViewsMNC.close();
            writerNoViewsMNCRWs.close();
        } catch (IOException e) {
            final String message =
                    String.format("Couldn't close  %s or %s file", RESULT_NOVIEWS_MNC, RESULT_NOVIEWS_MNC_RWS);
            throw new HadadException(message);
        }

    }

    /**
     * Run views pipeline (naive)
     * 
     * @throws HadadException
     */
    private void runViewsNaive() throws HadadException {
        try {
            writeViewsNaive = new BufferedWriter(new FileWriter(RESULT_VIEWS_NAIVE, true));
            writeViewsNaiveRWS = new BufferedWriter(new FileWriter(RESULT_VIEWS_NAIVE_RWS, true));

        } catch (IOException e) {
            final String message =
                    String.format("Couldn't find %s or %s file ", RESULT_VIEWS_NAIVE, RESULT_VIEWS_NAIVE_RWS);
            throw new HadadException(message);
        }
        initializeMeta();
        final File directory = new File(directoryViews);
        File[] directoryListing = directory.listFiles();
        if (directoryListing != null) {
            for (final File folder : directoryListing) {
                if (folder.getName().equals("P2.25") || folder.getName().equals("P2.27")
                        || folder.getName().equals("P2.21") || folder.getName().equals("P2.14")) {
                    continue;
                }

                final String folderName = folder.getName();

                for (Language language : LALanguages) {

                    System.out.println("\nLang:" + language.toString());
                    System.out.println("TestFolder:" + folderName);
                    final RunnerWrapper runnerWrapper =
                            RunnerWrapper.createInstance(folder.getAbsolutePath(), 1, language);
                    runnerWrapper.run(1);
                    try {
                        writeViewsNaive
                                .write(language.toString() + "," + folderName + "," + runnerWrapper.getTime() + "\n");
                        writeViewsNaiveRWS
                                .write(language.toString() + "," + folderName + " : " + runnerWrapper.getRW() + "\n");
                        System.out.print(runnerWrapper.getRW());
                    } catch (IOException e) {
                        final String message = String.format("Couldn't write on %s or %s file", RESULT_NOVIEWS_NAIVE,
                                RESULT_NOVIEWS_NAIVE_RWS);
                        throw new HadadException(message);
                    }
                }

            }
        }
        try

        {
            writeViewsNaive.close();
            writeViewsNaiveRWS.close();
        } catch (IOException e) {
            final String message =
                    String.format("Couldn't close  %s or %s file", RESULT_VIEWS_NAIVE, RESULT_VIEWS_NAIVE_RWS);
            throw new HadadException(message);
        }

    }

    /**
     * Run MR Pipeline
     * 
     * @throws HadadException
     */
    private void runHybridMR() throws HadadException {
        try {
            writHybridNaiveMR = new BufferedWriter(new FileWriter(RESULT_HYBRID_NAIVE_MR, true));
            writHybridNaiveRWSMR = new BufferedWriter(new FileWriter(RESULT_HYBRID_NAIVE_MR_RWS, true));

        } catch (IOException e) {
            final String message =
                    String.format("Couldn't find %s or %s file ", RESULT_HYBRID_NAIVE_MR, RESULT_HYBRID_NAIVE_MR_RWS);
            throw new HadadException(message);
        }
        initializeMeta();
        final File directory = new File(directoryHybridMR);
        File[] directoryListing = directory.listFiles();
        if (directoryListing != null) {
            for (final File folder : directoryListing) {
                final String folderName = folder.getName();
                MRRunnerWrapper runnerWrapper = new MRRunnerWrapper(folder.getAbsolutePath(), 1);
                runnerWrapper.run(1);
                try {
                    writHybridNaiveMR.write("MR" + "," + folderName + "," + runnerWrapper.getTime() + "\n");
                    writHybridNaiveRWSMR.write("MR" + "," + folderName + " : " + runnerWrapper.getRW() + "\n");

                } catch (IOException e) {
                    final String message = String.format("Couldn't write on %s or %s file", RESULT_HYBRID_NAIVE_MR,
                            RESULT_HYBRID_NAIVE_MR_RWS);
                    throw new HadadException(message);
                }
            }
        }
        try {
            writHybridNaiveMR.close();
            writHybridNaiveRWSMR.close();
        } catch (IOException e) {
            final String message =
                    String.format("Couldn't close  %s or %s file", RESULT_NOVIEWS_NAIVE, RESULT_NOVIEWS_NAIVE_RWS);
            throw new HadadException(message);
        }

    }

    private void initializeMeta() throws MetadataException {

        if (!metadataPath.equals("src/main/resources/metadata.json")) {
            final File file = new File(metadataPath);
            final String absolutePath = file.getAbsolutePath();
            Metadata.MetadataInit(absolutePath);
        }
        Metadata.load();
        Metadata.getInstance();
        Metadata.instance.delete();
        Metadata.instance.add("syn1.csv", 50000, 100, 5000000, "");
        Metadata.instance.add("syn2.csv", 100, 50000, 5000000, "");
        Metadata.instance.add("syn2D.csv", 50000, 100, 5000000, "");
        Metadata.instance.add("syn1D.csv", 100, 50000, 5000000, "");
        Metadata.instance.add("syn3a.csv", 1000000, 100, 100000000, "");
        Metadata.instance.add("syn3b.csv", 1000000, 100, 100000000, "");
        Metadata.instance.add("syn4a.csv", 5000000, 100, 100000000, "");
        Metadata.instance.add("syn4b.csv", 5000000, 100, 100000000, "");
        Metadata.instance.add("syn5c.csv", 10000, 10000, 100000000, "");
        Metadata.instance.add("syn5d.csv", 10000, 10000, 100000000, "");
        Metadata.instance.add("syn5e.csv", 10000, 10000, 100000000, "");
        Metadata.instance.add("syn6c.csv", 20000, 20000, 400000000, "");
        Metadata.instance.add("syn6d.csv", 20000, 20000, 400000000, "");
        Metadata.instance.add("syn6e.csv", 20000, 20000, 400000000, "");
        Metadata.instance.add("syn7.csv", 100, 1, 100, "");
        Metadata.instance.add("syn8.csv", 50000, 1, 50000, "");
        Metadata.instance.add("syn9.csv", 100000, 1, 100000, "");
        Metadata.instance.add("syn10.csv", 100, 100, 10000, "");
        Metadata.instance.add("AS.mtx", 50000, 100, 378, "");
        Metadata.instance.add("AM.mtx", 100000, 100, 673, "");
        Metadata.instance.add("AL1.mtx", 1000000, 100, 6539, "");
        Metadata.instance.add("AL2.mtx", 10000000, 100, 11897, "");
        Metadata.instance.add("AL3.mtx", 100000, 50000, 103557, "");
        Metadata.instance.add("NS.mtx", 50000, 100, 69559, "");
        Metadata.instance.add("NM.mtx", 100000, 100, 139344, "");
        Metadata.instance.add("NL1.mtx", 1000000, 100, 665445, "");
        Metadata.instance.add("NL2.mtx", 10000000, 100, 665445, "");
        Metadata.instance.add("NL3.mtx", 100000, 50000, 15357418, "");
        Metadata.instance.add("R.csv", 3, 3, 9, "");
        Metadata.instance.add("S.csv", 4, 3, 12, "");
        Metadata.instance.add("A.csv", 4, 4, 16, "");

        Metadata.instance.add("V1.csv", 10000, 10000, 100000000, "");
        Metadata.instance.add("V2.csv", 10000, 10000, 100000000, "");
        Metadata.instance.add("V3.csv", 100, 100, 10000, "");
        Metadata.instance.add("VZ.csv", 10000, 10000, 100000000, "");
        Metadata.instance.add("V4.csv", 100000, 50000, 2000000000, "");//5B > INTEGER.max. So far put it as 2000000000.
        Metadata.instance.add("V5A.csv", 10000, 10000, 100000000, "");
        Metadata.instance.add("V5B.csv", 20000, 20000, 400000000, "");
        Metadata.instance.add("V6A.csv", 1000000, 100, 100000000, "");
        Metadata.instance.add("V6B.csv", 5000000, 100, 500000000, "");
        Metadata.instance.add("V7.csv", 10000, 10000, 100000000, "");
        Metadata.instance.add("V8A.csv", 10000, 10000, 100000000, "");
        Metadata.instance.add("V8B.csv", 10000, 10000, 100000000, "");
        Metadata.instance.add("V9.csv", 10000, 10000, 100000000, "");
        Metadata.instance.add("V10.csv", 1, 1, 1, "");
        Metadata.instance.add("V11.csv", 1, 1, 1, "");
        Metadata.instance.add("V12A.csv", 1000000, 100, 100000000, "");
        Metadata.instance.add("V12.csv", 10000, 10000, 100000000, "");
        Metadata.instance.add("V12B.csv", 5000000, 100, 500000000, "");
    }

}