/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.runner;

import com.google.inject.Guice;
import com.google.inject.Injector;

/**
 * Main Runner
 * 
 * @author ranaalotaibi
 *
 */
public class Runner {
    public static void main(String[] args) throws NumberFormatException,

            Exception {
        Injector injector = Guice.createInjector(new HadadModule());
        final HadadRunner runner = injector.getInstance(HadadRunner.class);
        runner.run();
    }
}
