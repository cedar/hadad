/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.runner;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import hadad.base.compiler.Language;
import hadad.base.compiler.exceptions.HadadException;
import hadad.base.compiler.model.la.nm.NRunnerWrapper;
import hadad.base.compiler.model.la.rm.RRunnerWrapper;
import hadad.base.compiler.model.la.sm.SRunnerWrapper;
import hadad.base.compiler.model.la.tm.TRunnerWrapper;
import hadad.commons.conjunctivequery.ConjunctiveQuery;

/**
 * RunnerWrapper
 * 
 * @author ranaalotaibi
 *
 */
public abstract class RunnerWrapper {
    protected static final Logger LOGGER = Logger.getLogger(RunnerWrapper.class.getName());
    static {
        LOGGER.setLevel(Level.OFF);
    }
    private static RunnerWrapper instance;

    public static RunnerWrapper createInstance(final String directory, final int falg, final Language language)
            throws HadadException {

        switch (language) {
            case R:
                instance = new RRunnerWrapper(directory, falg);
                return instance;
            case NUMPY:
                instance = new NRunnerWrapper(directory, falg);
                return instance;
            case DML:
                instance = new SRunnerWrapper(directory, falg);
                return instance;
            case TFM:
                instance = new TRunnerWrapper(directory, falg);
                return instance;
            default:
                throw new HadadException("language not supported!");
        }
    }

    public abstract void run() throws HadadException;

    public abstract void run(int costModel) throws HadadException;

    public abstract ConjunctiveQuery getRW();

    public abstract double getTime();

}
