package hadad.base.utils;

import java.util.HashMap;
import java.util.Map;

import hadad.base.compiler.Language;

public class StringUtils {

    public static String processQuery(final String query, final Language lang) {
        switch (lang) {
            case R:
                return processRQuery(query);
            case MR:
                return procesMRQuery(query);
            //TODO: add others
        }
        return null;
    }

    public static Map<String, String> processView(final String view, final Language language) {
        switch (language) {
            case R:
                return processRView(view);
            //TODO: add others
        }
        return null;
    }

    private static String processRQuery(final String query) {
        final StringBuilder stringBuilder = new StringBuilder();
        final String[] strs = query.split("\n");
        stringBuilder.append("Q: ");
        for (int i = 0; i < strs.length; i++) {
            if (strs[i].contains("library") || strs[i].contains("source")) {
                continue;
            }
            stringBuilder.append(strs[i] + "\n");
        }
        return stringBuilder.toString();
    }

    private static String procesMRQuery(final String query) {
        final StringBuilder stringBuilder = new StringBuilder();
        final String[] strs = query.split("\n");
        stringBuilder.append("Q: ");
        for (int i = 0; i < strs.length; i++) {
            if (strs[i].contains("library") || strs[i].contains("source")) {
                continue;
            }
            stringBuilder.append(strs[i] + "\n");
        }
        return stringBuilder.toString();
    }

    public static Map<String, String> processRView(final String query) {
        final String trimQuery = query.trim();
        final int index = trimQuery.indexOf(";");
        final String subStringBody = trimQuery.substring(index + 1, trimQuery.length());
        //Process Body 
        final StringBuilder body = new StringBuilder();
        body.append("View: ");

        //Process View name
        final String[] splitedBody = subStringBody.split(";");

        int length = splitedBody.length;
        int i = 0;
        for (String entry : splitedBody) {
            if (i != (length - 1)) {
                body.append(entry + ";");
            }
            i++;
        }

        final String writeStatement = splitedBody[length - 1];
        final String[] splitedWriteStatement = writeStatement.split(",");
        final int firstQuote = splitedWriteStatement[1].indexOf("\"");
        final int secondQuote = splitedWriteStatement[1].lastIndexOf("\"") + 1;
        final String viewPath = splitedWriteStatement[1].substring(firstQuote, secondQuote);

        final Map<String, String> view = new HashMap<>();
        view.put(viewPath, body.toString());
        return view;

    }

}
