/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.encoder.la.sm;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import hadad.base.compiler.PathExpression;
import hadad.base.compiler.QueryBlockTree;
import hadad.base.compiler.Utils;
import hadad.base.compiler.exceptions.HadadCompilationException;
import hadad.base.test.framework.AbstracEncoderCompilerTest;
import hadad.commons.conjunctivequery.Atom;
import hadad.commons.conjunctivequery.ConjunctiveQuery;
import hadad.commons.conjunctivequery.parser.ParseException;

/**
 * AbstractSMEncoderCompilerTest
 * 
 * @author ranaalotaibi
 *
 */
public abstract class AbstractSMEncoderCompilerTest extends AbstracEncoderCompilerTest {

    protected abstract ConjunctiveQuery restrict(final ConjunctiveQuery query);

    private File getTestQuery(final File testDirectory) throws IOException {
        for (final File file : testDirectory.listFiles()) {
            if (file.isFile() && file.getName().equals("query")) {
                return file;
            }
        }
        throw new IOException("Query file not found.");
    }

    private File getTestEncoding(final File testDirectory) throws IOException {
        for (final File file : testDirectory.listFiles()) {
            if (file.isFile() && file.getName().equals("encoding")) {
                return file;
            }
        }
        throw new IOException("Encoding file not found.");
    }

    private List<File> getTestsDirectories() {
        final String TESTS_DIR = "/compiler/sm-encoder";
        final File directory =
                new File(AbstractSMEncoderCompilerTest.class.getResource(TESTS_DIR).toString().substring(5));
        final List<File> testsDirectory = new ArrayList<File>();
        for (final File file : directory.listFiles()) {
            if (file.isDirectory()) {
                testsDirectory.add(file);
            }
        }
        return testsDirectory;
    }

    @Test
    public void test() {
        System.out.println("Testing: SMEncoderCompiler");
        getTestsDirectories().stream().forEach(f -> test(f));
    }

    private void test(final File testDirectory) {
        System.out.println("Test directory: " + testDirectory.getName());

        String query;
        try {
            query = FileUtils.readFileToString(getTestQuery(testDirectory));
        } catch (IOException e1) {
            throw new RuntimeException(e1);
        }
        QueryBlockTree nbt;
        try {
            nbt = builder.buildQueryBlockTree(query);
        } catch (hadad.base.compiler.exceptions.ParseException e1) {
            throw new HadadCompilationException(e1);
        }
        final StringBuilder querystrBuilder = new StringBuilder();
        final Collection<PathExpression> paths = nbt.getRoot().getPattern().getStructural().getPathExpressions();
        final List<Atom> atoms = new ArrayList<>();
        final String queryName = nbt.getRoot().getQueryName();
        final String returnVar =
                nbt.getRoot().getReturnTemplate().getTerms().get(0).getParent().getElement().toString();
        querystrBuilder.append(queryName);
        querystrBuilder.append("<");
        querystrBuilder.append(returnVar);
        querystrBuilder.append(">:-");

        for (final PathExpression path : paths) {
            atoms.addAll(path.encoding());
        }
        int i = 0;
        for (final Atom atom : atoms) {
            querystrBuilder.append(atom);
            if (i != atoms.size() - 1)
                querystrBuilder.append(",");
            i++;
        }

        querystrBuilder.append(";");

        final Collection<ConjunctiveQuery> expectedEncoding;

        try {
            expectedEncoding = Utils.parseQueries(FileUtils.readFileToString(getTestEncoding(testDirectory)));
        } catch (ParseException | IOException e) {
            throw new RuntimeException(e);
        }

        final List<ConjunctiveQuery> encoding;

        try {
            encoding = new ArrayList<>(Utils.parseQueries(querystrBuilder.toString()));
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }

        final boolean areEquivalent = areEquivalent(encoding, new ArrayList<ConjunctiveQuery>(expectedEncoding));
        if (!areEquivalent) {
            System.out.println("The expected and current reformulations mismatch.");
            System.out.println("Expected: " + expectedEncoding.toString());
            System.out.println("Current: " + encoding.toString());
        }

        assertTrue(areEquivalent);

    }

    private boolean areEquivalent(final List<ConjunctiveQuery> reformulations,
            final List<ConjunctiveQuery> expectedReformulations) {
        if (reformulations.size() != expectedReformulations.size()) {
            return false;
        }
        final Map<Integer, Integer> mapping = new HashMap<Integer, Integer>();
        int i = 0;
        for (final ConjunctiveQuery reformulation : reformulations) {
            final Integer pos = mapping(restrict(reformulation), expectedReformulations, mapping);
            if (pos == null) {
                return false;
            } else {
                mapping.put(i, pos);
            }
            i++;
        }
        return true;
    }

    private Integer mapping(final ConjunctiveQuery reformulation, final List<ConjunctiveQuery> expectedReformulations,
            final Map<Integer, Integer> mapping) {
        for (Integer i = 0; i < expectedReformulations.size(); i++) {
            if (!mapping.values().contains(i) && reformulation.isEquivalent(expectedReformulations.get(i))) {
                return i;
            }
        }
        return null;
    }
}
