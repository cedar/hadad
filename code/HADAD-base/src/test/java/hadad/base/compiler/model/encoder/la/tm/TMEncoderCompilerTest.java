/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.encoder.la.tm;

import org.junit.After;
import org.junit.Before;

import com.google.inject.Guice;
import com.google.inject.Injector;

import hadad.base.compiler.IQueryBlockTreeBuilder;
import hadad.base.compiler.model.la.tm.TMNaiveModule;
import hadad.base.compiler.model.la.tm.TMQueryBuilder;
import hadad.commons.conjunctivequery.ConjunctiveQuery;

/**
 * TMEncoderCompilerTest
 * 
 * @author ranaalotaibi
 *
 */
public final class TMEncoderCompilerTest extends AbstractTMEncoderCompilerTest {
    private Injector injector;

    @Override
    @Before
    public void setUp() {
        injector = Guice.createInjector(new TMNaiveModule());
        super.setUp();
    }

    @Override
    @After
    public void tearDown() {
        injector = null;
        super.tearDown();
    }

    @Override
    protected IQueryBlockTreeBuilder createBuilder() {
        return injector.getInstance(TMQueryBuilder.class);
    }

    @Override
    protected ConjunctiveQuery restrict(final ConjunctiveQuery query) {
        return query;
    }
}
