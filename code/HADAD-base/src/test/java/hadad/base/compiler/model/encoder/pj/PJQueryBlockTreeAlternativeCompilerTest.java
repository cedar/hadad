/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.encoder.pj;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;

import com.google.inject.Guice;
import com.google.inject.Injector;

import hadad.base.compiler.IQueryBlockTreeBuilder;
import hadad.base.compiler.IQueryBlockTreeViewCompiler;
import hadad.base.compiler.model.pj.PJQueryBlockTreeBuilder;
import hadad.base.compiler.model.pj.Utils;
import hadad.base.compiler.model.pj.naive.PJNaiveModule;
import hadad.base.compiler.model.pj.naive.PJNaiveQueryBlockTreeCompiler;
import hadad.commons.conjunctivequery.ConjunctiveQuery;

/**
 * PJ QueryBlockTreeAlternativeCompilerTest
 * 
 * @author ranaalotaibi
 *
 */
public final class PJQueryBlockTreeAlternativeCompilerTest extends PJQueryBlockTreeCompilerTest {
    private Injector injector;

    @Override
    @Before
    public void setUp() {
        injector = Guice.createInjector(new PJNaiveModule());;
        super.setUp();
    }

    @Override
    @After
    public void tearDown() {
        injector = null;
        super.tearDown();
    }

    @Override
    protected IQueryBlockTreeBuilder createBuilder() {
        return injector.getInstance(PJQueryBlockTreeBuilder.class);
    }

    @Override
    protected List<IQueryBlockTreeViewCompiler> createCompilers() {
        final List<IQueryBlockTreeViewCompiler> compilers = new ArrayList<IQueryBlockTreeViewCompiler>();
        compilers.add(injector.getInstance(PJNaiveQueryBlockTreeCompiler.class));

        return compilers;
    }

    @Override
    protected ConjunctiveQuery restrict(final ConjunctiveQuery query) {
        return Utils.restrict(query);
    }
}
