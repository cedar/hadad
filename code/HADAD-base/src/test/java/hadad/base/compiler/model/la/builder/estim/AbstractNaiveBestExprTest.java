/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.builder.estim;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import hadad.base.compiler.Utils;
import hadad.base.compiler.model.la.builder.LAPlanBuilder;
import hadad.base.compiler.model.la.builder.operators.BinaryOperator;
import hadad.base.compiler.model.la.builder.operators.IOperator;
import hadad.base.compiler.model.la.builder.operators.UnaryOperator;
import hadad.base.rewriter.Context;
import hadad.base.rewriter.PACBConjunctiveQueryRewriter;
import hadad.base.test.framework.AbstractEstimBestExprTest;
import hadad.commons.conjunctivequery.ConjunctiveQuery;
import hadad.commons.conjunctivequery.parser.ParseException;
import hadad.commons.constraints.Constraint;
import hadad.commons.constraints.parser.ConstraintParser;
import hadad.commons.relationalschema.RelationalSchema;
import hadad.commons.relationalschema.parser.RelSchemaParser;

/**
 * AbstractNaiveBestExprTest
 * 
 * @author ranaalotaibi
 *
 */
public abstract class AbstractNaiveBestExprTest extends AbstractEstimBestExprTest {

    protected abstract ConjunctiveQuery restrict(ConjunctiveQuery reformulation);

    private File getTestQuery(final File testDirectory) throws IOException {
        for (final File file : testDirectory.listFiles()) {
            if (file.isFile() && file.getName().equals("query")) {
                return file;
            }
        }
        throw new IOException("Query file not found.");
    }

    private File getTestBestRW(final File testDirectory) throws IOException {
        for (final File file : testDirectory.listFiles()) {
            if (file.isFile() && file.getName().equals("expectedRW")) {
                return file;
            }
        }
        throw new IOException("Encoding file not found.");
    }

    private List<File> getTestsDirectories() {
        final String TESTS_DIR = "/compiler/la-naive-best-expr";
        final File directory = new File(AbstractNaiveBestExprTest.class.getResource(TESTS_DIR).toString().substring(5));
        final List<File> testsDirectory = new ArrayList<File>();
        for (final File file : directory.listFiles()) {
            if (file.isDirectory()) {
                testsDirectory.add(file);
            }
        }
        return testsDirectory;
    }

    @Test
    public void test() {
        System.out.println("Testing: NaiveBestExprTest");
        getTestsDirectories().stream().forEach(f -> test(f));
    }

    private void test(final File testDirectory) {

        System.out.println("Test directory :" + testDirectory.getName());
        final List<ConjunctiveQuery> query;
        try {
            query = new ArrayList<>(Utils.parseQueries(FileUtils.readFileToString(getTestQuery(testDirectory))));
        } catch (IOException | ParseException e1) {
            throw new RuntimeException(e1);
        }

        final List<ConjunctiveQuery> currentRW = new ArrayList<ConjunctiveQuery>();
        try {
            currentRW.add(computeRewritings(query.get(0), testDirectory));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        final List<ConjunctiveQuery> expectedRw;
        try {
            expectedRw = new ArrayList<>(Utils.parseQueries(FileUtils.readFileToString(getTestBestRW(testDirectory))));
        } catch (IOException | ParseException e) {
            throw new RuntimeException(e);
        }

        final boolean areEquivalent = areEquivalent(currentRW, expectedRw);

        if (!areEquivalent) {
            System.out.println("The expected and current reformulations mismatch.");
            System.out.println("Expected: " + expectedRw.toString());
            System.out.println("Current: " + currentRW.toString());
        }
        assertTrue(areEquivalent);

    }

    private Context getContext(final File testDirectory) throws Exception {
        final List<Constraint> fw = parseConstraints(testDirectory.getAbsolutePath() + "/constraints_chase");
        final List<Constraint> bw = parseConstraints(testDirectory.getAbsolutePath() + "/constraints_bkchase");
        final List<RelationalSchema> schemas = parseSchemas(testDirectory.getAbsolutePath() + "/schemas");
        return new Context(schemas.get(0), schemas.get(1), fw, bw);
    }

    private List<Constraint> parseConstraints(final String fileName)
            throws IOException, hadad.commons.constraints.parser.ParseException {
        final FileReader fr = new FileReader(fileName);
        final ConstraintParser parser = new ConstraintParser(fr);
        final List<Constraint> constraints = parser.parse();
        fr.close();
        return constraints;
    }

    private static List<RelationalSchema> parseSchemas(final String fileName)
            throws IOException, hadad.commons.relationalschema.parser.ParseException {
        final FileReader fr = new FileReader(fileName);
        final RelSchemaParser parser = new RelSchemaParser(fr);
        final List<RelationalSchema> constraints = parser.parse();
        fr.close();
        return constraints;
    }

    private PACBConjunctiveQueryRewriter getRewriter(final File testDirectory) throws Exception {
        return new PACBConjunctiveQueryRewriter(getContext(testDirectory));
    }

    private ConjunctiveQuery computeRewritings(final ConjunctiveQuery query, final File testDirectory)
            throws Exception {
        List<ConjunctiveQuery> rws = getRewriter(testDirectory).getReformulations(query);
        return naive(rws);
    }

    private ConjunctiveQuery naive(List<ConjunctiveQuery> rws) {
        double minCost = Double.MAX_VALUE;
        ConjunctiveQuery minRW = null;
        // warm-up
        for (final ConjunctiveQuery rw : rws) {
            final IOperator root = LAPlanBuilder.constructPlan(rw);
            NaiveCostEstimator estimator = null;
            if (root != null) {
                estimator = new NaiveCostEstimator();
                if (root instanceof UnaryOperator) {
                    estimator.visit((UnaryOperator) root, null);

                }
                if (root instanceof BinaryOperator) {
                    estimator.visit((BinaryOperator) root, null);

                }
                double currentCost = estimator.getEstimatedCost();
                if (currentCost <= minCost) {
                    minCost = currentCost;
                    minRW = rw;
                }
            }
        }
        return minRW;

    }

    private boolean areEquivalent(final List<ConjunctiveQuery> reformulations,
            final List<ConjunctiveQuery> expectedReformulations) {
        if (reformulations.size() != expectedReformulations.size()) {
            return false;
        }
        final Map<Integer, Integer> mapping = new HashMap<Integer, Integer>();
        int i = 0;
        for (final ConjunctiveQuery reformulation : reformulations) {
            final Integer pos = mapping(restrict(reformulation), expectedReformulations, mapping);
            if (pos == null) {
                return false;
            } else {
                mapping.put(i, pos);
            }
            i++;
        }
        return true;
    }

    private Integer mapping(final ConjunctiveQuery reformulation, final List<ConjunctiveQuery> expectedReformulations,
            final Map<Integer, Integer> mapping) {
        for (Integer i = 0; i < expectedReformulations.size(); i++) {
            if (!mapping.values().contains(i) && reformulation.isEquivalent(expectedReformulations.get(i))) {
                return i;
            }
        }
        return null;
    }

}
