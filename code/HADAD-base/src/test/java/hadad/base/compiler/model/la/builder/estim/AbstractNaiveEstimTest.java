/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.compiler.model.la.builder.estim;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import hadad.base.compiler.Utils;
import hadad.base.compiler.model.la.builder.LAPlanBuilder;
import hadad.base.compiler.model.la.builder.operators.BinaryOperator;
import hadad.base.compiler.model.la.builder.operators.IOperator;
import hadad.base.compiler.model.la.builder.operators.UnaryOperator;
import hadad.base.test.framework.AbstractEstimBestExprTest;
import hadad.commons.conjunctivequery.ConjunctiveQuery;
import hadad.commons.conjunctivequery.parser.ParseException;

/**
 * AbstractNaiveEstimTest
 * 
 * @author ranaalotaibi
 *
 */
public abstract class AbstractNaiveEstimTest extends AbstractEstimBestExprTest {

    private File getTestQuery(final File testDirectory) throws IOException {
        for (final File file : testDirectory.listFiles()) {
            if (file.isFile() && file.getName().equals("rewriting")) {
                return file;
            }
        }
        throw new IOException("Query file not found.");
    }

    private File getTestEstim(final File testDirectory) throws IOException {
        for (final File file : testDirectory.listFiles()) {
            if (file.isFile() && file.getName().equals("estim")) {
                return file;
            }
        }
        throw new IOException("Encoding file not found.");
    }

    private List<File> getTestsDirectories() {
        final String TESTS_DIR = "/compiler/la-naive-estim";
        final File directory = new File(AbstractNaiveEstimTest.class.getResource(TESTS_DIR).toString().substring(5));
        final List<File> testsDirectory = new ArrayList<File>();
        for (final File file : directory.listFiles()) {
            if (file.isDirectory()) {
                testsDirectory.add(file);
            }
        }
        return testsDirectory;
    }

    @Test
    public void test() {
        System.out.println("Testing: NaiveEstim");
        getTestsDirectories().stream().forEach(f -> test(f));
    }

    private void test(final File testDirectory) {
        System.out.println("Test directory" + testDirectory.getName());
        final List<ConjunctiveQuery> rewriting;
        try {
            rewriting = new ArrayList<>(Utils.parseQueries(FileUtils.readFileToString(getTestQuery(testDirectory))));
        } catch (IOException | ParseException e1) {
            throw new RuntimeException(e1);
        }

        double estim = 0;
        final IOperator root = LAPlanBuilder.constructPlan(rewriting.get(0));
        NaiveCostEstimator estimator = null;
        if (root != null) {
            estimator = new NaiveCostEstimator();
            if (root instanceof UnaryOperator) {
                estimator.visit((UnaryOperator) root, null);

            }
            if (root instanceof BinaryOperator) {
                estimator.visit((BinaryOperator) root, null);

            }

            estim = estimator.getEstimatedCost();
        }
        System.out.println(estim);

        double expectedEstim;
        try {
            expectedEstim = Double.valueOf(FileUtils.readFileToString(getTestEstim(testDirectory)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        final boolean areEquivalent = expectedEstim == estim;
        assertTrue(areEquivalent);
    }

}
