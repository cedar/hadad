/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.test.framework;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;

import com.google.common.collect.ImmutableList;

import hadad.base.compiler.QueryBlockTree;
import hadad.base.compiler.IQueryBlockTreeBuilder;
import hadad.base.compiler.IQueryBlockTreeViewCompiler;
import hadad.base.rewriter.ConjunctiveQueryRewriter;
import hadad.base.rewriter.Context;
import hadad.base.rewriter.PACBConjunctiveQueryRewriter;
import hadad.commons.relationalschema.Relation;
import hadad.commons.relationalschema.RelationalSchema;

/**
 * Abstract class for query block tree compilers unit tests.
 * 
 * @author ranaalotaibi
 *
 */
public abstract class AbstractQueryBlockTreeCompilerTest {
    protected IQueryBlockTreeBuilder builder;
    private ImmutableList<IQueryBlockTreeViewCompiler> compilers;

    @Before
    public void setUp() {
        builder = createBuilder();
        compilers = ImmutableList.copyOf(createCompilers());
    }

    @After
    public void tearDown() {
        builder = null;
        compilers = null;
    }

    protected abstract IQueryBlockTreeBuilder createBuilder();

    protected abstract List<IQueryBlockTreeViewCompiler> createCompilers();

    protected List<Context> createContexts(final List<QueryBlockTree> nbts) {
        final List<Context> contexts = new ArrayList<Context>();
        compilers.stream().forEach(
                c -> contexts.add(c.compileContext(nbts, new RelationalSchema(new ArrayList<Relation>()), false)));
        return contexts;
    }

    protected List<ConjunctiveQueryRewriter> createRewriters(final List<QueryBlockTree> nbts) {
        final List<ConjunctiveQueryRewriter> rewriters = new ArrayList<ConjunctiveQueryRewriter>();
        createContexts(nbts).stream().forEach(c -> rewriters.add(new PACBConjunctiveQueryRewriter(c)));
        return rewriters;
    }
}
