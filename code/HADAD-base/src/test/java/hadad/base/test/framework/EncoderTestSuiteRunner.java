/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.base.test.framework;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import hadad.base.compiler.model.encoder.aj.AJQueryBlockTreeNaiveCompilerTest;
import hadad.base.compiler.model.encoder.la.mr.MREncoderCompilerTest;
import hadad.base.compiler.model.encoder.la.nm.NMEncoderCompilerTest;
import hadad.base.compiler.model.encoder.la.rm.RMEncoderCompilerTest;
import hadad.base.compiler.model.encoder.la.sm.SMEncoderCompilerTest;
import hadad.base.compiler.model.encoder.pj.PJQueryBlockTreeNaiveCompilerTest;
import hadad.base.compiler.model.encoder.pr.PRQueryBlockTreeNaiveCompilerTest;
import hadad.base.compiler.model.encoder.sj.SJQueryBlockTreeNaiveCompilerTest;

/**
 * Encoders Test Suite Runner
 * 
 * @author ranaalotaibi
 */
@RunWith(Suite.class)
@SuiteClasses({ PJQueryBlockTreeNaiveCompilerTest.class, SJQueryBlockTreeNaiveCompilerTest.class,
        AJQueryBlockTreeNaiveCompilerTest.class, PRQueryBlockTreeNaiveCompilerTest.class, NMEncoderCompilerTest.class,
        RMEncoderCompilerTest.class, SMEncoderCompilerTest.class, MREncoderCompilerTest.class })
public class EncoderTestSuiteRunner {
};
