package hadad.commons.conjunctivequery;

/**
 * Constants are used to represent constant objects in the CQs.
 * 
 * @author Stamatis Zampetakis
 * @author Damian Bursztyn
 */
public abstract class Constant implements Term, java.io.Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    public boolean isConstant() {
        return true;
    }

    @Override
    public boolean isVariable() {
        return false;
    }

    public abstract Object getValue();
}
