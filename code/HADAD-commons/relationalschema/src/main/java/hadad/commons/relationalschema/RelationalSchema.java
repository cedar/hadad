/**
  * Copyright 2013, 2014 Ioana Ileana @ Telecom ParisTech 
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/
package hadad.commons.relationalschema;

import java.util.List;

/**
 * A relational schema
 * 
 * @author Ioana Ileana
 */
public class RelationalSchema implements java.io.Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /** The relations in the schema */
    private List<Relation> relations;

    /**
     * Constructor
     *
     * @param relations
     *            The relations in the schema
     */
    public RelationalSchema(final List<Relation> relations) {
        this.relations = relations;
    }

    /**
     * Returns the relations in the schema
     * 
     * @return the relations in the schema
     */
    public List<Relation> getRelations() {
        return relations;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder();
        for (Relation r : relations) {
            result.append(r);
            result.append(',');
        }
        if (relations.size() != 0) {
            result.deleteCharAt(result.length() - 1);
        }
        result.append(";");
        return result.toString();
    }
}
