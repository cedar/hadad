<p>
    <img src="https://gitlab.inria.fr/cedar/hadad/-/raw/master/HADAD.png"  width=150  >
    <br>

</p>

# My first `HADAD` 
This tutorial is aimed at getting you up and running with `HADAD` internal representation.
If you haven't heard about conjunctive queries and rewriting under integrity constraints, you may want to read 
[HADAD paper](https://arxiv.org/pdf/2103.12317.pdf). **More materials to be added (in progress)**

# Tutorial Setup 
(1) [Build](https://gitlab.inria.fr/cedar/hadad#code-requirements-and-build) `HADAD` and let's open an [LA Example folder](https://gitlab.inria.fr/cedar/hadad/-/tree/master/code/HADAD-tutorial/examples/LAExample), where you can add your LA query, constraints, and generated data for the example. You need to provide a `config.json` file that includes the following information: 

```bash
{
	
	"exampleFolder":"examples/LAExample/",
	"lang": "R",
	"costModel":0, 
	"metadata": [
		{
			"MatrixFilepath":"examples/LAExample/M.mtx", 
			"nnz": 15000,
			"nrow": 1000,
			"ncol": 50 
		},
		
		{
			"MatrixFilepath":"examples/LAExample/N.csv",
			"nnz": 50000,
			"nrow": 1000,
			"ncol": 50
		}
		]
}
```
`Config.json` **info**:
* `"exampleFolder":"examples/LAExample/"` (Example folder path)
* `"lang": "R"` (Used query language. For example, in this tutorial, we use `R`-R language and `MR`-MorpheusR language)
* `"costModel":0` (0: no cost model, 1:naive cost model, 2: MNC based cost model)
* `"MatrixFilepath":"examples/LAExample/M.mtx"`  (path of a matrix data file) 
* `"nnz": 15000` (number of non-zeros), `"nrow": 15000` (number of rows), and `"ncol": 50` (number of columns)

**NOTE: Computing base matrices histgoram can take sometime if the size of the base matrix is large.**

(2) Let's generate some synthetic matrix data
```bash
# python3.7 or python3 both work
$ cd examples/LAExample/
# It generates M.csv matrix, where 30% of the matrix is dense(=1) and 70% is sparse(=0).
$ python3.7 ../../../../data/synthetic/generate.py --name M --nrow 1000 --ncol 50 --density 30% 
# Convert M.csv into M.mtx (supported format for sparse matrices).
$ python3.7 ../../../../data/synthetic/toMTX.py --file M.csv
# It generates N.csv dense matrix.
$ python3.7 ../../../../data/synthetic/generate.py --name N --nrow 1000 --ncol 50 --density 100%

```

# Let's Find Some LA Rewritings!
* Add your LA pipeline in the `query.R` file, and make sure to specifiy the used pipeline/script language in `config.json`. In the provided example, we use the `R` langugage and the pipeline is: 

```bash
library(Matrix);
A<- as.matrix(readMM("M.mtx"));
B<- as.matrix(read.table("N.csv",header = FALSE, sep = ","));
R<- sum(A + B);
```
* Add a set of constraints that **you want**  `HADAD` to explore in `constraints_chase` and `constraints_bkchase` files. For example: 

```
#EGDs
name(id1,n1),name(id2,n1)->id2=id1;
add(idA,idB,idr1),add(idA,idB,idr2)->idr1=idr2;
add_s(idA,idB,idr1),add_s(idA,idB,idr2)->idr1=idr2;
sum(id1,idt1),sum(id1,idt2)->idt2=idt1;

#TGDs
#A+B=B+A (matrix addition)
add(idA,idB,idr1)->add(idB,idA,idr1);

#a+b=b+a (scalar addition)
add_s(idA,idB,idr1)->add_s(idB,idA,idr1);

#sum(A+B)=sum(A)+sum(B)
sum (idA,s1),sum(idB,s2),add_s(s1,s2,idr2)->add(idA,idB,idr1),sum(idr1,idr2);
add(idA,idB,idr1),sum(idr1,idr2)->sum(idA,s1),sum(idB,s2),add_s(s1,s2,idr2);

```
* Run the following command to get all possible equivalent rewritings of a given pipeline under a set of given constraints.

```bash 
$ mvn exec:java -Dexec.mainClass=hadad.tutorial.main.Main  #Make sure to run this command under `HADAD-tutorial` directory
```

The output: (As you can see `HADAD` finds 4 rewritings: sum(M+N), sum(N+M), sum(M)+sum(N), and sum(N)+sum(M))
``` 
****** Encoded CQ Query *****
Q<b_2> :- name(b_0,"M.mtx"),add(b_0,b_1,c_2),sum(c_2,b_2),name(b_1,"N.csv");

****** CQ Rewritings *****
RW0<b_2> :- name(b_1,"N.csv"),sum(c_2,b_2),name(b_0,"M.mtx"),add(b_0,b_1,c_2);
RW1<b_2> :- name(b_1,"N.csv"),sum(c_2,b_2),add(b_1,b_0,c_2),name(b_0,"M.mtx");
RW2<b_2> :- name(b_1,"N.csv"),add_s(SK_0,SK_1,b_2),sum(b_0,SK_0),sum(b_1,SK_1),name(b_0,"M.mtx");
RW3<b_2> :- name(b_1,"N.csv"),sum(b_0,SK_0),sum(b_1,SK_1),name(b_0,"M.mtx"),add_s(SK_1,SK_0,b_2);

****** Decoded Rewritings *****
library(Matrix);
b_0<-as.matrix(readMM("M.mtx"))
b_1<-as.matrix(read.table("N.csv", header=FALSE, sep=","))
RW0<-(sum((b_0+b_1)))

library(Matrix);
b_0<-as.matrix(readMM("M.mtx"))
b_1<-as.matrix(read.table("N.csv", header=FALSE, sep=","))
RW1<-(sum((b_1+b_0)))

library(Matrix);
b_0<-as.matrix(readMM("M.mtx"))
b_1<-as.matrix(read.table("N.csv", header=FALSE, sep=","))
RW2<-((sum(b_0))+(sum(b_1)))

library(Matrix);
b_0<-as.matrix(readMM("M.mtx"))
b_1<-as.matrix(read.table("N.csv", header=FALSE, sep=","))
RW3<-((sum(b_1))+(sum(b_0)))
```
* Now, let's see what rewritings we get if we disable the property (constraint) `addition is commuitative`. Go to `constraints_chase` file and comment out (`#`) the constraints as follows:
 ```
 ...
 #A+B=B+A (matrix addition)
 #add(idA,idB,idr1)->add(idB,idA,idr1);
 #a+b=b+a (scalar addition)
 #add_s(idA,idB,idr1)->add_s(idB,idA,idr1);
 ....
 ```
 The output: (As you can see `HADAD` finds 2 rewritings: sum(M+N) and sum(M)+sum(N))
 ```
****** Encoded CQ Query *****
Q<b_2> :- name(b_0,"M.mtx"),add(b_0,b_1,c_2),sum(c_2,b_2),name(b_1,"N.csv");

****** CQ Rewritings *****
RW0<b_2> :- name(b_1,"N.csv"),sum(c_2,b_2),name(b_0,"M.mtx"),add(b_0,b_1,c_2);
RW1<b_2> :- name(b_1,"N.csv"),add_s(SK_0,SK_1,b_2),sum(b_0,SK_0),sum(b_1,SK_1),name(b_0,"M.mtx");

****** Decoded Rewritings *****
library(Matrix);
b_0<-as.matrix(readMM("M.mtx"))
b_1<-as.matrix(read.table("N.csv", header=FALSE, sep=","))
RW0<-(sum((b_0+b_1)))

library(Matrix);
b_0<-as.matrix(readMM("M.mtx"))
b_1<-as.matrix(read.table("N.csv", header=FALSE, sep=","))
RW1<-((sum(b_0))+(sum(b_1)))
 ```

# Searching for an Efficient LA Rewriting
* We update the `costModel` field  in`config.json` as follows to enable finding the best expression based on a selected cost-model:
```bash 
...
 "costModel": 2, // 0:no cost model, 1:Naive cost model and 2: MNC based cost model
...
```
* We re-run the following command:   
```bash 
mvn exec:java -Dexec.mainClass=hadad.tutorial.main.Main #Make sure to run this command under `HADAD-tutorial` directory
```
The output:

```
****** Encoded CQ Query *****
Q<b_2> :- name(b_0,"M.mtx"),add(b_0,b_1,c_2),sum(c_2,b_2),name(b_1,"N.csv");

****** CQ Rewritings *****
RW1<b_2> :- name(b_1,"N.csv"),add_s(SK_0,SK_1,b_2),sum(b_0,SK_0),sum(b_1,SK_1),name(b_0,"M.mtx");

****** Decoded Rewritings *****
library(Matrix);
b_0<-as.matrix(readMM("M.mtx"))
b_1<-as.matrix(read.table("N.csv", header=FALSE, sep=","))
RW1<-((sum(b_0))+(sum(b_1)))
```
**NOTE: The naive cost model will select the same rewriting.**
# LA View-based Rewritings!
(1) let's open [LAViewExample folder](https://gitlab.inria.fr/cedar/hadad/-/tree/master/code/HADAD-tutorial/examples/LAViewExample) to work on our running example. 

(2) We are going to generate more synthetic data for this example as follows:

```bash
# python3.7 or python3 both work
$ cd examples/LAViewExample/
# It generates X.csv matrix, where 30% of the matrix is dense(=1) and 70% is sparse(=0).
$ python3.7 ../../../../data/synthetic/generate.py --name X --nrow 1000 --ncol 50 --density 30% 
# Convert X.csv into X.mtx (supported format for sparse matrices).
$ python3.7 ../../../../data/synthetic/toMTX.py --file X.csv
$ python3.7 ../../../../data/synthetic/generate.py --name U --nrow 1000 --ncol 50 --density 100%
$ python3.7 ../../../../data/synthetic/generate.py --name V1 --nrow 50 --ncol 50 --density 100%
$ python3.7 ../../../../data/synthetic/generate.py --name V2 --nrow 50 --ncol 1 --density 100%
```
(3) We need to update the `config.json` file with the path of our working directory `LAViewExample` and the metadata info of generated matrices
```bash
{
	
	"exampleFolder":"examples/LAViewExample/",
	"lang": "R",
	"costModel":0,
	"metadata": [

		{
			"MatrixFilepath":"examples/LAViewExample/X.mtx",
			"nnz": 15000,
			"nrow": 1000,
			"ncol": 50
		},
		{
			"MatrixFilepath":"examples/LAViewExample/U.csv",
			"nnz": 50000,
			"nrow": 1000,
			"ncol": 50
		},
		
		{
			"MatrixFilepath":"examples/LAViewExample/V1.csv",
			"nnz": 2500,
			"nrow": 50,
			"ncol": 50
		},
		
		{
			"MatrixFilepath":"examples/LAViewExample/V2.csv",
			"nnz": 50,
			"nrow": 50,
			"ncol": 1
		}
		]
}
```
(4) Add the following query in the `query.R` file. 
``` bash
library(Matrix);
X<- as.matrix(readMM("X.mtx"));
U<- as.matrix(read.table("U.csv", header = FALSE, sep = ","));
V1<- as.matrix(read.table("V1.csv", header = FALSE, sep = ","));
V2<- as.matrix(read.table("V2.csv", header = FALSE, sep = ","));
R<- ((U %*% t(V1)) - X) %*%V2;
```

(5) Add a set of constraints that you want `HADAD` to explore for this example in `constraints_chase` and `constraints_bkchase` files. For example,

```
#EGDs
name(id1,n1),name(id2,n1)->id2=id1;
sub(idA,idB,idr1),sub(idA,idB,idr2)->idr1=idr2;
multi(idA,idB,idr1),multi(idA,idB,idr2)->idr1=idr2;

#TGDs
#(AB)C=A(BC)
multi(idA,idB,idr1),multi(idr1,idC,idr2)->multi(idB,idC,idr3),multi(idA,idr3,idr2);
multi(idB,idC,idr3),multi(idA,idr3,idr2)->multi(idA,idB,idr1),multi(idr1,idC,idr2);


#(A-B)C=AC-BC
sub(idA,idB,idr1),multi(idr1,idC,idr2)->multi(idA,idC,idr3),multi(idB,idC,idr4),sub(idr3,idr4,idr2);
multi(idA,idC,idr3),multi(idB,idC,idr4),sub(idr3,idr4,idr2)->sub(idA,idB,idr1),multi(idr1,idC,idr2);
```

(6) Now, we add the following view defintion in `ViewV.R` file (**NOTE:You need to run the R script: `Rscript ViewV.R` to produce the view `ViewV.csv`**). 
```bash
library(Matrix);
V1<- as.matrix(read.table("V1.csv", header = FALSE, sep = ","));
V2<- as.matrix(read.table("V2.csv", header = FALSE, sep = ","));
R<- t(V1)%*%V2;
write.table(R, file="ViewV.csv", sep=",",row.names = F, col.names = F);
```

After that, the view `ViewV` metadata info should be add in `config.json` file as follows:
```bash
...
,
{
	"MatrixFilepath":"examples/LAViewExample/ViewV.csv",
	"nnz": 50,
	"nrow": 50,
	"ncol": 1
}
...
```

(6) Now, we are ready to play with the example. First, we will use `HADAD` to enumerate all possible rewritings including the one with the view. Make sure that the `costModel` field in the `config.json` file is 0. To get all possible rewritings run the following command: 
```bash
$ mvn exec:java -Dexec.mainClass=hadad.tutorial.main.Main  #Make sure to run this command under `HADAD-tutorial` directory
```
The output:
```
****** Encoded CQ Query *****
Q<b_4> :- name(b_0,"X.mtx"),name(b_2,"V1.csv"),name(b_1,"U.csv"),tr(b_2,c_4),multi(b_1,c_4,c_5),sub(c_5,b_0,c_6),multi(c_6,b_3,b_4),name(b_3,"V2.csv");

****** CQ Rewritings *****
RW0<b_4> :- sub(c_5,b_0,c_6),name(b_2,"V1.csv"),name(b_0,"X.mtx"),multi(c_6,b_3,b_4),name(b_3,"V2.csv"),tr(b_2,c_4),multi(b_1,c_4,c_5),name(b_1,"U.csv");
RW1<b_4> :- multi(b_0,b_3,SK_1),name(b_2,"V1.csv"),name(b_0,"X.mtx"),sub(SK_0,SK_1,b_4),multi(c_5,b_3,SK_0),name(b_3,"V2.csv"),tr(b_2,c_4),multi(b_1,c_4,c_5),name(b_1,"U.csv");
RW2<b_4> :- multi(b_0,b_3,SK_1),name(b_2,"V1.csv"),name(b_0,"X.mtx"),multi(b_1,SK_2,SK_0),sub(SK_0,SK_1,b_4),multi(c_4,b_3,SK_2),name(b_3,"V2.csv"),tr(b_2,c_4),name(b_1,"U.csv");
RW3<b_4> :- multi(b_0,b_3,SK_1),name(b_0,"X.mtx"),multi(b_1,SK_2,SK_0),sub(SK_0,SK_1,b_4),name(b_3,"V2.csv"),name(SK_2,"ViewV.csv"),name(b_1,"U.csv");

****** Decoded Rewritings *****
library(Matrix);
b_0<-as.matrix(readMM("X.mtx"))
b_2<-as.matrix(read.table("V1.csv",header=FALSE, sep=","))
b_1<-as.matrix(read.table("U.csv", header=FALSE, sep=","))
b_3<-as.matrix(read.table("V2.csv",header=FALSE, sep=","))
RW0<-(((b_1%*%(t(b_2)))-b_0)%*%b_3)

library(Matrix);
b_0<-as.matrix(readMM("X.mtx"))
b_2<-as.matrix(read.table("V1.csv", header=FALSE, sep=","))
b_1<-as.matrix(read.table("U.csv",  header=FALSE, sep=","))
b_3<-as.matrix(read.table("V2.csv", header=FALSE, sep=","))
RW1<-(((b_1%*%(t(b_2)))%*%b_3)-(b_0%*%b_3))

library(Matrix);
b_0<-as.matrix(readMM("X.mtx"))
b_2<-as.matrix(read.table("V1.csv", header=FALSE, sep=","))
b_1<-as.matrix(read.table("U.csv",  header=FALSE, sep=","))
b_3<-as.matrix(read.table("V2.csv", header=FALSE, sep=","))
RW2<-((b_1%*%((t(b_2))%*%b_3))-(b_0%*%b_3))

library(Matrix);
b_0<-as.matrix(readMM("X.mtx"))
b_1<-as.matrix(read.table("U.csv",  header=FALSE, sep=","))
b_3<-as.matrix(read.table("V2.csv", header=FALSE, sep=","))
SK_2<-as.matrix(read.table("ViewV.csv", header=FALSE, sep=","))
RW3<-((b_1%*%SK_2)-(b_0%*%b_3))
```
`HADAD` will encode the view `ViewV` into a constraint. You will see that the generated constraint is added in `constraints_chase` and `constraints_bkchase` files.

`constraints_chase`:
```
...
name(b_0,"V1.csv"),tr(b_0,c_2),multi(c_2,b_1,b_2),name(b_1,"V2.csv")->name(b_2,"ViewV.csv");
...
```

`constraints_chase`:
```
...
name(b_2,"ViewV.csv")->name(b_0,"V1.csv"),tr(b_0,c_2),multi(c_2,b_1,b_2),name(b_1,"V2.csv");
...
```
Also, as you can see `HADAD` found all 4 rewritings including the one with view `ViewV`: (1)`((U%*%t(V1))-X)%*%V2` (the original query), (2) `(U%*%t(V2))%*%V2-X%*%V2`, (3) `U%*%(t(V2)%*%V2)-X%*%V2`, and (4) `(U%*%ViewV)-(X%*%V2)`.

(7) Let's disable (comment out) some constraints in `constraints_chase` and see how can this affect the results. For example, 
```
#(AB)C=A(BC)
#multi(idA,idB,idr1),multi(idr1,idC,idr2)->multi(idB,idC,idr3),multi(idA,idr3,idr2);
multi(idB,idC,idr3),multi(idA,idr3,idr2)->multi(idA,idB,idr1),multi(idr1,idC,idr2);
```
By disabling this constraint ((AB)C=A(BC) - matrix multiplication associativity), the view `ViewV` cannot be exploited. 

Again, we re-run the following command to get the new results:
```bash
mvn exec:java -Dexec.mainClass=hadad.tutorial.main.Main #Make sure to run this command under `HADAD-tutorial` directory.
```
The output:
```
****** Encoded CQ Query *****
Q<b_4> :- name(b_0,"X.mtx"),name(b_2,"V1.csv"),name(b_1,"U.csv"),tr(b_2,c_4),multi(b_1,c_4,c_5),sub(c_5,b_0,c_6),multi(c_6,b_3,b_4),name(b_3,"V2.csv");

****** CQ Rewritings *****
RW0<b_4> :- multi(c_6,b_3,b_4),name(b_3,"V2.csv"),sub(c_5,b_0,c_6),name(b_2,"V1.csv"),tr(b_2,c_4),name(b_0,"X.mtx"),multi(b_1,c_4,c_5),name(b_1,"U.csv");
RW1<b_4> :- multi(b_0,b_3,SK_1),name(b_3,"V2.csv"),name(b_2,"V1.csv"),tr(b_2,c_4),name(b_0,"X.mtx"),multi(b_1,c_4,c_5),sub(SK_0,SK_1,b_4),multi(c_5,b_3,SK_0),name(b_1,"U.csv");

****** Decoded Rewritings *****
library(Matrix);
b_0<-as.matrix(readMM("X.mtx"))
b_2<-as.matrix(read.table("V1.csv", header=FALSE, sep=","))
b_1<-as.matrix(read.table("U.csv",  header=FALSE, sep=","))
b_3<-as.matrix(read.table("V2.csv", header=FALSE, sep=","))
RW0<-(((b_1%*%(t(b_2)))-b_0)%*%b_3)

library(Matrix);
b_0<-as.matrix(readMM("X.mtx"))
b_2<-as.matrix(read.table("V1.csv", header=FALSE, sep=","))
b_1<-as.matrix(read.table("U.csv",  header=FALSE, sep=","))
b_3<-as.matrix(read.table("V2.csv", header=FALSE, sep=","))
RW1<-(((b_1%*%(t(b_2)))%*%b_3)-(b_0%*%b_3))
```
As you can see, only two rewritings are found by `HADAD`: (1) `((U%*%t(V1))-X)%*%V2` (the original query) and (2) `(U%*%t(V2))%*%V2-X%*%V2`. None of the them uses the view `ViewV` due to the misssing constraint:(AB)C=A(BC) - matrix multiplication associativity.

(8) In order to select the best expression using MNC cost model (without disabling any constraints), we update the `costModel` field in `config.json` file  
to 2, and re-run again the following command: 
``` bash
$ mvn exec:java -Dexec.mainClass=hadad.tutorial.main.Main  #Make sure to run this command under `HADAD-tutorial` directory
```

The output:
```
****** Encoded CQ Query *****
Q<b_4> :- name(b_0,"X.mtx"),name(b_2,"V1.csv"),name(b_1,"U.csv"),tr(b_2,c_4),multi(b_1,c_4,c_5),sub(c_5,b_0,c_6),multi(c_6,b_3,b_4),name(b_3,"V2.csv");

****** CQ Rewritings *****
RW3<b_4> :- multi(b_0,b_3,SK_1),name(b_0,"X.mtx"),multi(b_1,SK_2,SK_0),sub(SK_0,SK_1,b_4),name(b_3,"V2.csv"),name(SK_2,"ViewV.csv"),name(b_1,"U.csv");

****** Decoded Rewritings *****
library(Matrix);
b_0<-as.matrix(readMM("X.mtx"))
b_1<-as.matrix(read.table("U.csv", header=FALSE, sep=","))
b_3<-as.matrix(read.table("V2.csv", header=FALSE, sep=","))
SK_2<-as.matrix(read.table("ViewV.csv", header=FALSE, sep=","))
RW3<-((b_1%*%SK_2)-(b_0%*%b_3))
```

# HADAD Rewritings on top of MorpheusR!
[MorpheusR](http://www.vldb.org/pvldb/vol10/p1214-chen.pdf) is a framework for factorizing LA operations over normalized data. `HADAD` can help MorpheusR do much better by exploiting LA properties (MorpheusR does not reason about LA properties). 

(1) Let's open [MorpheusRExample folder](https://gitlab.inria.fr/cedar/hadad/-/tree/master/code/HADAD-tutorial/examples/MorpheusRExample) to work on our running example. 

(2) We need to update the `config.json` file with the path of our working directory `MorpheusRExample` and the metadata info of tables `R.csv` and `S.csv`( For the purpose of this tutorial, you can find them in the same directory. MorpheusR assumed that these tables are casted as matrices), and the matrix `A.csv` which can be constructor as follows:

```bash
$ cd examples/MorpheusRExample/
$ python3.7 ../../../../data/synthetic/generate.py --name A --nrow 4 --ncol 4 --density 100%
```

Note that the primary key of `R.csv` is `K` and the foreign key in `S.csv` is `K`. For more details about assumptions made by MorpheusR, check out the [paper](http://www.vldb.org/pvldb/vol10/p1214-chen.pdf). 

```bash
{
	
	"exampleFolder":"examples/MorpheusRExample/",
	"lang": "MR",
	"costModel":1,
	"metadata": [
		...
		,
		{
			"MatrixFilepath":"examples/MorpheusRExample/R.csv",
			"nnz": 9,
			"nrow": 3,
			"ncol": 3
		},
		{
			"MatrixFilepath":"examples/MorpheusRExample/S.csv",
			"nnz": 12,
			"nrow": 4,
			"ncol": 3
		}
		,{
			"MatrixFilepath":"examples/MorpheusRExample/A.csv",
			"nnz": 16,
			"nrow":4,
			"ncol":4
		}
		]
}
```
(4) Add the following query in the `query.R` file. The query constructs the normalized matrix `TN` from the base tables `R.csv` and `S.csv`. Then, MorpheusR will factorize `TN%*%A` operation by pushing the multiplication of `TN` with `A` into `R` and `S`. After that, the `colSums` operation on the result of the factorization will be computed. **Note:** In order to run `query.R` using MorpheusR, make sure to build MorpheusR under `bin/` directory. 
``` bash
library(Matrix);
library('matrixStats');
library(data.table);
source('../../../../bin/Morpheus/src/NormalMatrix.r');
R<- read.csv ("R.csv");
S<- read.csv ("S.csv");
A<- as.matrix ( read.table ( "A.csv" , header = FALSE, sep = ","));
K<- sparseMatrix(i=1:nrow(S), j=S[,"K"], x=1); 
S1<- as.matrix(subset(S, select = c("a","b")));
R1<- as.matrix(subset(R, select = c("c","d" )));
TN<-NormalMatrix(EntTable = list(S1),
                   AttTables = list(R1),
                   KFKDs = list(K),
                  Sparse = TRUE);
res<- colSums(TN%*%A);
```
(5) Add a set of constraints that you want `HADAD` to explore for this example in `constraints_chase` and `constraints_bkchase` files. For example,
```
#EGDs
name(id1,n1),name(id2,n1)->id2=id1;
colSums(idA,id1),colSums(idA,id2)->id2=id1;
multi(idA,idB,idr1),multi(idA,idB,idr2)->idr2=idr1;


#TGDs
#colSum(A%*%B)=colSum(A)%*%B)
multi(idA,idB,idr1),colSums(idr1,idr2)->colSums(idA,idr3),multi(idr3,idB,idr2);
colSums(idA,idr3),multi(idr3,idB,idr2)->multi(idA,idB,idr1),colSums(idr1,idr2);
```

(6) Let's see what is the best rewiritng found by `HADAD`?. **Note: the used costModel should be naive(1) since MNC cannot construct a matrix histgoram over a normalized matrix**. More details about MNC can be found [here](https://dl.acm.org/doi/10.1145/3299869.3319854). Run the following command to get the results:
``` bash
$ mvn exec:java -Dexec.mainClass=hadad.tutorial.main.Main  #Make sure to run this command under `HADAD-tutorial` directory
```
The output
```
****** Encoded CQ Query *****
Q<b_7> :- nameR(b_0,"R.csv"),name(b_2,"A.csv"),nameR(b_1,"S.csv"),subset_2(b_1,"a","b",b_4),nrow(b_1,b_1_i),createRange("1",b_1_i,i),createVector(b_1,"_","K",j),indicatorMatrix(i,j,b_3),normalizedMatrix(b_4,b_5,b_3,b_6),subset_2(b_0,"c","d",b_5),multi(b_6,b_2,c_8),colSums(c_8,b_7);

****** CQ Rewritings *****
RW1<b_7> :- nrow(b_1,b_1_i),subset_2(b_1,"a","b",b_4),nameR(b_1,"S.csv"),createVector(b_1,"_","K",j),createRange("1",b_1_i,i),name(b_2,"A.csv"),indicatorMatrix(i,j,b_3),nameR(b_0,"R.csv"),multi(SK_0,b_2,b_7),subset_2(b_0,"c","d",b_5),normalizedMatrix(b_4,b_5,b_3,b_6),colSums(b_6,SK_0);

****** Decoded Rewritings *****
library(Matrix);
library('matrixStats');
library(data.table);
source('../../../../bin/Morpheus/src/NormalMatrix.r');
b_1<-read.csv("S.csv");
b_0<-read.csv("R.csv");
b_2<-as.matrix(read.table("A.csv", header=FALSE, sep=","));
b_3<-sparseMatrix(i=1:nrow(b_1),j=b_1[,"K"],x=1);
b_4<-as.matrix(subset(b_1,select=c("a","b")));
b_5<-as.matrix(subset(b_0,select=c("c","d")));
b_6<-NormalMatrix(EntTable=list(b_4),AttTables=list(b_5),KFKDs=list(b_3),Sparse = TRUE);
RW1<-((colSums(b_6))%*%b_2)
```
As you can see, `HADAD` rewrites the pipeline to `colSums(b_6)%*%b_2` (`b_6` is the constructed normalized matrix) by
exploiting the property colSums(𝐴𝐵) =colSums(𝐴)𝐵 and applying its cost estimator, which favors rewritings with a small intermediate result size. Evaluating this HADAD-produced rewriting, MorpheusR’s multiplication pushdown rule no longer applies, while
the `colSums` pushdown rule is now enabled, which can lead to a significant performance gain. 

(7) Let's change the LA pipeline in the `query.R` file. Replace `res<- colSums(TN%*%A)` with `res<- rowSums(A%*%TN)`. 
``` bash
...
res<- rowSums(A%*%TN);
```
In order for `HADAD` to exploit the property: rowSums(𝐴𝐵) =𝐴rowSums(𝐵). We add this property as an integrity constraint in `constraints_chase` and `constraints_bkchase` files. 
```
....
#rowSums(AB)=ArowSums(B)
multi(idA,idB,idr1),rowSums(idr1,idr2)->rowSums(idB,idr3),multi(idA,idr3,idr2);
rowSums(idB,idr3),multi(idA,idr3,idr2)->multi(idA,idB,idr1),rowSums(idr1,idr2);
```
Now, we run `HADAD` for this example:
``` bash
$ mvn exec:java -Dexec.mainClass=hadad.tutorial.main.Main  #Make sure to run this command under `HADAD-tutorial` directory
```
The output:
```
****** Encoded CQ Query *****
Q<b_7> :- nameR(b_0,"R.csv"),name(b_2,"A.csv"),nameR(b_1,"S.csv"),subset_2(b_1,"a","b",b_4),nrow(b_1,b_1_i),createRange("1",b_1_i,i),createVector(b_1,"_","K",j),indicatorMatrix(i,j,b_3),normalizedMatrix(b_4,b_5,b_3,b_6),subset_2(b_0,"c","d",b_5),multi(b_2,b_6,c_8),rowSums(c_8,b_7);

****** CQ Rewritings *****
RW1<b_7> :- nrow(b_1,b_1_i),subset_2(b_1,"a","b",b_4),multi(b_2,SK_0,b_7),nameR(b_1,"S.csv"),createVector(b_1,"_","K",j),createRange("1",b_1_i,i),name(b_2,"A.csv"),indicatorMatrix(i,j,b_3),nameR(b_0,"R.csv"),rowSums(b_6,SK_0),subset_2(b_0,"c","d",b_5),normalizedMatrix(b_4,b_5,b_3,b_6);

****** Decoded Rewritings *****
library(Matrix);
library('matrixStats');
library(data.table);
source('../../../../bin/Morpheus/src/NormalMatrix.r');
b_1<-read.csv("S.csv");
b_0<-read.csv("R.csv");
b_2<-as.matrix(read.table("A.csv", header=FALSE, sep=","));
b_3<-sparseMatrix(i=1:nrow(b_1),j=b_1[,"K"],x=1);
b_4<-as.matrix(subset(b_1,select=c("a","b")));
b_5<-as.matrix(subset(b_0,select=c("c","d")));
b_6<-NormalMatrix(EntTable=list(b_4),AttTables=list(b_5),KFKDs=list(b_3),Sparse = TRUE);
RW1<-(b_2%*%(rowSums(b_6)))
```
`HADAD` rewrites the pipeline into `b_2%*%(rowSums(b_6)))` (`b_6` is the constructed normalized matrix) . In this rewriting,  `HADAD` enables Morpheus to push the `rowSums` operator to `R` and `S` instead of computing the large intermediate matrix multiplication. 

(8) Finally, let's change the LA pipeline in the `query.R` file to `sum(TN+A)`. Replace `res<- rowSums(A%*%TN)` with `res<- sum(TN+A)`.
``` bash
...
res<- sum(TN+A);
```
If you try to run `query.R` using MorpheusR, it will result into `Execution halted`. The reason is that MorpheusR does not factorize element-wise operations (e.g., addition). However, `HADAD` will find an alternative rewrite of the query by distributing the `sum` over the addition. This will enbale the pipeline to be executed by MorpheusR. The rewriting enables MorpheusR to push the `sum` to `R` and `S` and avoids the (large and
dense) intermediate result of the element-wise matrix addition.  

In order for `HADAD` to find such rewriting, we need to add the property: `sum(A+B)=sum(A)+sum(B)` as an integrity constraint in our example `constraints_chase` and `constraints_bkchase` files. 
```
....
#sum(A+B)=sum(A)+sum(B)
sum (idA,s1),sum(idB,s2),add_s(s1,s2,idr2)->add(idA,idB,idr1),sum(idr1,idr2);
add(idA,idB,idr1),sum(idr1,idr2)->sum(idA,s1),sum(idB,s2),add_s(s1,s2,idr2);

```
Now, we run `HADAD` for this example again:
``` bash
$ mvn exec:java -Dexec.mainClass=hadad.tutorial.main.Main  #Make sure to run this command under `HADAD-tutorial` directory
```
The output:
```
****** Encoded CQ Query *****
Q<b_7> :- nameR(b_0,"R.csv"),name(b_2,"A.csv"),nameR(b_1,"S.csv"),subset_2(b_1,"a","b",b_4),nrow(b_1,b_1_i),createRange("1",b_1_i,i),createVector(b_1,"_","K",j),indicatorMatrix(i,j,b_3),normalizedMatrix(b_4,b_5,b_3,b_6),subset_2(b_0,"c","d",b_5),add(b_2,b_6,c_8),sum(c_8,b_7);

****** CQ Rewritings *****
RW1<b_7> :- nrow(b_1,b_1_i),sum(b_2,SK_1),subset_2(b_1,"a","b",b_4),nameR(b_1,"S.csv"),createVector(b_1,"_","K",j),sum(b_6,SK_0),createRange("1",b_1_i,i),name(b_2,"A.csv"),indicatorMatrix(i,j,b_3),nameR(b_0,"R.csv"),subset_2(b_0,"c","d",b_5),add_s(SK_0,SK_1,b_7),normalizedMatrix(b_4,b_5,b_3,b_6);

****** Decoded Rewritings *****
library(Matrix);
library('matrixStats');
library(data.table);
source('../../../../bin/Morpheus/src/NormalMatrix.r');
b_1<-read.csv("S.csv");
b_0<-read.csv("R.csv");
b_2<-as.matrix(read.table("A.csv", header=FALSE, sep=","));
b_3<-sparseMatrix(i=1:nrow(b_1),j=b_1[,"K"],x=1);
b_4<-as.matrix(subset(b_1,select=c("a","b")));
b_5<-as.matrix(subset(b_0,select=c("c","d")));
b_6<-NormalMatrix(EntTable=list(b_4),AttTables=list(b_5),KFKDs=list(b_3),Sparse = TRUE);
RW1<-((sum(b_6))+(sum(b_2)))
```
`HADAD` distributes the `sum` operation over the addition. This rewriting enables MorpheusR to apply its `sum` factorization rule which pushes the `sum` to `R` and `S`.
