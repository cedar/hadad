library(Matrix);
A<- as.matrix(readMM("M.mtx"));
B<- as.matrix(read.table("N.csv",header = FALSE, sep = ","));
R<- sum(A + B);
