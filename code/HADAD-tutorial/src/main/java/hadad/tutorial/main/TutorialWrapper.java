/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.tutorial.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import hadad.base.compiler.Language;
import hadad.base.compiler.exceptions.HadadException;
import hadad.base.rewriting.decoder.la.api.LATranslator;
import hadad.commons.conjunctivequery.ConjunctiveQuery;
import hadad.tutorial.utils.MetadataUtils;
import hadad.tutorial.utils.RewritingUtils;

/**
 * Tutorial Example Wrapper
 * 
 * @author ranaalotaibi
 * 
 */
public class TutorialWrapper {

    /** Found rewritings **/
    private static List<ConjunctiveQuery> rws = new ArrayList<>();
    /** Decoded rewritings **/
    private static List<String> decodedRWs = new ArrayList<>();

    /**
     * Find and decode rewritings for an example provided in config.json
     */
    @SuppressWarnings("unchecked")
    public static void findRewritings() throws HadadException {

        //Read from config JSON file 
        final Map<String, Object> tutConfig = MetadataUtils.parseConfigTutorialFile();
        final Language language = Language.valueOf((String) tutConfig.get("lang"));
        final int costModel = (int) tutConfig.get("costModel");
        final String examplePath = (String) tutConfig.get("exampleFolder");
        final List<Object> metaData = (List<Object>) tutConfig.get("metadata");

        //Set metdata info obtained from config.json
        MetadataUtils.setMetaData(metaData, costModel);

        //find Rewritings
        try {
            rws = RewritingUtils.computeRewritings(examplePath, language, costModel);

        } catch (Exception e) {
            throw new HadadException(e.getMessage());
        }

        //Decode  Rewritings
        decode(language);
    }

    /**
     * Decode RWs
     * 
     * @param language
     *            query language specified in config.json
     */
    private static void decode(Language language) throws HadadException {
        LATranslator laTranslator = LATranslator.createInstance(language);
        for (ConjunctiveQuery rw : rws) {
            laTranslator.setRewriting(rw);
            decodedRWs.add(laTranslator.translate());
        }
    }

    /**
     * Get enocded query
     * 
     * @return encoded query
     */
    public static ConjunctiveQuery getEncodedQuery() {
        return RewritingUtils.getEncodedQuery();
    }

    /**
     * Get CQ rewritings
     * 
     * @return rws
     *         CQ rewritings
     */
    public static List<ConjunctiveQuery> getRWs() {
        return rws;
    }

    /**
     * Get decoded rewritings
     * 
     * @return decodedRWs decoded rewritings
     */
    public static List<String> getDecodedRWs() {
        return decodedRWs;
    }

}
