/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.tutorial.utils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import hadad.base.compiler.exceptions.HadadException;
import hadad.base.compiler.exceptions.MetadataException;
import hadad.base.compiler.model.la.builder.estim.MatrixBlock;
import hadad.base.compiler.model.la.builder.estim.MatrixHistogram;
import hadad.base.compiler.model.la.metadata.Metadata;
import hadad.base.loader.DataLoaderCSV;
import hadad.base.loader.DataLoaderMTX;
import hadad.base.loader.Serializer;

/**
 * Tutorial metdata data and configuration utils
 * 
 * @author ranaalotaibi
 */
public class MetadataUtils {

    /**
     * Metadata path
     */
    private final static String META_DATA_PATH = "src/main/resources/metadata.json";

    /**
     * Parse config tutorial file
     * 
     * @return parsed info
     */
    public static Map<String, Object> parseConfigTutorialFile() throws HadadException {
        try {
            final ObjectMapper mapper = new ObjectMapper();

            @SuppressWarnings("unchecked")
            final Map<String, Object> map = mapper.readValue(new File("config.json"), Map.class);

            return map;

        } catch (Exception ex) {
            throw new HadadException("Config file coudln't be parsed: \n" + ex.getMessage());
        }
    }

    /**
     * Set metadata info obtained from config.json and build matrix histgorms on base matrices
     * if MNC cost model is selected
     * 
     * @param metaData
     *            the metadata info from config.json
     */
    public static void setMetaData(final List<Object> metaData, final int costModel) throws HadadException {
        try {
            final File file = new File(META_DATA_PATH);
            final String absolutePath = file.getAbsolutePath();

            Metadata.MetadataInit(absolutePath);
            Metadata.getInstance();
            Metadata.load();
            Metadata.instance.delete();
        } catch (MetadataException e) {
            throw new HadadException(e.getMessage());
        }

        for (Object data : metaData) {
            @SuppressWarnings("unchecked")
            Map<String, Object> castedData = (Map<String, Object>) data;
            String matrixPath = (String) castedData.get("MatrixFilepath");
            Path p = Paths.get(matrixPath);

            Metadata.instance.add(p.getFileName().toString(), (int) castedData.get("nrow"),
                    (int) castedData.get("ncol"), (int) castedData.get("nnz"), null);
            if (costModel == 2) {
                buildBaseHistgoram(matrixPath);
            }
        }

    }

    /**
     * Build base matrix histgorm
     * 
     * @param matrixPath
     *            matrix file path
     * @throws HadadException
     *             hadad exception
     */
    private static void buildBaseHistgoram(final String matrixPath) throws HadadException {
        final File file = new File(matrixPath);
        try {
            MatrixBlock matrixBlock = null;
            if (file.getName().contains(".csv")) {
                matrixBlock = DataLoaderCSV.load(file);
            }
            if (file.getName().contains(".mtx")) {

                matrixBlock = DataLoaderMTX.load(file);
            }
            final MatrixHistogram histogram = new MatrixHistogram(matrixBlock, true);
            final String serPath = Files.createTempDirectory("TempFolder").resolve(file.getName()).toString();
            Path p = Paths.get(matrixPath);
            Metadata.instance.update(p.getFileName().toString(), serPath);
            Metadata.instance.write();
            Serializer.serialize(serPath, histogram);
            Metadata.load();
        } catch (Exception e) {
            throw new HadadException(e.getMessage());
        }
    }

}
