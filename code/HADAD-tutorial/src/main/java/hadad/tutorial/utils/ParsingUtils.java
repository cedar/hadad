/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.tutorial.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import com.google.inject.Guice;
import com.google.inject.Injector;

import hadad.base.compiler.Language;
import hadad.base.compiler.PathExpression;
import hadad.base.compiler.QueryBlockTree;
import hadad.base.compiler.IQueryBlockTreeBuilder;
import hadad.base.compiler.exceptions.HadadException;
import hadad.base.compiler.exceptions.ParseException;
import hadad.base.compiler.model.la.mr.MRNaiveModule;
import hadad.base.compiler.model.la.mr.MRQueryBuilder;
import hadad.base.compiler.model.la.nm.NMNaiveModule;
import hadad.base.compiler.model.la.nm.NMQueryBuilder;
import hadad.base.compiler.model.la.rm.RMNaiveModule;
import hadad.base.compiler.model.la.rm.RMQueryBuilder;
import hadad.base.compiler.model.la.sm.SMNaiveModule;
import hadad.base.compiler.model.la.sm.SMQueryBuilder;
import hadad.base.compiler.model.la.tm.TMNaiveModule;
import hadad.base.compiler.model.la.tm.TMQueryBuilder;
import hadad.base.utils.StringUtils;
import hadad.commons.conjunctivequery.Atom;

/**
 * Query and views ParsingUtils
 * 
 * @author ranaalotaibi
 */
public class ParsingUtils {

    /**
     * Get CQ of a query
     * 
     * @param lang
     *            query language
     * @return a string CQ of a query
     */
    public static String getCQQuery(final Language lang, final String examplePath) throws HadadException {
        final IQueryBlockTreeBuilder queryBlockTreeBuilder = getBuilder(lang);
        QueryBlockTree nbt = null;

        try {

            final String query = FileUtils.readFileToString(new File(examplePath + "/query" + getLangEx(lang)));

            final String processedQuery = StringUtils.processQuery(query, lang);
            if (processedQuery != null) {
                nbt = queryBlockTreeBuilder.buildQueryBlockTree(processedQuery);
            }
        } catch (ParseException | IOException e) {
            throw new HadadException("Query couldn't be parsed: \n" + e.getMessage());
        }
        final StringBuilder querystrBuilder = new StringBuilder();
        final Collection<PathExpression> paths = nbt.getRoot().getPattern().getStructural().getPathExpressions();
        final List<Atom> atoms = new ArrayList<>();
        final String queryName = nbt.getRoot().getQueryName();
        final String returnVar =
                nbt.getRoot().getReturnTemplate().getTerms().get(0).getParent().getElement().toString();
        querystrBuilder.append(queryName);
        querystrBuilder.append("<");
        querystrBuilder.append(returnVar);
        querystrBuilder.append(">:-");

        for (final PathExpression path : paths) {
            atoms.addAll(path.encoding());
        }
        int i = 0;
        for (final Atom atom : atoms) {
            querystrBuilder.append(atom);
            if (i != atoms.size() - 1)
                querystrBuilder.append(",");
            i++;
        }

        querystrBuilder.append(";");
        return querystrBuilder.toString();

    }

    private static String getLangEx(final Language lang) {
        switch (lang) {
            case R:
            case MR:
                return ".R";
        }
        return null;

    }

    /**
     * Get CQ constraints of views
     * 
     * @param lang
     *            query language
     * @param examplePath
     *            example path
     * @return list of view constraints
     */
    public static final Map<String, List<String>> getCQViews(final Language language, final String examplePath)
            throws HadadException {
        final IQueryBlockTreeBuilder queryBlockTreeBuilder = getBuilder(language);
        final Map<String, List<String>> viewConstraints = new HashMap<>();
        QueryBlockTree nbt = null;
        final File[] files = new File(examplePath).listFiles();

        for (final File file : files) {
            if (file.getName().contains("View")
                    && (!file.getName().contains(".csv") || file.getName().contains(".mtx"))) {
                String name = "";
                try {

                    final String view = FileUtils.readFileToString(new File(examplePath + file.getName()));
                    final Map<String, String> processedView = StringUtils.processView(view, language);
                    Map.Entry<String, String> entry = processedView.entrySet().iterator().next();
                    nbt = queryBlockTreeBuilder.buildQueryBlockTree(entry.getValue());
                    name = entry.getKey();
                } catch (ParseException | IOException e) {
                    throw new HadadException("Query couldn't be parsed: \n" + e.getMessage());
                }
                final StringBuilder bodyConstraint = new StringBuilder();
                final StringBuilder headConstraint = new StringBuilder();
                final StringBuilder fwConstraint = new StringBuilder();
                final StringBuilder bwConstraint = new StringBuilder();

                final Collection<PathExpression> paths =
                        nbt.getRoot().getPattern().getStructural().getPathExpressions();
                final List<Atom> atoms = new ArrayList<>();
                final String viewName = name;
                final String returnVar =
                        nbt.getRoot().getReturnTemplate().getTerms().get(0).getParent().getElement().toString();

                headConstraint.append("name(");
                headConstraint.append(returnVar);
                headConstraint.append(",");
                headConstraint.append(viewName);
                headConstraint.append(")");

                for (final PathExpression path : paths) {
                    atoms.addAll(path.encoding());
                }
                int i = 0;
                for (final Atom atom : atoms) {
                    bodyConstraint.append(atom);
                    if (i != atoms.size() - 1)
                        bodyConstraint.append(",");
                    i++;
                }

                fwConstraint.append(bodyConstraint.toString());
                fwConstraint.append("->");
                fwConstraint.append(headConstraint.toString() + ";");

                bwConstraint.append(headConstraint.toString());
                bwConstraint.append("->");
                bwConstraint.append(bodyConstraint.toString() + ";");

                final List<String> constraints = new ArrayList<>();
                constraints.add(fwConstraint.toString());
                constraints.add(bwConstraint.toString());
                viewConstraints.put(name, constraints);
            }
        }
        return viewConstraints;

    }

    /**
     * Get Language Builder
     * 
     * @param lang
     *            query language
     * @return language builder
     */
    private static IQueryBlockTreeBuilder getBuilder(final Language lang) throws HadadException {
        Injector injector = null;
        IQueryBlockTreeBuilder builder = null;

        switch (lang) {
            case R:
                injector = Guice.createInjector(new RMNaiveModule());
                builder = injector.getInstance(RMQueryBuilder.class);
                return builder;
            case NUMPY:
                injector = Guice.createInjector(new NMNaiveModule());
                builder = injector.getInstance(NMQueryBuilder.class);
                return builder;
            case TFM:
                injector = Guice.createInjector(new TMNaiveModule());
                builder = injector.getInstance(TMQueryBuilder.class);
                return builder;
            case DML:
                injector = Guice.createInjector(new SMNaiveModule());
                builder = injector.getInstance(SMQueryBuilder.class);
                return builder;
            case MR:
                injector = Guice.createInjector(new MRNaiveModule());
                builder = injector.getInstance(MRQueryBuilder.class);
                return builder;
            default:
                throw new HadadException("Langugae is not supported!");
        }
    }

}
