/**
   * Copyright 2021 Rana Alotaibi, Bogdan Cautis, Alin Deutsch, and Ioana Manolescu
   * Licensed under the Apache License, Version 2.0 (the "License");
   * you may not use this file except in compliance with the License.
   * You may obtain a copy of the License at
   *     http://www.apache.org/licenses/LICENSE-2.0
   * Unless required by applicable law or agreed to in writing, software
   * distributed under the License is distributed on an "AS IS" BASIS,
   * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   * See the License for the specific language governing permissions and
   * limitations under the License.
**/
package hadad.tutorial.utils;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hadad.base.compiler.Language;
import hadad.base.rewriter.Context;
import hadad.base.rewriter.PACBRewriter;
import hadad.commons.conjunctivequery.ConjunctiveQuery;
import hadad.commons.constraints.Constraint;
import hadad.commons.constraints.parser.ConstraintParser;
import hadad.commons.constraints.parser.ParseException;
import hadad.commons.relationalschema.RelationalSchema;
import hadad.commons.relationalschema.parser.RelSchemaParser;

/**
 * Rewriting Utils
 * 
 * @author ranaalotaibi
 */
public class RewritingUtils {
    private static ConjunctiveQuery encodedQuery = null;

    /**
     * Compute rewritings
     * 
     * @param examplePath
     *            example path
     * @param language
     *            query and view language
     * @param costModel
     *            seleced cot model
     * @return found rewritings
     * @throws Exception
     */
    public static List<ConjunctiveQuery> computeRewritings(final String examplePath, final Language language,
            final int costModel) throws Exception {
        //Parse query in `query` file and generate the corresponding CQ query.
        final ConjunctiveQuery query =
                hadad.base.compiler.Utils.parseQuery(ParsingUtils.getCQQuery(language, examplePath));
        encodedQuery = query;
        final Map<String, List<String>> viewConstraints = ParsingUtils.getCQViews(language, examplePath);
        //Add views to the set of constraints 
        addViewsConstraints(viewConstraints, examplePath);
        //Find rewritings of a given example configuration
        return getRewriter(examplePath).getReformulations(query, language, costModel);
    }

    /**
     * Prepare context for PACB
     * 
     * @param examplePath
     *            example path that includes constraints files
     * @return context
     * @throws Exception
     */
    private static Context getContext(final String examplePath) throws Exception {
        final List<Constraint> fw = parseConstraints(examplePath + "constraints_chase");
        final List<Constraint> bw = parseConstraints(examplePath + "constraints_bkchase");
        final List<RelationalSchema> schemas = parseSchemas(examplePath + "schemas");
        return new Context(schemas.get(0), schemas.get(1), fw, bw);
    }

    /**
     * Parse constraint
     * 
     * @param fileName
     *            file name
     * @return list of parsed constraints
     * @throws IOException
     *             | hadad.commons.constraints.parser.ParseException
     */
    private static List<Constraint> parseConstraints(final String fileName)
            throws IOException, hadad.commons.constraints.parser.ParseException {
        final FileReader fr = new FileReader(fileName);
        final ConstraintParser parser = new ConstraintParser(fr);
        final List<Constraint> constraints = parser.parse();
        fr.close();
        return constraints;
    }

    /**
     * Parse schemas
     * 
     * @param fileName
     * @return
     * @throws IOException
     * @throws hadad.commons.relationalschema.parser.ParseException
     */
    private static List<RelationalSchema> parseSchemas(final String fileName)
            throws IOException, hadad.commons.relationalschema.parser.ParseException {
        final FileReader fr = new FileReader(fileName);
        final RelSchemaParser parser = new RelSchemaParser(fr);
        final List<RelationalSchema> constraints = parser.parse();
        fr.close();
        return constraints;
    }

    /**
     * Get rewriter
     * 
     * @param examplePath
     *            example path
     * @param language
     *            language
     * @return PACB rewriter
     * @throws Exception
     */
    private static PACBRewriter getRewriter(final String examplePath) throws Exception {
        return new PACBRewriter(getContext(examplePath));
    }

    private static void addViewsConstraints(final Map<String, List<String>> viewConstraints, final String examplePath)
            throws IOException, ParseException {

        //Avoid adding redundent views
        final List<Constraint> fws = parseConstraints(examplePath + "constraints_chase");
        final Map<String, List<String>> filteredViewConstraints = new HashMap<>();
        for (Map.Entry<String, List<String>> constraint : viewConstraints.entrySet()) {
            boolean flag = false;
            for (Constraint fw : fws) {
                if (fw.toString().contains(constraint.getKey())) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                filteredViewConstraints.put(constraint.getKey(), constraint.getValue());
            }
        }

        //Add view constraints
        for (Map.Entry<String, List<String>> constraint : filteredViewConstraints.entrySet()) {
            try {
                Files.write(Paths.get(examplePath + "constraints_chase"), constraint.getValue().get(0).getBytes(),
                        StandardOpenOption.APPEND);
                Files.write(Paths.get(examplePath + "constraints_bkchase"), constraint.getValue().get(1).getBytes(),
                        StandardOpenOption.APPEND);
            } catch (IOException e) {
                throw e;
            }
        }

    }

    /**
     * Get encoded query
     * 
     * @return encoded query
     */
    public static ConjunctiveQuery getEncodedQuery() {
        return encodedQuery;
    }

}
