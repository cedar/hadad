#Commons Build
cd HADAD-commons/
cd conjunctivequery/
mvn clean install -DskipTests
cd ..
cd relationalschema/
mvn clean install -DskipTests
cd ../..

#HADAD-PACB
cd HADAD-PACB/
cd Chase/
mvn clean install -DskipTests
cd ..
cd Backchase/
mvn clean install -DskipTests
cd ..
cd CB/
mvn clean install -DskipTests
cd ../..

#HADAD-base
cd HADAD-base/
mvn clean install
#mvn javadoc:javadoc
#cp -R target/site ../../documentation/codeDocumentation/
cd ..

#HADAD-tutorial
cd HADAD-tutorial/
mvn clean install -DskipTests
cd ..
