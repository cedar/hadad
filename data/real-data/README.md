- The google drive link to the real-data: https://drive.google.com/drive/folders/1imC5lMV8hA7csua91nj0efW19lK2GN9p?usp=sharing
- Due to Twitter policy (https://developer.twitter.com/en/developer-terms/more-on-restricted-use-cases), we are only allowed to share Tweet IDs(TweetIDs.csv). The tweet content of these IDs can be retrieved from Twitter APIs (https://developer.twitter.com/en/docs/tutorials/step-by-step-guide-to-making-your-first-request-to-the-twitter-api-v2). The JSON twitter dataset should be placed under `data/real-data/tweets/` directory. 

- In order to get MIMIC dataset, you are required to formally request an access via a process documented on the MIMIC website (https://mimic.mit.edu/). Once you can access the data, the CSV files should be placed under `data/real-data/` directory. 


