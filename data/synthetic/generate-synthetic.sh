#Generate
python3.7 generate.py --nrow 50000 --ncol 100 --density 100%
python3.7 generate.py --nrow 100 --ncol 50000 --density 100%
python3.7 generate.py --nrow 1000000 --ncol 100 --density 100%
python3.7 generate.py --nrow 5000000 --ncol 100 --density 100%
python3.7 generate.py --nrow 10000 --ncol 10000 --density 100%
python3.7 generate.py --nrow 20000 --ncol 20000 --density 100%
python3.7 generate.py --nrow 100 --ncol 1 --density 100%
python3.7 generate.py --nrow 50000 --ncol 1 --density 100%
python3.7 generate.py --nrow 100000 --ncol 1 --density 100%
python3.7 generate.py --nrow 100 --ncol 100 --density 100%

#Copy
cp matrix-50000-100-1.0.csv syn1.csv
cp matrix-100-50000-1.0.csv syn2.csv
cp matrix-1000000-100-1.0.csv syn3a.csv
cp matrix-1000000-100-1.0.csv syn3b.csv
cp matrix-5000000-100-1.0.csv syn4a.csv
cp matrix-5000000-100-1.0.csv syn4b.csv
cp matrix-10000-10000-1.0.csv syn5c.csv
cp matrix-10000-10000-1.0.csv syn5d.csv
cp matrix-10000-10000-1.0.csv syn5e.csv
cp matrix-20000-20000-1.0.csv syn6c.csv
cp matrix-20000-20000-1.0.csv syn6d.csv
cp matrix-20000-20000-1.0.csv syn6e.csv
cp matrix-100-1-1.0.csv syn7.csv
cp matrix-100-1-1.0.csv V1.csv
cp matrix-50000-1-1.0.csv syn8.csv
cp matrix-100000-1-1.0.csv syn9.csv
cp matrix-100-100-1.0.csv syn10.csv

#Hybrid Exp Synthetic: MIMIC Exp
python3.7 generate.py --nrow 35000 --ncol 1 --density 100%
python3.7 generate.py --nrow 48612 --ncol 1 --density 100%
python3.7 generate.py --nrow 35000 --ncol 48612 --density 100%
python3.7 generate.py --nrow 48612 --ncol 35000 --density 100%
python3.7 generate.py --nrow 82 --ncol 35000 --density 100%
python3.7 generate.py --nrow 28000 --ncol 1 --density 100%
python3.7 generate.py --nrow 28000 --ncol 35000 --density 100%
python3.7 generate.py --nrow 28000 --ncol 48612 --density 100%
python3.7 generate.py --nrow 82 --ncol 28000 --density 100%
python3.7 generate.py --nrow 35000 --ncol 28000 --density 100%
python3.7 generate.py --nrow 11000 --ncol 1 --density 100%
python3.7 generate.py --nrow 11000 --ncol 35000 --density 100%
python3.7 generate.py --nrow 82 --ncol 11000 --density 100%
python3.7 generate.py --nrow 35000 --ncol 11000 --density 100%
python3.7 generate.py --nrow 82 --ncol 48612 --density 100%
python3.7 generate.py --nrow 11000 --ncol 48612 --density 100%

#Hybrid Exp Synthetic: Twitter Exp
python3.7 generate.py --nrow 1000 --ncol 1 --density 100%
python3.7 generate.py --nrow 1 --ncol 1000 --density 100%
python3.7 generate.py --nrow 2380000 --ncol 1 --density 100%
python3.7 generate.py --nrow 1000 --ncol 2380000 --density 100%
python3.7 generate.py --nrow 12 --ncol 2380000 --density 100%
python3.7 generate.py --nrow 2380000 --ncol 1000 --density 100%
python3.7 generate.py --nrow 1000 --ncol 2380000 --density 100%
python3.7 generate.py --nrow 12 --ncol 1000 --density 100%
python3.7 generate.py --nrow 1380000 --ncol 1 --density 100%
python3.7 generate.py --nrow 1380000 --ncol 1000 --density 100%
python3.7 generate.py --nrow 12 --ncol 1380000 --density 100%
python3.7 generate.py --nrow 1000 --ncol 1380000 --density 100%
python3.7 generate.py --nrow 538000 --ncol 1 --density 100%
python3.7 generate.py --nrow 538000 --ncol 1000 --density 100%
python3.7 generate.py --nrow 12 --ncol 538000 --density 100%
python3.7 generate.py --nrow 1000 --ncol 538000 --density 100%
python3.7 generate.py --nrow 20000 --ncol 20000 --density 100%
python3.7 generate.py --nrow 20000 --ncol 1 --density 100%
python3.7 generate.py --nrow 12 --ncol 20000 --density 100%



