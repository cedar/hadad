#script for converting CSV file into MTX for spare matrix
import argparse
import scipy.sparse as sparse
import scipy.stats as stats
import numpy as np
import os.path
from os import path
import csv
import sys

#Arguments parser
parser = argparse.ArgumentParser()
parser.add_argument(
    '--fileName', default="", type=str,
    help='Matirx name')

#Create MTX format from CSV file
def generateMTXMatrix (fileName):
    sp_data = []
    i =1
    nrow=0
    ncol=0
    fname = fileName.split(".")[0]+'.mtx';
    if os.path.exists(fname): 
        os.remove(fname)
    f= open(fname,'a')
    f.write("%%MatrixMarket matrix coordinate integer general\n")
    with open(fileName) as csv_file: 
        for row in csv_file:
            data = np.fromstring(row, sep=",")
            if i==1:
                ncol=len(data)
            data = sparse.coo_matrix(data)
            sp_data.append(data)
            nrow=nrow+1
    sp_data = sparse.vstack(sp_data)
    f.write(str(nrow)+' '+str(ncol)+' '+str(ncol*nrow)+'\n')
    for i,j,v in zip(sp_data.row, sp_data.col, sp_data.data):
        val = 0
        if(i==0):
            val=1
        else:
            val=i
        f.write(str(val)+' '+str(j)+' '+str(int(v))+'\n')

# make sure git ignores the csv file
with open('.gitignore', 'w') as fh:
    fh.write('*.csv')

args = parser.parse_args()
generateMTXMatrix(args.fileName)
